<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

    <?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
		  if(!isset($_GET['Seminar_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Seminarteilnehmer: <a href="seminar.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><font color="black"> 
									<?php echo $seminar['Titel']; ?> </font></a> </br>
				 Lehrstuhl: <a href="profil2.php?Email=<?php echo $seminar['Email'] ?>" data-toggle="tooltip" title="Weiter zum Lehrstuhl"><font color="black"> <?php echo $seminar['Bezeichnung']; ?></font> </a></h2>	
	<?php
			if(empty ($teilnehmer)){
	?>
			<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Keine Seminarteilnehmer zu diesem Seminar. </br>
					   Alle Bewerber: <a href="bewerberliste.php?Seminar_ID=<?php echo $seminarID; ?>&Semester=<?php echo $semester; ?>" >
						<font color="red">anzeigen</font> </a>
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
				
			}else{
	?>
			<h4> Es sind <font color="red"><?php echo $eingetragen[0]; ?></font> Studenten in diesem Seminar eingetragen.</h4>
			<h5> Maximale Teilnehmeranzahl: <?php echo $seminar['Teilnehmeranzahl']; ?> Studenten </h5>
			<h5> Alle Bewerber <a href="bewerberliste.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>"> <font color="red">anzeigen</font> </a> </h5>
	<?php 
				if($rolle != 1){
	?>	
			<div>
				<div>
				<form class="form-horizontal" align="right" action="befehlProzesse.php" method="post" name="export_csv_bewerber" enctype="multipart/form-data">
					<div class="form-group">
						<input type="submit" name="Export_Teilnehmer" class="btn btn-success" value="Download CSV"/>
						<input type="hidden" name="seminarID" value=<?php echo $seminarID ?> >
						<input type="hidden" name="semester" value=<?php echo $semester ?> >
					</div>
                </div>                    
				</form> 
 			</div>
	<?php
				}
	?>
			<div class="table-responsive">	
			<table class="table table table-striped table-bordered">		
			<thead>
			<tr>
				<th scope="col"> Anzahl             	</th>
				<th scope="col"> Matrikelnummer     	</th>
				<th scope="col"> Vorname            	</th>
				<th scope="col"> Name               	</th>
				<th scope="col"> Email              	</th>
				<th scope="col"> Studiengang	    	</th>
				<th scope="col"> Fachsemester       	</th>
				<th scope="col"> Priorität          	</th>
				<th scope="col"> Seminarleistungen  	</th>
				<th scope="col"> Bewerbung am      	 	</th>
				<th scope="col"> Zuteilung am       	</th>
				<th scope="col"> Seminarplatzannahme am </th>
				<th scope="col"> Absolviert             </th>
				<th scope="col"> Absolviert am          </th>
			</tr>
			</thead>
			<tbody>	
	<?php		
				$i = 1; //Zählt die Anzahl der Teilnehmer durch.
				foreach ($teilnehmerDaten as $row){
	?>
			<tr>
				<th scope="row"> <?php echo $i; ?>              				</th>
				<td> <?php  echo $row['Student_ID']; ?>          				</td>
				<td> <?php  echo $row['Vorname']; ?>        				    </td> 
				<td> <?php  echo $row['Name']; ?>               				</td> 
				<td> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" data-toggle="tooltip" title="Weiter zum Profil"> 
					 <font color="black"><?php  echo $row['Email']; ?></font></a></td>  
				<td> <?php  echo $row['Studiengang']; ?>   						</td>  
				<td> <?php  echo $row['Fachsemester']; ?>   					</td>  
				<td> <?php  echo $row['Prioritaet']; ?>          				</td>
				<td> <?php  echo $row['Seminarleistungen']; ?>   				</td>
				<td> <?php $date = new DateTime($row['Bewerbung_Datum']);
							echo $date->format('d.m.Y H:i'); ?> 				</td>
				<td> <?php $date2 = new DateTime($row['Zuteilung_Datum']);
							echo $date2->format('d.m.Y H:i'); ?> 				</td>
	<?php
					if($row['Ablehnung_Datum'] != NULL){
	?>
				<td> <?php $date3 = new DateTime($row['Ablehnung_Datum']);
							echo $date3->format('d.m.Y H:i'); ?> 				</td>
	<?php
					}else{
							echo '<td> </td>';
					}
					if($row['Absolviert_Datum'] == NULL){
							echo '<td><i>  </i></td>';
							echo '<td><i>  </i></td>';
					}else{
						if($row['Absolviert'] == 1){
							echo '<td><i> Ja </i></td>';
							echo '<td><i> ';
								  $date4 = new DateTime($row['Absolviert_Datum']);
							echo  $date4->format('d.m.Y H:i');
							echo ' </i></td>';
						}
						if($row['Absolviert'] == 0){      
							echo '<td><i> Nein </i></td>';
							echo '<td><i> ';
								  $date5 = new DateTime($row['Absolviert_Datum']);
							echo  $date5->format('d.m.Y H:i');
							echo ' </i></td>';
						}
					}
	?>
			</tr>
	<?php
					$i++;
				}
	?>
			</tbody>
			</table>
			</div>
			</br>
			
	<?php
					if((!empty($zuteilungZeitraum)   	   && $email == $seminar['Email']   && $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID']) ||
						(!empty($zuteilungZeitraum)   	   && $rolle == 4 					&& $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID'])	|| 
						(!empty($zwZuteilungZeitraum) 	   && $email == $seminar['Email'] 	&& $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID']) ||
						(!empty($zwZuteilungZeitraum)      && $rolle == 4 					&& $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID'])	|| 
						(!empty($ZnachZweiteZuteilungEnde) && $rolle == 3 					&& $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID'])	||
						(!empty($ZnachZweiteZuteilungEnde) && $rolle == 4 					&& $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID']) ){
	?>
			<a class="btn btn-outline-info" href="bewerberliste.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button"> Weitere Seminarteilnehmer hinzufügen </a>
	<?php
					}
					if($ZnachAblehnungEnde || $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID']){
	?>
		<a class="btn btn-outline-info" href="seminarThemen.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button"> Teilnehmer mit zugehörigen Seminarthemen</a>
	<?php
					
					}
					if(($bewerbungszeitraumFestlegen  && $email == $seminar['Email'] && $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID']) ||
					   ($bewerbungszeitraumFestlegen  && $rolle == 4 && $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $seminar['Bewerbungszeitraum_ID']) ){
	?>
		<a class="btn btn-outline-info" href="seminarAbsolviert.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button"> Abschluss Eintragen </a>
	<?php
					
					}
			}
			include 'fusszeile.php';
		  }
		}
	?>
    </div>
  </body>
</html>
