<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
		require 'sql2.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
		  if(!isset($_GET['Bewerbungszeitraum_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$bewerbungszeitraumID   = $_GET['Bewerbungszeitraum_ID'];
	
			include 'navBar.php';
			include 'meldung.php';
			include 'sql2.php';
			
			if($rolle == "1"){
				include 'keineBerechtigung.php';
			}else{ 
	?>
			<h2> Auslastung der Lehrstühle im: <?php echo $bewerbungszeitraum['Name'] ?></h2>
	<?php
				if(empty ($lehrstuehle)){
					echo 'Keine Lehrstühle vorhanden.';
				}else{
	?>
			</br>
	<?php
					foreach ($lehrst as $row){
						$lehrstuhlID = $row['Lehrstuhl_ID'];
						$_SESSION['Lehrstuhl_ID'] = $lehrstuhlID;
						include 'sql2.php';  //Nochmal einbinden, da $lehrstuhlID neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
						
						if($lehrstuhlID != 1){
	?>		
			<h3> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" style="text-decoration:none; color:black;"> 
						<?php echo $row['Bezeichnung']; ?> </a> </h3>
	<?php
							if(empty ($seminar)){
								echo 'Keine Seminare zu diesem Lehrstuhl vorhanden.';
							}else{
								foreach ($seminare as $row2){ 
									$seminarID = $row2['Seminar_ID'];
									$_SESSION['SeminarID'] = $seminarID;
									$semester  = $row2['Semester'];
									$_SESSION['Semester']  = $semester;
									include 'sql2.php'; //Nochmal einbinden, da $seminarID und $semester neu in der Session übergeben werden. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
	?>
		<div class="table-responsive">	
			<table class="table table table-striped table-bordered">
				<h5><?php echo $row2['Abschluss'] ?>seminar: <a href="seminar.php?Seminar_ID=<?php echo $row2['Seminar_ID'] ?>&Semester=<?php echo $row2['Semester'] ?>" style="text-decoration:none; color:black;"> 
									<?php echo $row2['Titel']; ?> </a></h5>
				Maximale Teilnehmeranzahl: <b><?php echo $row2['Teilnehmeranzahl'] ?></b>
	<?php
									if(empty ($teilnehmer)){
										echo '</br>Keine Seminarteilnehmer zu diesem Seminar.';
									}else{
										$i = 1; //Zählt die Anzahl der Bewerber durch.
	?>		
			</br> Seminarteilnehmer: <b> <?php echo $seminarteilnehmer[0]; ?> </b>
			<thead>
			<tr>
				<th scope="col"> Anzahl             </th>
				<th scope="col"> Matrikelnummer     </th>
				<th scope="col"> Vorname            </th>
				<th scope="col"> Name               </th>
				<th scope="col"> Email              </th>
				<th scope="col"> Studiengang	    </th>
				<th scope="col"> Fachsemester       </th>
				<th scope="col"> Priorität          </th>
				<th scope="col"> Seminarleistungen  </th>
				<th scope="col"> Bewerbung am       </th>
				<th scope="col"> Zuteilung am       </th>
				<th scope="col"> Seminarplatzannahme am </th>
				<th scope="col"> Absolviert             </th>
				<th scope="col"> Absolviert am          </th>
			</tr>
			</thead>
			<tbody>	
	<?php		
										foreach ($teilnehmerDaten as $row3){
	?>
			<tr>
				<th scope="row"> <?php echo $i; ?>              </th>
				<td> <?php echo $row3['Student_ID']; ?>          </td>
				<td> <?php echo $row3['Vorname']; ?>             </td> 
				<td> <?php echo $row3['Name']; ?>                </td> 
				<td> <a href="profil2.php?Email=<?php echo $row3['Email'] ?>"> 
						<?php echo $row3['Email']; ?>	      </a></td>  
				<td> <?php echo $row3['Studiengang']; ?>   		</td>  
				<td> <?php echo $row3['Fachsemester']; ?>   		</td>  
				<td> <?php echo $row3['Prioritaet']; ?>          </td>
				<td> <?php echo $row3['Seminarleistungen']; ?>   </td>
				<td> <?php $date = new DateTime($row3['Bewerbung_Datum']);
							echo $date->format('d.m.Y H:i'); ?> </td>
				<td> <?php $date2 = new DateTime($row3['Zuteilung_Datum']);
							echo $date2->format('d.m.Y H:i'); ?> </td>
	<?php
											if($row3['Ablehnung_Datum'] != NULL){
	?>
				<td> <?php $date3 = new DateTime($row3['Ablehnung_Datum']);
							echo $date3->format('d.m.Y H:i'); ?> </td>
	<?php
											}else{
												echo '<td> </td>';
											}
											if($row3['Absolviert_Datum'] == NULL || $row3['Absolviert_Datum'] == "0000-00-00 00:00:00"){
												echo '<td><i>  </i></td>';
												echo '<td><i>  </i></td>';
											}else{
												if($row3['Absolviert'] == 1){
													echo '<td><i> Ja </i></td>';
													echo '<td><i> ';
														 $date4 = new DateTime($row3['Absolviert_Datum']);
													echo $date4->format('d.m.Y H:i');
													echo ' </i></td>';
												}
												if($row3['Absolviert'] == 0){      
													echo '<td><i> ';
														 $date4 = new DateTime($row3['Absolviert_Datum']);
													echo $date4->format('d.m.Y H:i');
													echo ' </i></td>';
												}
											}
	?>
			</tr>
	<?php
					$i++;
										}//Ende foreach Teilnehmer
	?>aa
			</tbody>
	<?php
									}//Ende if(!empty($teilnehmer))
	?>
			</table>
			</div>
			</br>
			</br>
	<?php
								}//Ende foreach Seminare
	?>
			</br></br>
	<?php
							}//Ende if(!empty($seminare))
						}//Ende if($lehrstuhlID != 1)
					}//Ende foreach Lehrstühle 
				}//Ende if(!empty($lehrstuhl))
			}
			include 'fusszeile.php';
		  }
		}
		?>
    </div>
  </body>
</html>
