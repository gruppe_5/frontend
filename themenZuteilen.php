<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	
		  if(!isset($_GET['Seminar_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Seminarteilnehmer: <a href="seminar.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><font color="black"> 
									<?php echo $seminar['Titel']; ?> </font></a> </br>
				 Lehrstuhl: <a href="profil2.php?Email=<?php echo $seminar['Email'] ?>" data-toggle="tooltip" title="Weiter zum Lehrstuhl"><font color="black"> <?php echo $seminar['Bezeichnung']; ?></font> </a></h2>	
	<?php
			if(!empty($thZuteilungZeitraum)){
				if($email == $seminar['Email'] || $rolle == 4){
					if($eingetragen[0] == "0"){
						echo 'Keine Seminarteilnehmer mit zugehörigen Themen zu diesem Seminar.';
					}else{
	?>
			<h4> Seminarteilnehmerzahl: <?php echo $eingetragen[0]; ?> </h4>
			<h5> Maximale Teilnehmeranzahl: <?php echo $seminar['Teilnehmeranzahl']; ?> Studenten </h5>

			<div class="table-responsive">	
			<table class="table table table-striped table-bordered">
	<?php		
						$i = 1; //Zählt die Anzahl der Bewerber durch.
	?>		
			<thead>
			<tr>
				<th scope="col"> Anzahl             </th>
				<th scope="col"> Matrikelnummer     </th>
				<th scope="col"> Vorname            </th>
				<th scope="col"> Name               </th>
				<th scope="col"> Email              </th>
				<th scope="col"> Studiengang	    </th>
				<th scope="col"> Fachsemester       </th>
				<th scope="col"> Seminarthema       </th>
			</tr>
			</thead>
			<tbody>	
	<?php		
						foreach ($teilnehmerDaten as $row){ 
							$studentID = $row['Student_ID'];
							$_SESSION['Student_ID'] = $studentID;
							include 'sql.php';  //Nochmal einbinden, da $lehrstuhlID neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
	?>
				<form action="befehlProzesse.php?Seminar_ID=<?php echo $seminarID ?>&Semester=<?php echo $semester ?>" method="POST">
					<input type="hidden" name="themaZuteilen" value="zuteilen">
					<input type="hidden" name="seminarID"     value=<?php echo $seminarID ?>>
					<input type="hidden" name="semester"      value=<?php echo $semester ?>>
					<input type="hidden" name="student[]"     value=<?php echo $row['Student_ID']; ?>>
			<tr>
				<th scope="row"> <?php echo $i; ?>              </th>
				<td> <?php echo $row['Student_ID']; ?>          </td>
				<td> <?php echo $row['Vorname']; ?>             </td> 
				<td> <?php echo $row['Name']; ?>                </td> 
				<td> <?php echo $row['Email']; ?>				</td>  
				<td> <?php echo $row['Studiengang']; ?>   		</td>  
				<td> <?php echo $row['Fachsemester']; ?>   		</td> 
				<td> <input type="text" name="thema[]" value="<?php echo $thema['Thema']; ?>" > </td> 
			</tr>
		<?php
							$i++;
							}
		?>
			</tbody>
			</table>
			</div>
			</br>
				<button type="submit" class="btn btn-outline-info"> Seminarthemen Zuteilen</button>
				 <a href="seminarThemen.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" class="btn btn-outline-info"> Abbrechen </a>
			</form>
		<?php
					}
				}else{
					include 'keineBerechtigung.php';
				}
			}else{
				include 'keineBerechtigung.php';
			}
			include 'fusszeile.php';
		  }
		}
		?>
    </div>
  </body>
</html>
