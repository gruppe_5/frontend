 <!DOCTYPE html>
<html>
	<head><title>Prozess</title>
	
	<?php include 'css.php'; ?>
	</head>

	<body>
		<div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
		
		//Weiterleitung auf die Startseite, falls diese Seite mit Hilfe der URL direkt aufgerufen wird.
		if( !isset ($_POST['adminRegistrierung'])    &&
			!isset ($_POST['studentRegistrierung'])  &&
			!isset ($_POST['login'])      			 &&
			!isset ($_POST['passwortZuruecksetzen']) &&
			!isset ($_SESSION['Email']) ){
				include 'keinZugriff.php';
		}
		//Weiterleitung auf die Startseite, falls diese Seite mit Hilfe der URL direkt aufgerufen wird.
		if( isset ($_SESSION['Email']) 						&&
			!isset ($_POST['logout'])						&&
			!isset ($_POST['passwortAenderung']) 			&&
			!isset ($_POST['userEntsperren']) 				&&
			!isset ($_POST['studiendekanAnlegen']) 			&&
			!isset ($_POST['lehrstuhlAnlegen']) 			&&
			!isset ($_POST['bewerbungszeitraumFestlegen'])  &&
			!isset ($_POST['bewerbungszeitraumBearbeiten']) &&
			!isset ($_POST['seminarAnlegen']) 				&&
			!isset ($_POST['seminarBearbeiten']) 			&&
			!isset ($_POST['anzahlErweitern']) 				&&
			!isset ($_POST['seminarLoeschen']) 				&&
			!isset ($_POST['bewerben']) 					&&
			!isset ($_POST['bewerbungZurueck']) 			&&
			!isset ($_POST['prioritaetBearbeiten']) 		&&
			!isset ($_POST['zuteilen']) 					&&
			!isset ($_POST['themaZuteilen']) 				&&
			!isset ($_POST['zusagen']) 						&&
			!isset ($_POST['absagen']) 						&&
			!isset ($_POST["Export_Bewerber"]) 				&&
			!isset ($_POST["Export_Teilnehmer"]) 			&&
			!isset ($_GET['Email']) 						&&
			!isset ($_POST['Upload']) 						&&
			!isset ($_POST['userBearbeiten']) 				&&
			!isset ($_POST['userLoeschen']) 				&&
			!isset ($_POST['alleSeminareLoeschen']) 		&&
			!isset ($_POST['testAktivieren']) 				&&
			!isset ($_POST['testDeaktivieren']) 			&&
			!isset ($_POST['absolviertEintragen'])){
				include 'keineBerechtigung.php';
		}
		
		//Registrierung Admin - Nur bei der ersten Inbetriebnahme des Systems möglich!
		$stmt   = $pdo->prepare("SELECT COUNT(Email)
								 FROM User;");
		$result = $stmt->execute(array());
		$admin  = $stmt->fetch();
			
		if ($admin[0] == "0"){
			if (isset ($_POST['adminRegistrierung'])){
				$adminName     = htmlentities($_POST['adminName'],ENT_QUOTES);
				$email         = htmlentities($_POST['email']    ,ENT_QUOTES);
				$email = $email."@uni-passau.de";
				$passwort      = htmlentities($_POST['passwort'] ,ENT_QUOTES);
				$passwort2     = htmlentities($_POST['passwort2'],ENT_QUOTES);
				$token 		   = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';	
				$rolle         = 4;
				$aktiv         = 0;
				
				$error         = false;
		
				if( empty ($adminName) || empty($email) ||
					empty ($passwort)  || empty ($passwort2) ){
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Alle Pflichtfelder bitte ausfüllen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}
				if ($passwort != $passwort2){
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Passwörter stimmen nicht überein.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}
				if(strlen ($passwort) < 8 || strlen ($passwort2) < 8){
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Das Passwort muss mindestens 8 Zeichen enthalten.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}

				if ($error == false){
					$cryptPasswort 					= password_hash ($passwort, PASSWORD_BCRYPT);
					$loginErfolgreich 				= 0; 
					$passwortaenderungErfolgreich 	= 1; 
					$falschesPasswort 				= 0;
					$token 							= str_shuffle($token);
					$token 							= substr($token, 0, 10);
				
					//Versand Verifizierungsmail Admin	
					include_once "phpmailer/PHPMailerAutoload.php";
				
					//Instance für PHPMailer
					$mail = new PHPMailer();
                
					//Host fuer PHPMailer
					$mail->Host = "smtp.gmail.com";
                
					//Aktiviert SMTP
					$mail->isSMTP();
                
					//Authentifizierung
					$mail->SMTPAuth = true;
                
					//Login Details fuer GMAIL
					$mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
					$mail->Password = 'z1cfc4f5';
	
					$mail->SMTPSecure = "tls";

					//Port
					$mail->Port = 587;
	
					//Betreff
					$mail->Subject = "Registrierung: Seminarvergabesystem Gruppe 5";
	
					$mail->isHTML(true);

					//Inhalt
					$mail->Body = "
						Willkommen zum Seminarvergabesystem der Gruppe 5,<br><br>
						Sie haben sich erfolgreich als Admin registriert. <br>
						Um Zugang zum System zu erhalten, aktivieren Sie bitte Ihren Account hier: <br>
						
						<a href='https://132.231.36.206/confirm.php?email=$email&token=$token'> hier</a>";

					//Absender
					$mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
               
					//Empfaenger
					$mail->addAddress($email, $adminName);
	
					if ($mail->send()){
						$msg = "Eine Aktivierungsmail wurde an ihre hinterlegte E-Mail-Adresse gesendet. Bitte aktivieren Sie ihren Account!";
						$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
										Eine Aktivierungsmail wurde an ihre hinterlegte E-Mail-Adresse gesendet. Bitte aktivieren Sie ihren Account!
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
					}else{
						$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
						$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
										Etwas ist schief gelaufen! Verifizierungsemail nicht gesendet! Bitte probieren Sie es nochmal.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									 </div>';
						$error = true;
					}

					//Beginne Transaktion, um Tabellen zu füllen.
					$pdo->beginTransaction();
					try{
						$stmt = $pdo->prepare("INSERT INTO User (Email, Passwort, Rolle, Aktiv, Token)
												VALUES (:email, :cryptPasswort, :rolle, :aktiv, :token)");
						$res  = $stmt->execute(array(':email' => $email, ':cryptPasswort' => $cryptPasswort, ':rolle' => $rolle, ':aktiv' => $aktiv, ':token' => $token));
				
						$stmt2 = $pdo->prepare("INSERT INTO Admin (Name, Email)
												VALUES (:adminName, :email)");
						$res2  = $stmt2->execute(array(':adminName' => $adminName, ':email' => $email));

						$stmt3 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
												VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
						$res3  = $stmt3->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
				
						//Erzeuge Lehrstuhl mit Admin Daten, um Funktionen der Lehrstühle tätigen zu können
						$lehrstuhlID = 1; //Die ID des Admins ist bei der Registrierung 1. Die ID des Admins wird für die Lehrstuhl_ID übernommen.
					
						$stmt4 = $pdo->prepare("INSERT INTO Lehrstuhl (Lehrstuhl_ID, Bezeichnung, Email)
												VALUES (:lehrstuhlID, :adminName, :email)");
						$res4  = $stmt4->execute(array(':lehrstuhlID' => $lehrstuhlID, ':adminName' => $adminName, ':email' => $email));

						//Erzeuge Studiendekan mit Admin Daten, um Funktionen des Studiendekans tätigen zu können
						$studiendekanID = 1; //Die ID des Admins ist bei der Registrierung 1. Die ID des Admins wird für die Studiendekan_ID übernommen.
						$imAmt = 1;
					
						$stmt5 = $pdo->prepare("INSERT INTO Studiendekan (Studiendekan_ID, Name, Email, ImAmt)
												VALUES (:studiendekanID, :adminName, :email, :imAmt)");
						$res5  = $stmt5->execute(array(':studiendekanID' => $studiendekanID, ':adminName' => $adminName, ':email' => $email, ':imAmt' => $imAmt));

						//Erzeuge ein Student mit Admin Daten, um Funktionen der Studenten tätigen zu können
						$studentID = 1; //Die ID des Admins ist bei der Registrierung 1. Die ID des Admins wird für die Student_ID übernommen.
						$studentVorname = "Admin";
						$fachsemester = 0;
						$studiengang = "Admin";
						$abschluss = " ";
						$seminarleistungen = 0;
					
						$stmt6 = $pdo->prepare("INSERT INTO Student (Student_ID, Name, Vorname, Fachsemester, Studiengang, Abschluss, Email, Seminarleistungen)
												VALUES (:studentID, :adminName, :studentVorname, :fachsemester, :studiengang, :abschluss, :email, :seminarleistungen)");
						$res6  = $stmt6->execute(array(':studentID' => $studentID, ':adminName' => $adminName, ':studentVorname' => $studentVorname, ':fachsemester' => $fachsemester,
														':studiengang' => $studiengang, ':abschluss' => $abschluss, ':email' => $email, ':seminarleistungen' => $seminarleistungen));

						$pdo->commit();
						$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
										Registrierung erfolgreich abgeschlossen.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
						$_SESSION['Error']  = $successMsg;
						if(!empty($successMsg2)){
							$_SESSION['Error2'] = $successMsg2;
						}else{
							$_SESSION['Error2'] = $errorMsg2;
						}
						header('Location:index.php');
						exit;
					}catch(Exception $e){
						echo $e->getMessage();
						$pdo->rollBack();
						$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Registrierung fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
						$error = true;
					}
				}
				if ($error == true){
					$_SESSION['Error']  = $errorMsg;
					$_SESSION['Error2'] = $errorMsg2;
					header("Location:index.php");
					exit;
				}
			}
		}
	
		//Registrierung Student
		if (isset ($_POST['studentRegistrierung'])){
			$studentID           = htmlentities($_POST['studentID']        ,ENT_QUOTES);
			$studentName         = htmlentities($_POST['studentName']      ,ENT_QUOTES);
			$studentVorname      = htmlentities($_POST['studentVorname']   ,ENT_QUOTES);
			$fachsemester        = htmlentities($_POST['fachsemester']     ,ENT_QUOTES);
			$studiengang         = htmlentities($_POST['studiengang']      ,ENT_QUOTES);
			$abschluss           = htmlentities($_POST['abschluss']        ,ENT_QUOTES);
			$email               = htmlentities($_POST['email']            ,ENT_QUOTES);
			$email = $email."@gw.uni-passau.de";
			$seminarleistungen   = htmlentities($_POST['seminarleistungen'],ENT_QUOTES);
			$passwort            = htmlentities($_POST['passwort']         ,ENT_QUOTES);
			$passwort2           = htmlentities($_POST['passwort2']        ,ENT_QUOTES);
			$token 				 = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';	
			$rolle               = 1;
			$aktiv 				 = 0;

			$error         = false;
		
			$stmt          = $pdo->prepare("SELECT * 
						      				FROM User
											JOIN Student ON Student.Email = User.Email
						     				WHERE User.Email = :email
												OR Student.Student_ID = :studentID");
			$result        = $stmt->execute(array(':email' => $email, ':studentID' => $studentID));
			$user          = $stmt->fetch();
		
			if ($user != false){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
							 Diese E-Mail oder Matrikelnummer ist bereits vorhanden.
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			}
			if (empty ($studentID)    || empty ($studentName) || empty ($studentVorname) ||
				empty ($fachsemester) || empty ($studiengang) || empty ($abschluss) ||
				empty ($email)        || empty ($passwort)    || empty ($passwort2)  ){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
							Alle Pflichtfelder müssen ausgefüllt werden. Registrierung fehlgeschlagen. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			}
			if ($passwort != $passwort2){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								 	Passwörter stimmen nicht überein. Registrierung fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>';
				$error = true;
			}
			if(strlen ($passwort) < 8 || strlen ($passwort2) < 8){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								 	Das Passwort muss mindestens 8 Zeichen enthalten. Registrierung fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>';
				$error = true;
			}

			if ($error == false){
				$cryptPasswort 					= password_hash ($passwort, PASSWORD_BCRYPT);
				$loginErfolgreich 				= 0; 
				$passwortaenderungErfolgreich 	= 1; 
				$falschesPasswort 				= 0;
				$token 							= str_shuffle($token);
				$token 							= substr($token, 0, 10);

				//Hisqis Upload
				$hisqisAuszug = $_FILES['hisqisAuszug'];
				
				$fileName    = $_FILES['hisqisAuszug']['name'];
				$fileTmpName = $_FILES['hisqisAuszug']['tmp_name'];
				$fileSize    = $_FILES['hisqisAuszug']['size'];
				$fileError   = $_FILES['hisqisAuszug']['error'];
				$fileType    = $_FILES['hisqisAuszug']['type'];

				$fileExt       = explode('.', $fileName);
				$fileActualExt = strtolower(end($fileExt));

				$allowed = array('pdf');

				//Error Alert ergänzen!!!
				if(in_array($fileActualExt, $allowed)){
					if($fileError === 0) {
						if($fileSize < 4000000){
							$fileNameNew = uniqid('', true).".".$fileActualExt;
							$fileDestination = 'uploads/'.$fileNameNew;
							move_uploaded_file($fileTmpName, $fileDestination);
							$filePath ="uploads/".$fileNameNew;
						}else{
							$errorMsg3 = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
										Datei ist zu groß.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										</div>';
							$error = true;
						}
					}else{
						$errorMsg3 = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								 	Problem mit dem Upload.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
						$error = true;
					}
				}else{
					/* vorerst auskommentiert, Bei nicht Upload wird diese Fehlermeldung ausgegeben.
					$errorMsg3 = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								 	Es sind nur PDF-Dateien erlaubt.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true; */
				}

				//Versand Verifizierungsmail Student	
				include_once "phpmailer/PHPMailerAutoload.php";
				
				//Instance für PHPMailer
                $mail = new PHPMailer();
                
                //Host fuer PHPMailer
                $mail->Host = "smtp.gmail.com";
                
                //Aktiviert SMTP
                $mail->isSMTP();
                
                //Authentifizierung
                $mail->SMTPAuth = true;
                
                //Login Details fuer GMAIL
                $mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
                $mail->Password = 'z1cfc4f5';

               	$mail->SMTPSecure = "tls";

               	//Port
               	$mail->Port = 587;

              	//Betreff
               	$mail->Subject = "Registrierung: Seminarvergabesystem Gruppe 5";

               	$mail->isHTML(true);

               	//Inhalt
               	$mail->Body = "
                    Willkommen zum Seminarvergabesystem der Gruppe 5,<br><br>
                    Sie haben sich erfolgreich als Student registriert. <br>
					Um Zugang zum System zu erhalten, aktivieren Sie bitte Ihren Account hier: <br>
                    
                    <a href='https://132.231.36.206/confirm.php?email=$email&token=$token'> hier</a>";

                //Absender
                $mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
               
                //Empfaenger
                $mail->addAddress($email, $studentName);

                if ($mail->send()){
					$msg = "Eine Aktivierungsmail wurde an ihre hinterlegte E-Mail-Adresse gesendet. Bitte aktivieren Sie ihren Account!";
					$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Eine Aktivierungsmail wurde an ihre hinterlegte E-Mail-Adresse gesendet. Bitte aktivieren Sie ihren Account!
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
				}else{
					$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
					$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
									Etwas ist schief gelaufen! Verifizierungsemail nicht gesendet! Bitte probieren Sie es nochmal oder wenden Sie sich an den Administrator.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								 </div>';
					$error = true;
				}
				
				//Beginne Transaktion, um Tabellen zu füllen.
				$pdo->beginTransaction();
				try{
					$stmt = $pdo->prepare("INSERT INTO User (Email, Passwort, Rolle, Aktiv, Token)
											VALUES (:email, :cryptPasswort, :rolle, :aktiv, :token)");
					$res  = $stmt->execute(array(':email' => $email, ':cryptPasswort' => $cryptPasswort, ':rolle' => $rolle, ':aktiv' => $aktiv, ':token' => $token));
				
					$stmt2 = $pdo->prepare("INSERT INTO Student (Student_ID, Name, Vorname, Fachsemester, Studiengang, Abschluss, Email, HISQIS_Auszug_Link, Seminarleistungen)
											VALUES (:studentID, :studentName, :studentVorname, :fachsemester, :studiengang, :abschluss, :email, :hisqisAuszug, :seminarleistungen)");
					$res2  = $stmt2->execute(array(':studentID' => $studentID, ':studentName' => $studentName, ':studentVorname' => $studentVorname, ':fachsemester' => $fachsemester,
												   ':studiengang' => $studiengang, ':abschluss' => $abschluss, ':email' => $email, ':hisqisAuszug' => $filePath, ':seminarleistungen' => $seminarleistungen));

					$stmt3 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
											VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
					$res3  = $stmt3->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
    
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
										Registrierung erfolgreich abgeschlossen.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					if(!empty($successMsg2)){
						$_SESSION['Error2'] = $successMsg2;
					}else{
						$_SESSION['Error2'] = $errorMsg2;
					}
					if(!empty($errorMsg3)){
						$_SESSION['Error3'] = $errorMsg3;
					}
					header('Location:index.php');
					exit;
						
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Registrierung fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				$_SESSION['Error2'] = $errorMsg2;
				$_SESSION['Error3'] = $errorMsg3;
				header("Location:registrierung.php");
				exit;
			}
		}
		
		//Login für alle User
		if(isset($_POST['login'])) {
			$email    = htmlentities($_POST['email']   ,ENT_QUOTES);
			$passwort = htmlentities($_POST['passwort'],ENT_QUOTES);
			$loginAusführen = false;
			
			$stmt     = $pdo->prepare("SELECT *, TIMESTAMPDIFF(SECOND, Letzter_Login, NOW())
											  AS TimeDiff
									   FROM User
									   INNER JOIN User_Historie ON User.Email = User_Historie.Email
									   WHERE User.Email = :email
									   ORDER BY User_Historie_ID DESC 
									   LIMIT 1");
			$result   = $stmt->execute(array(':email' => $email));
			$user     = $stmt->fetch();
			
			if($user['Aktiv'] == "0"){
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Login fehlgeschlagen. Bitte Account aktivieren.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>';
			}else{
				if($user['Rolle'] == 3){
					$stmt     = $pdo->prepare("SELECT ImAmt
												FROM Studiendekan
												WHERE Email = :email
												LIMIT 1");
					$result   = $stmt->execute(array(':email' => $email));
					$imAmt    = $stmt->fetch();
					
					if($imAmt['ImAmt'] == false){ 
						$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Sie haben als Studiendekan keinen Zugriff mehr auf das System, da Sie von Ihrem Nachfolger abgelöst wurden.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					}else{
						$loginAusführen = true;
					}
				}else{ //Alle Rollen außer Studiendekan
					$loginAusführen = true;
				}
				
				if($loginAusführen == true){
					$error = false;
					$loginErfolgreich = 0;
					
					if($user != false){
						$falschesPasswort = $user['Falsches_Passwort'];
						$verbleibendeVersuche = 9;
						$verbleibendeVersuche = $verbleibendeVersuche - $falschesPasswort;
				
						if($falschesPasswort >= 6 && $falschesPasswort < 10){
							$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											 Verbleibende Versuche zur Eingabe des richtigen Passwortes: <strong>'.$verbleibendeVersuche.' </strong></br>
												Bei folgender falscher Eingabe ist die Entsperrung nur beim Admin möglich.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>';
						}
						if($falschesPasswort == 10){
							$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											Keine weiteren Versuche zur Eingabe des richtigen Passwortes. </br>
												Zum Entsperren des Accounts bitte den <strong>Admin</strong> kontaktieren.  
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>';

							//Versand Mail Nutzer gesperrt
							include_once "phpmailer/PHPMailerAutoload.php";
							
							//Instance für PHPMailer
			                $mail = new PHPMailer();
			                
			                //Host fuer PHPMailer
			                $mail->Host = "smtp.gmail.com";
			                
			                //Aktiviert SMTP
			                $mail->isSMTP();
			                
			                //Authentifizierung
			                $mail->SMTPAuth = true;
			                
			                //Login Details fuer GMAIL
			                $mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
			                $mail->Password = 'z1cfc4f5';

			               	$mail->SMTPSecure = "tls";

			               	//Port
			               	$mail->Port = 587;

			              	//Betreff
			               	$mail->Subject = "Sperrung: Seminarvergabesystem Gruppe 5";

			               	$mail->isHTML(true);

			               	//Inhalt
			               	$mail->Body = "
			                    Sehr geehrte/r Nutzer/in,<br><br>

								Ihr Account wurde aus Sicherheitsgr&uuml;nden gesperrt, da ihr Passwort mehr als zehn mal falsch eingegeben wurde.<br><br> 
								<b>Bitte wenden Sie sich an den Administrator.</b>

								<a href='https://132.231.36.206/index.php'> Weiter zur Webseite</a>
								
			                    
			                    ";

			                //Absender
			                $mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
			               
			                //Empfaenger
			                $mail->addAddress($email);

			                if ($mail->send()){
								$msg = "Sie wurden gesperrt!";
							}else{
								$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
								$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
												Etwas ist schief gelaufen! E-Mail zum Passwort zurücksetzen nicht gesendet. Bitte probieren Sie es nochmal.
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											  </div>';
								$error = true;
							}
						}else
						if($falschesPasswort < 10){
							if($user['TimeDiff'] >= 2){
								if (password_verify($passwort, $user['Passwort'])) {
									$_SESSION['Email']          = $user['Email'];
									$_SESSION['Rolle']          = $user['Rolle'];
									$falschesPasswort = 0;
									$loginErfolgreich = 1;
									$error = true;
									
									$letzterLogin = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Falsches_Passwort)	  
																	VALUES (:email, NOW(), :loginErfolgreich, :falschesPasswort)");
									$login  = $letzterLogin->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':falschesPasswort' => $falschesPasswort));
		
									
									header("Location:index.php");
									exit;
								}else{
									$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											E-Mail oder Passwort falsch.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>';
									$falschesPasswort++;
								}		
							}else{
								$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											Anmeldung fehlgeschlagen.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>';
								$falschesPasswort++;
							}
						}
						
						if($user['Rolle'] == 4){
							$falschesPasswort = 0;
						}
		
						$letzterLogin = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Falsches_Passwort)	  
														VALUES (:email, NOW(), :loginErfolgreich, :falschesPasswort)");
						$login  = $letzterLogin->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':falschesPasswort' => $falschesPasswort));
		
					}else{
						if(empty ($passwort) || empty ($email)){
							$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											E-Mail und Password sind Pflichtfelder.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>';
						}else{
							$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											E-Mail oder Passwort ungültig.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>';
						}
					}	
				}
			}
			if($loginAusführen == false || $error == false){
				$_SESSION['Error'] = $errorMsg;
				$_SESSION['Error2'] = $errorMsg2;
				header("Location:index.php");
				exit;
			}
		}
	
		//Passwort zurücksetzen
		if (isset ($_POST['passwortZuruecksetzen'])){
				$id           = htmlentities($_POST['id']          ,ENT_QUOTES);
				$email        = htmlentities($_POST['email']       ,ENT_QUOTES);
				$neuPasswortGen = 'qwertzuiop4([]K2?#asdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
				$neuPasswort  = str_shuffle($neuPasswortGen);
				$neuPasswort  =substr($neuPasswort, 0, 10);
		
				$error    = false;
			
				if (empty ($id) || empty ($email)){
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	ID oder Matrikelnummer und E-Mail bitte angeben.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				} 
				
				$stmt     = $pdo->prepare("SELECT *
											FROM User
											WHERE Email = :email
											LIMIT 1");
				$result   = $stmt->execute(array(':email' => $email));
				$user     = $stmt->fetch();
					
				if ($user == false){
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Diese E-Mail ist nicht vorhanden oder falsch.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}
				
				if ($error == false){
					$rolle = $user['Rolle'];
					$error2 = false;
					
					if ($rolle == 1){
						$stmt     = $pdo->prepare("SELECT Student_ID
													FROM Student
													WHERE Student_ID = :id
													LIMIT 1");
						$result   = $stmt->execute(array(':id' => $id));
						$student  = $stmt->fetch();
						
						if ($student == false){
							$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											Die E-Mail Adresse und/oder die Id oder Matrikelnummer sind ungültig.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error2 = true;
						}
					}	

					if ($rolle == 2){
						$stmt     = $pdo->prepare("SELECT Lehrstuhl_ID
													FROM Lehrstuhl
													WHERE Lehrstuhl_ID = :id
													LIMIT 1");
						$result   = $stmt->execute(array(':id' => $id));
						$lehrstuhl  = $stmt->fetch();
						
						if ($lehrstuhl == false){
							$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											Die E-Mail Adresse und/oder die Id oder Matrikelnummer sind ungültig.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error2 = true;
						}
					}
					
					if ($rolle == 3){
						$stmt     = $pdo->prepare("SELECT Studiendekan_ID
													FROM Studiendekan
													WHERE Studiendekan_ID = :id
													LIMIT 1");
						$result   = $stmt->execute(array(':id' => $id));
						$studiendekan  = $stmt->fetch();
						
						if ($studiendekan == false){
							$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											Die E-Mail Adresse und/oder die Id oder Matrikelnummer sind ungültig.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error2 = true;
						}
					}
					
					if ($rolle == 4){
						$stmt     = $pdo->prepare("SELECT Admin_ID
													FROM Admin
													WHERE Admin_ID = :id
													LIMIT 1");
						$result   = $stmt->execute(array(':id' => $id));
						$admin  = $stmt->fetch();
						
						if ($admin == false){
							$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											Die E-Mail Adresse und/oder die Id oder Matrikelnummer sind ungültig.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error2 = true;
						}
					}
					
					if ($error2 == true){
							$stmt     = $pdo->prepare("SELECT Falsches_Passwort
														FROM User_Historie
														WHERE Email = :email
														LIMIT 1");
							$result   = $stmt->execute(array(':email' => $email));
							$passwortFalsch  = $stmt->fetch();
							
							$loginErfolgreich = 0;
							$passwortaenderungErfolgreich = 0;
							$falschesPasswort = $passwortFalsch['Falsches_Passwort'];
						
							$stmt2 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
													VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
							$neueUserHistorie  = $stmt2->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
					}
					
					if ($error2 == false){
							$cryptedPass = password_hash ($neuPasswort, PASSWORD_BCRYPT);
						
							//Versand Mail Passwort Reset	
							include_once "phpmailer/PHPMailerAutoload.php";
							
							//Instance für PHPMailer
			                $mail = new PHPMailer();
			                
			                //Host fuer PHPMailer
			                $mail->Host = "smtp.gmail.com";
			                
			                //Aktiviert SMTP
			                $mail->isSMTP();
			                
			                //Authentifizierung
			                $mail->SMTPAuth = true;
			                
			                //Login Details fuer GMAIL
			                $mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
			                $mail->Password = 'z1cfc4f5';

			               	$mail->SMTPSecure = "tls";

			               	//Port
			               	$mail->Port = 587;

			              	//Betreff
			               	$mail->Subject = "Passwort zur&uuml;cksetzen: Seminarvergabesystem Gruppe 5";

			               	$mail->isHTML(true);

			               	//Inhalt
			               	$mail->Body = "
			                    Sehr geehrte/r Nutzer/in,<br><br>
								Sie haben Ihr Passwort erfolgreich zur&uuml;ckgesetzt. <br>
			                    Anbei ist ihr neues Passwort: $neuPasswort<br><br>

			                    Bitte beachten Sie, dass Sie ihr neues Passwort im Reiter Profil sofort &auml;ndern!<br><br>

			                    <a href='https://132.231.36.206/index.php'> Weiter zur Webseite</a>
			                    
			                    ";

			                //Absender
			                $mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
			               
			                //Empfaenger
			                $mail->addAddress($email);

			                if ($mail->send()){
								$msg = "Ihr neues Passwort wurde an Ihre E-Mail-Adresse gesendet!";
								$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
												Ihr neues Passwort wurde an Ihre E-Mail-Adresse gesendet!
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												</div>';
							}else{
								$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
								$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
												Etwas ist schief gelaufen! E-Mail zum Passwort zurücksetzen nicht gesendet. Bitte probieren Sie es nochmal.
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											  </div>';
								$error = true;
							}

							//Update Tabellen
							$stmt = $pdo->prepare("UPDATE User 
													SET Passwort = :cryptedPass
													WHERE email = :email
													LIMIT 1");
							$setzeNeuesPasswort  = $stmt->execute(array(':cryptedPass' => $cryptedPass, ':email' => $email));
							
							$stmt     = $pdo->prepare("SELECT Falsches_Passwort
														FROM User_Historie
														WHERE Email = :email
														LIMIT 1");
							$result   = $stmt->execute(array(':email' => $email));
							$passwortFalsch  = $stmt->fetch();
							
							$loginErfolgreich = 0;
							$passwortaenderungErfolgreich = 1;
							$falschesPasswort = $passwortFalsch['Falsches_Passwort'];
						
							$stmt2 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
													VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
							$neueUserHistorie  = $stmt2->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
    						
							$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
												 Zurücksetzen des Passworts erfolgreich.
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												</div>';
							$_SESSION['Error']  = $successMsg;
							if(!empty($successMsg2)){
								$_SESSION['Error2'] = $successMsg2;
							}else{
								$_SESSION['Error2'] = $errorMsg2;
							}
							header('Location:index.php');
							exit;
					}
				}
				if ($error == true || $error2 == true){
					$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 	Zurücksetzen des Passworts fehlgeschlagen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$_SESSION['Error']  = $errorMsg;
					$_SESSION['Error2'] = $errorMsg2;
					header("Location:passwortZuruecksetzen.php");
					exit;
				}
		}
	
	
		
	//Folgende Aktionen sind nur im eingeloggten Zustand im System möglich		
	if(!isset($_SESSION['Email'])){	
			echo '<h1> </h1>';
	}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];

		
		//Logout für alle User
		if (isset ($_POST['logout'])){	 
			if(!isset($_SESSION['Email'])) {
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Nicht im System angemeldet, um auszuloggen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$_SESSION['Error'] = $errorMsg;
				header("Location:index.php");
				exit;
			}else{	
				session_destroy();
				//Die Meldung wird nicht übergeben, da keine Session mehr besteht und die Meldung deshalb nicht in der Session gespeichert werden kann.
				$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
								Abmeldung erfolgreich.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
				$_SESSION['Error'] = $successMsg;
				header('Location:index.php');
				exit;
			}
		}
		
		//Passwortänderung
		if (isset ($_POST['passwortAenderung'])){
				$altPasswort  = htmlentities($_POST['altPasswort'] ,ENT_QUOTES);
				$neuPasswort  = htmlentities($_POST['neuPasswort'] ,ENT_QUOTES);
				$neuPasswort2 = htmlentities($_POST['neuPasswort2'],ENT_QUOTES);
				
				$error    = false;
			
				if (empty ($altPasswort) || empty ($neuPasswort) || empty ($neuPasswort2)){
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Alle Passwort-Felder müssen ausfegüllt werden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
				if(strlen ($neuPasswort) < 8 || strlen ($neuPasswort2) < 8){
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Das neue Passwort muss mindestens 8 Zeichen enthalten.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
				if ($neuPasswort != $neuPasswort2){
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
									Passwörter stimmen nicht überein.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}
	
				if ($error == false){
					$Stmt     = $pdo->prepare("SELECT Passwort
												FROM User 
												WHERE Email = :email");
					$result   = $Stmt ->execute(array(':email' => $email));
					$altesPasswort = $Stmt ->fetch();
				
					if (password_verify($altPasswort, $altesPasswort['Passwort'])) {
							$cryptedPass = password_hash ($neuPasswort, PASSWORD_BCRYPT);
						
							$stmt = $pdo->prepare("UPDATE User 
													SET Passwort = :cryptedPass
													WHERE email = :email
													LIMIT 1");
							$setzeNeuesPasswort  = $stmt->execute(array(':cryptedPass' => $cryptedPass, ':email' => $email));
							
							$loginErfolgreich = 1;
							$passwortaenderungErfolgreich = 1;
							$falschesPasswort = 0;
						
							$stmt3 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
													VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
							$neueUserHistorie  = $stmt3->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
							
							$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
											Änderung des Passworts erfolgreich.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										  </div>';
							$_SESSION['Error']  = $successMsg;
							header('Location:profil.php');
							exit;
					}else{
						$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
									Das angebene Passwort ist falsch.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
						$error = true;
					}
				}
				
				if ($error == true){
				//Beginne Transaktion
				$pdo->beginTransaction();
				try{
						$loginErfolgreich = 1;
						$passwortaenderungErfolgreich = false;
						$falschesPasswort = 0;
						
						$stmt = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
												VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
						$neueUserHistorie  = $stmt->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
    
					$pdo->commit();
					$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
									Passwortänderung ist fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								 </div>';
					$_SESSION['Error']  = $errorMsg;
					$_SESSION['Error2'] = $errorMsg2;
					header("Location:passwortAenderung.php");
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
									Passwortänderung ist fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								 </div>';
					$_SESSION['Error']  = $errorMsg;
					$_SESSION['Error2'] = $errorMsg2;
					header("Location:passwortAenderung.php");
					exit;
				}
			}
		}
		
		//Testmodus Aktivieren
		if (isset ($_POST['testAktivieren'])){
			$adminEmail = htmlentities($_POST['adminEmail'],ENT_QUOTES);
			$testmodus = 1;
			
			$error         = false;
		
			if (empty ($adminEmail)){
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Daten wurden nicht richtig übermittelt. Testmodus aktivieren fehlgeschlagen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			}
			if ($error == false){
				//Beginne Transaktion, um Tabelle upzudaten.
				$pdo->beginTransaction();
				try{
					$testmodusAktivieren = $pdo->prepare("UPDATE Admin
														  SET  Testmodus = :testmodus
														  WHERE Email = :adminEmail
														  LIMIT 1");
					$aktivieren  = $testmodusAktivieren->execute(array(':testmodus' => $testmodus,':adminEmail' => $adminEmail));
			
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Testmodus wurde aktiviert.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:profil.php');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Testmodus aktivieren fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header('Location:profil.php');
				exit;
			}
		}
		
		//Testmodus Deaktivieren
		if (isset ($_POST['testDeaktivieren'])){
			$adminEmail = htmlentities($_POST['adminEmail'],ENT_QUOTES);
			$testmodus = 0;
			
			$error         = false;
		
			if (empty ($adminEmail)){
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Daten wurden nicht richtig übermittelt. Testmodus deaktivieren fehlgeschlagen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			}
			if ($error == false){
				//Beginne Transaktion, um Tabelle upzudaten.
				$pdo->beginTransaction();
				try{
					$testmodusAktivieren = $pdo->prepare("UPDATE Admin
														  SET  Testmodus = :testmodus
														  WHERE Email = :adminEmail
														  LIMIT 1");
					$aktivieren  = $testmodusAktivieren->execute(array(':testmodus' => $testmodus,':adminEmail' => $adminEmail));
			
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Testmodus wurde deaktiviert.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:profil.php');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Testmodus deaktivieren fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header('Location:profil.php');
				exit;
			}
		}
		
		//Nutzerdaten Bearbeiten
		if (isset ($_POST['userBearbeiten'])){
			$userEmail             = $_GET['User_Email'];
			
			$adminName             = htmlentities($_POST['adminName']           ,ENT_QUOTES);
			
			$studentID             = htmlentities($_POST['studentID']           ,ENT_QUOTES);
			$studentName           = htmlentities($_POST['studentName']         ,ENT_QUOTES);
			$studentVorname        = htmlentities($_POST['studentVorname']      ,ENT_QUOTES);
			$fachsemester          = htmlentities($_POST['fachsemester']        ,ENT_QUOTES);
			$studiengang           = htmlentities($_POST['studiengang']         ,ENT_QUOTES);
			$abschluss             = htmlentities($_POST['abschluss']           ,ENT_QUOTES);
			$seminarleistungen     = htmlentities($_POST['seminarleistungen']   ,ENT_QUOTES);
			
			$lehrstuhlBezeichnung  = htmlentities($_POST['lehrstuhlBezeichnung'],ENT_QUOTES);
			
			$studiendekanName      = htmlentities($_POST['studiendekanName']    ,ENT_QUOTES);
			
			$error         = false;
		  
			if(!empty($userEmail)){
				$userRolle = $pdo->prepare("SELECT Rolle
											FROM User
											WHERE Email = :userEmail");
				$result	   = $userRolle->execute(array(':userEmail' => $userEmail));
				$userRolle     = $userRolle->fetch();
	
				if($userRolle['Rolle'] == "4"){
					if(empty ($adminName)){
						$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
										Admin Name fehlt. Bearbeiten der Daten fehlgeschlagen.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
						$error = true;
					}else{
						$adminBearbeiten = $pdo->prepare("UPDATE Admin 
															SET Name = :adminName
															WHERE Email = :userEmail
															LIMIT 1");
						$bearbeiten  = $adminBearbeiten->execute(array(':adminName' => $adminName, ':userEmail' => $userEmail));
					}
				}else{
					if($userRolle['Rolle'] == "1"){
						if(empty ($studentID)   || empty( $studentName) || empty($studentVorname) || 
						   empty ($fachsemester)|| empty ($studiengang) || empty($abschluss) ){
							$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											Bitte alle Daten des Studenten angeben. Bearbeiten der Daten fehlgeschlagen.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error = true;
						}else{
							$studentIDAbfrage = $pdo->prepare("SELECT Student_ID, Email
																FROM  Student 
																WHERE Student_ID = :studentID");
							$result	   		  = $studentIDAbfrage->execute(array(':studentID' => $studentID));
							$studentIDNummer  = $studentIDAbfrage->fetch();
							
							if($studentIDNummer['Email'] != $userEmail){
								$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											Diese Matrikelnummer ist bereits vergeben. Bearbeiten der Daten fehlgeschlagen.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
								$error = true;
							}else{
								$studentBearbeiten = $pdo->prepare("UPDATE Student 
																	SET Student_ID = :studentID, Name = :studentName, Vorname = :studentVorname,
																		Fachsemester = :fachsemester, Studiengang = :studiengang, Abschluss = :abschluss, Seminarleistungen = :seminarleistungen
																	WHERE Email = :userEmail
																	LIMIT 1");
								$bearbeiten  = $studentBearbeiten->execute(array(':studentID' => $studentID, ':studentName' => $studentName, ':studentVorname' => $studentVorname,
																				':fachsemester' => $fachsemester, 'studiengang' => $studiengang, ':abschluss' => $abschluss, 
																				':seminarleistungen' => $seminarleistungen, ':userEmail' => $userEmail));
							}
						}
					}
					if($userRolle['Rolle'] == "2"){
						if(empty($lehrstuhlBezeichnung) ){
							$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											Bitte die Bezeichnung des Lehrstuhls angeben. Bearbeiten der Daten fehlgeschlagen.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error = true;
						}else{
							$lehrstuhlBearbeiten = $pdo->prepare("UPDATE Lehrstuhl 
																	SET Bezeichnung = :lehrstuhlBezeichnung
																	WHERE Email = :userEmail
																	LIMIT 1");
							$bearbeiten  = $lehrstuhlBearbeiten->execute(array(':lehrstuhlBezeichnung' => $lehrstuhlBezeichnung, ':userEmail' => $userEmail));
						}
					}
					if($userRolle['Rolle'] == "3"){
						if(empty($studiendekanName) ){
							$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
											Bitte den Namen des Studiendekans angeben. Bearbeiten der Daten fehlgeschlagen.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
							$error = true;
						}else{
							$studiendekanBearbeiten = $pdo->prepare("UPDATE Studiendekan 
																	SET Name = :studiendekanName
																	WHERE Email = :userEmail
																	LIMIT 1");
							$bearbeiten  = $studiendekanBearbeiten->execute(array(':studiendekanName' => $studiendekanName, ':userEmail' => $userEmail));
						}
					}
				}
			}else{
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Bearbeiten der Daten fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
	
			if ($error == false){
				
				if($bearbeiten){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Der Nutzer mit der E-Mail '.$userEmail.' wurde erfolgreich bearbeitet.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header("Location:userBearbeiten.php?Email=".$userEmail." ");
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 Bearbeitung des Nutzers fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				} 
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header("Location:userBearbeiten.php?Email=".$userEmail." ");
				exit;
			}
		}
		
		//Nutzer Entsperren
		if (isset ($_POST['userEntsperren'])){
			$userEmail    					= htmlentities($_POST['userEmail'],ENT_QUOTES);
			
			$loginErfolgreich 				= 0;
			$passwortaenderungErfolgreich 	= 0;
			$falschesPasswort 	  			= 0;
			$neuPasswortGen 				= 'qwertzuiop4([]K2?#asdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
			$neuPasswort  					= str_shuffle($neuPasswortGen);
			$neuPasswort  					= substr($neuPasswort, 0, 10);
			$cryptedPass 					= password_hash ($neuPasswort, PASSWORD_BCRYPT);
						
			$stmt = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
									VALUES (:userEmail, NOW(), :loginErfolgreich, :passwortaenderungErfolgreich, :falschesPasswort)");
			$entsperren  = $stmt->execute(array(':userEmail' => $userEmail, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));

			$stmt = $pdo->prepare("UPDATE User 
													SET Passwort = :cryptedPass
													WHERE email = :email
													LIMIT 1");
							$setzeNeuesPasswort  = $stmt->execute(array(':cryptedPass' => $cryptedPass, ':email' => $userEmail));
    
				if($entsperren){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Der Nutzer mit der E-Mail '.$userEmail.' wurde entsperrt
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
						
							//Versand Mail Nutzer entsperrt	
							include_once "phpmailer/PHPMailerAutoload.php";
							
							//Instance für PHPMailer
			                $mail = new PHPMailer();
			                
			                //Host fuer PHPMailer
			                $mail->Host = "smtp.gmail.com";
			                
			                //Aktiviert SMTP
			                $mail->isSMTP();
			                
			                //Authentifizierung
			                $mail->SMTPAuth = true;
			                
			                //Login Details fuer GMAIL
			                $mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
			                $mail->Password = 'z1cfc4f5';

			               	$mail->SMTPSecure = "tls";

			               	//Port
			               	$mail->Port = 587;

			              	//Betreff
			               	$mail->Subject = "Entsperrung: Seminarvergabesystem Gruppe 5";

			               	$mail->isHTML(true);

			               	//Inhalt
			               	$mail->Body = "
			                    Sehr geehrte/r Nutzer/in,<br><br>

								Ihr Account wurde vom Administrator entsperrt. <br>
								Aus Sicherheitsgr&uuml;nden wurde ihr Passwort neu vergeben:<br><br>
								<b>Passwort:</b> $neuPasswort <br><br>

								Bitte &auml;ndern Sie ihr Passwort nach dem ersten Login im Reiter Profil. <br><br>

								<a href='https://132.231.36.206/index.php'> Weiter zur Webseite</a>
								
			                    
			                    ";

			                //Absender
			                $mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
			               
			                //Empfaenger
			                $mail->addAddress($userEmail);

			                if ($mail->send()){
								$msg = "Eine Email mit dem neuen Passwort wurde an die E-Mail-Adresse des Nutzers gesendet.";
								$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
												Eine E-Mail mit dem neuen Passwort wurde an die E-Mail-Adresse des Nutzers gesendet.
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												</div>';
							}else{
								$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
								$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
												Etwas ist schief gelaufen! E-Mail zur Benachrichtigung der Entsperrung des Accounts nicht gesendet.
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											  </div>';
								$error = true;
							}

					header('Location:gesperrteUser.php');
					exit;
				}else{
					echo "</br> Entsperren des Nutzers fehlgeschlagen. Bitte erneut versuchen.";
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Entsperren des Nutzers fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$_SESSION['Error']  = $errorMsg;
					header("Location:gesperrteUser.php");
					exit;
				}
		}
		
		//Nutzer Löschen
		if (isset ($_POST['userLoeschen'])){
			$userEmail    = htmlentities($_POST['userEmail'],ENT_QUOTES);
			$userRolle    = htmlentities($_POST['userRolle'],ENT_QUOTES);
				
			$pdo->beginTransaction();
			try{
				if($userRolle == "1"){
					$studentDatenLoeschen = $pdo->prepare("DELETE FROM Student 
															WHERE Email = :userEmail ");
					$studentLoeschen      = $studentDatenLoeschen->execute(array(':userEmail' => $userEmail));
				}
				if($userRolle == "2"){
					$lehrstuhlDatenLoeschen = $pdo->prepare("DELETE FROM Lehrstuhl 
															WHERE Email = :userEmail ");
					$lehrstuhlLoeschen      = $lehrstuhlDatenLoeschen->execute(array(':userEmail' => $userEmail));
				}
				if($userRolle == "3"){
					$studiendekanDatenLoeschen = $pdo->prepare("DELETE FROM Studiendekan 
																WHERE Email = :userEmail ");
					$studiendekanLoeschen      = $studiendekanDatenLoeschen->execute(array(':userEmail' => $userEmail));
				}
				
				$nutzerHistorieLoeschen = $pdo->prepare("DELETE FROM User_Historie 
														 WHERE Email = :userEmail ");
				$historieLoeschen       = $nutzerHistorieLoeschen->execute(array(':userEmail' => $userEmail));
				
				$nutzerLoeschen = $pdo->prepare("DELETE FROM User 
												  WHERE Email = :userEmail ");
				$loeschen        = $nutzerLoeschen->execute(array(':userEmail' => $userEmail));
			
				$pdo->commit();
				$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
								Nutzer mit der E-Mail '.$userEmail.' erfolgreich aus dem System gelöscht.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							  </div>';
				$_SESSION['Error']  = $successMsg;
				header('Location:alleUser.php');
				exit;
					
			}catch(Exception $e){
				echo $e->getMessage();
				$pdo->rollBack();
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
							Löschen des Nutzers mit der E-Mail '.$userEmail.' fehlgeschlagen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$_SESSION['Error']  = $errorMsg;
				header("Location:alleUser.php");
				exit;
			}
		}
		
		//Studiendekan Anlegen
		if (isset ($_POST['studiendekanAnlegen'])){
			$studiendekanID       = htmlentities($_POST['studiendekanID']  ,ENT_QUOTES);
			$studiendekanName     = htmlentities($_POST['studiendekanName'],ENT_QUOTES);
			$email                = htmlentities($_POST['email']           ,ENT_QUOTES);
			$email = $email."@uni-passau.de";
			$PasswortGen 	  	  = 'qwertzuiop4([]K2?#asdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
			$passwort         	  = str_shuffle($PasswortGen);
			$passwort             = substr($passwort, 0, 10);
			$token 				  = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';	
			$rolle                = 3;
			$aktiv                = 0;
			
			$error         = false;
			
			$stmt          = $pdo->prepare("SELECT * 
						      				FROM User
											JOIN Studiendekan ON Studiendekan.Email = User.Email
						     				WHERE User.Email = :email
												OR Studiendekan_ID = :studiendekanID");
			$result        = $stmt->execute(array(':email' => $email, ':studiendekanID' => $studiendekanID));
			$user          = $stmt->fetch();
		
			if ($user != false){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Diese E-Mail oder Studiendekan ID ist bereits vorhanden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;   
			}

			if (empty ($studiendekanID) || empty ($studiendekanName) || empty($email)){
				$errorMsg = '<br> <div class="alert alert-warning alert-dismissible fade show top50" role="alert">
									Alle Pflichtfelder müssen ausgefüllt werden.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								  </div>';

				$error = true;
			}
	
			if ($error == false){
				$cryptPasswort = password_hash ($passwort, PASSWORD_BCRYPT);
				$loginErfolgreich             = 0; 
				$passwortaenderungErfolgreich = 1; 
				$falschesPasswort             = 0;
				$token 						  = str_shuffle($token);
				$token 						  = substr($token, 0, 10);
				$imAmt                        = 1;
				
				//Versand Verifizierungsmail Dekan	
				include_once "phpmailer/PHPMailerAutoload.php";
				
				//Instance für PHPMailer
                $mail = new PHPMailer();
                
                //Host fuer PHPMailer
                $mail->Host = "smtp.gmail.com";
                
                //Aktiviert SMTP
                $mail->isSMTP();
                
                //Authentifizierung
                $mail->SMTPAuth = true;
                
                //Login Details fuer GMAIL
                $mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
                $mail->Password = 'z1cfc4f5';

               	$mail->SMTPSecure = "tls";

               	//Port
               	$mail->Port = 587;

              	//Betreff
               	$mail->Subject = "Registrierung: Seminarvergabesystem Gruppe 5";

               	$mail->isHTML(true);

               	//Inhalt
               	$mail->Body = "
                    Willkommen zum Seminarvergabesystem der Gruppe 5,<br><br>
					
                    Sie wurden vom Administrator als Studiendekan im Seminarvergabesystem registriert. <br>
					Um Zugang zum System zu erhalten, aktivieren Sie bitte Ihren Account 
                    
                    <a href='https://132.231.36.206/confirm.php?email=$email&token=$token'> hier</a>.

					<br><br>
					Anbei ist auch Ihre vom System vergebene ID: $studiendekanID
					<br>
					und ihr vorl&auml;ufiges Passwort: 				 $passwort
					<br><br>
					Bitte beachten Sie, dass Sie ihr Passwort nach der ersten Anmeldung unter dem Reiter Profil sofort &auml;ndern!";

                //Absender
                $mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
               
                //Empfaenger
                $mail->addAddress($email, $studiendekanName);

				if ($mail->send()){
						$msg = "Der Dekan ist nun registiert. Bitte erinnern Sie den Dekan daran, dass sein Account vor der Nutzung aktiviert werden muss!";
						$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
										Der Dekan ist nun registiert. Bitte erinnern Sie den Dekan daran, dass sein Account vor der Nutzung aktiviert werden muss!
										Eine E-Mail zur Aktivierung des Accounts wurde gesendet.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
				}else{
						$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
						$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
										Etwas ist schief gelaufen! Verifizierungsemail nicht gesendet! Bitte probieren Sie es nochmal.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									 </div>';
						$error = true;
				}
				
				//Beginne Transaktion, um Tabellen zu füllen.
				$pdo->beginTransaction();
				try{
						$stmt     = $pdo->prepare("SELECT COUNT(ImAmt)
													FROM Studiendekan
													WHERE ImAmt = '1' ");
						$result   = $stmt->execute(array());
						$dekane   = $stmt->fetch();
						
						//Der vorherige Studiendekan wird aus dem System gesperrt, in dem ImAmt = 0 gesetzt wird.
						if($dekane[0] >= "2"){
							$stmt = $pdo->prepare("UPDATE Studiendekan 
													SET ImAmt = '0'
													WHERE (SELECT MAX(Letzter_Login)
															FROM User_Historie
															LIMIT 1)");
							$sperreAltenDekan  = $stmt->execute(array());
						}
					
					$stmt = $pdo->prepare("INSERT INTO User (Email, Passwort, Rolle, Aktiv, Token)
											VALUES (:email, :cryptPasswort, :rolle, :aktiv, :token)");
					$res  = $stmt->execute(array(':email' => $email, ':cryptPasswort' => $cryptPasswort, ':rolle' => $rolle, ':aktiv' => $aktiv, ':token' => $token));
				
					$stmt2 = $pdo->prepare("INSERT INTO Studiendekan (Studiendekan_ID, Name, Email, ImAmt)
											VALUES (:studiendekanID, :studiendekanName, :email, :imAmt)");
					$res2  = $stmt2->execute(array(':studiendekanID' => $studiendekanID, ':studiendekanName' => $studiendekanName, ':email' => $email, ':imAmt' => $imAmt));

					$stmt3 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
											VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
					$res3  = $stmt3->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
    
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
						 	Anlegen des Studiendekans '.$studiendekanName.' mit der ID '.$studiendekanID.' und der E-Mail '.$email.' erfolgreich abgeschlossen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
					</div>';
					$_SESSION['Error']  = $successMsg;
					if(!empty($successMsg2)){
						$_SESSION['Error2'] = $successMsg2;
					}else{
						$_SESSION['Error2'] = $errorMsg2;
					}
					header('Location:studiendekanAnlegen.php');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Anlegen des Studiendekans fehlgeschlagen. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
					</div>';
					$error = true;
				}
			}
			
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				$_SESSION['Error2'] = $errorMsg2;
				header("Location:studiendekanAnlegen.php");
				exit;
			}

		}
	
		//Lehrstuhl Anlegen
		if (isset ($_POST['lehrstuhlAnlegen'])){
			$lehrstuhlID          = htmlentities($_POST['lehrstuhlID']         ,ENT_QUOTES);
			$lehrstuhlBezeichnung = htmlentities($_POST['lehrstuhlBezeichnung'],ENT_QUOTES);
			$email                = htmlentities($_POST['email']               ,ENT_QUOTES);
			$email = $email."@uni-passau.de";
			$PasswortGen 	      = 'qwertzuiop4([]K2?#asdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';
			$passwort         	  = str_shuffle($PasswortGen);
			$passwort             = substr($passwort, 0, 10);
			$token 				  = 'qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM0123456789!$/()*';	
			$rolle                = 2;
			$aktiv                = 0;
			
			$error         = false;
			
			$stmt          = $pdo->prepare("SELECT * 
						      				FROM User
											JOIN Lehrstuhl ON Lehrstuhl.Email = User.Email
						     				WHERE User.Email = :email
												OR Lehrstuhl_ID = :lehrstuhlID");
			$result        = $stmt->execute(array(':email' => $email, ':lehrstuhlID' => $lehrstuhlID));
			$user          = $stmt->fetch();
		
			if ($user != false){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Diese E-Mail oder Lehrstuhl ID ist bereits vorhanden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if (empty ($lehrstuhlID) || empty ($lehrstuhlBezeichnung) || empty($email)){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
						 	Alle Pflichtfelder müssen ausgefüllt werden.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
					</div>';
				$error = true;
			}
	
			if ($error == false){
				$cryptPasswort = password_hash ($passwort, PASSWORD_BCRYPT);
				$loginErfolgreich             = 0; 
				$passwortaenderungErfolgreich = 1; 
				$falschesPasswort             = 0;
				$token 						  = str_shuffle($token);
				$token 						  = substr($token, 0, 10);
				
				//Versand Verifizierungsmail Lehrstuhl	
				include_once "phpmailer/PHPMailerAutoload.php";
				
				//Instance für PHPMailer
                $mail = new PHPMailer();
                
                //Host fuer PHPMailer
                $mail->Host = "smtp.gmail.com";
                
                //Aktiviert SMTP
                $mail->isSMTP();
                
                //Authentifizierung
                $mail->SMTPAuth = true;
                
                //Login Details fuer GMAIL
                $mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
                $mail->Password = 'z1cfc4f5';

               	$mail->SMTPSecure = "tls";

               	//Port
               	$mail->Port = 587;

              	//Betreff
               	$mail->Subject = "Registrierung: Seminarvergabesystem Gruppe 5";

               	$mail->isHTML(true);

               	//Inhalt
               	$mail->Body = "
                    Willkommen zum Seminarvergabesystem der Gruppe 5,<br><br>
                    
					Sie wurden vom Administrator als Lehrstuhl im Seminarvergabesystem registriert. </br>
					Um Zugang zum System zu erhalten, aktivieren Sie bitte Ihren Account 
                    
                    <a href='https://132.231.36.206/confirm.php?email=$email&token=$token'> hier</a>.

                    <br><br>
					Anbei ist auch Ihre vom System vergebene ID:		$lehrstuhlID
					<br>
					und ihr vorl&auml;ufiges Passwort: 				$passwort
					<br><br>
					Bitte beachten Sie, dass Sie ihr Passwort nach der ersten Anmeldung unter dem Reiter Profil sofort &auml;ndern!";

                //Absender
                $mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
               
                //Empfaenger
                $mail->addAddress($email, $lehrstuhlBezeichnung);

                if ($mail->send()){
						$msg = "Der Lehrstuhl ist nun registiert. Bitte erinnern Sie den Dekan daran, dass sein Account vor der Nutzung aktiviert werden muss!";
						$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
										Der Lehrstuhl ist nun registriert. Eine E-Mail zur Aktivierung des Accounts wurde versendet.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
				}else{
						$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
						$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
										Etwas ist schief gelaufen! Verifizierungsemail an Lehrstuhl nicht gesendet! Bitte probieren Sie es nochmal.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									 </div>';
						$error = true;
				}

				//Beginne Transaktion, um Tabellen zu füllen.
				$pdo->beginTransaction();
				try{
					$stmt = $pdo->prepare("INSERT INTO User (Email, Passwort, Rolle, Aktiv, Token)
											VALUES (:email, :cryptPasswort, :rolle, :aktiv, :token)");
					$res  = $stmt->execute(array(':email' => $email, ':cryptPasswort' => $cryptPasswort, ':rolle' => $rolle, ':aktiv'=> $aktiv, ':token' => $token));
				
					$stmt2 = $pdo->prepare("INSERT INTO Lehrstuhl (Lehrstuhl_ID, Bezeichnung, Email)
											VALUES (:lehrstuhlID, :lehrstuhlBezeichnung, :email)");
					$res2  = $stmt2->execute(array(':lehrstuhlID' => $lehrstuhlID, ':lehrstuhlBezeichnung' => $lehrstuhlBezeichnung, ':email' => $email));

					$stmt3 = $pdo->prepare("INSERT INTO User_Historie (Email, Letzter_Login, Login_Erfolgreich, Passwortaenderung, Passwortaenderung_Erfolgreich, Falsches_Passwort)	  
											VALUES (:email, NOW(), :loginErfolgreich, NOW(), :passwortaenderungErfolgreich, :falschesPasswort)");
					$res3  = $stmt3->execute(array(':email' => $email, ':loginErfolgreich' => $loginErfolgreich, ':passwortaenderungErfolgreich' => $passwortaenderungErfolgreich, ':falschesPasswort' => $falschesPasswort));
    
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50 role="alert">
									Anlegen des Lehrstuhls '.$lehrstuhlBezeichnung.' mit der ID '.$lehrstuhlID.' und der E-Mail '.$email.' erfolgreich abgeschlossen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								   </div>';
					$_SESSION['Error']  = $successMsg;
					if(!empty($successMsg2)){
						$_SESSION['Error2'] = $successMsg2;
					}else{
						$_SESSION['Error2'] = $errorMsg2;
					}
					header('Location:lehrstuhlAnlegen.php');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Anlegen des Lehrstuhls fehlgeschlagen. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
					</div>';
					$error = true;
				}
			}
			
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				$_SESSION['Error2'] = $errorMsg2;
				header("Location:lehrstuhlAnlegen.php");
				exit;
			}
		}
		
		//Bewerbungszeitraum Festlegen
		if (isset ($_POST['bewerbungszeitraumFestlegen'])){
			$zeitraumName            = htmlentities($_POST['zeitraumName']           ,ENT_QUOTES);
			$anmeldeAnfang           = htmlentities($_POST['anmeldeAnfang']          ,ENT_QUOTES);
			$anmeldeEnde             = htmlentities($_POST['anmeldeEnde']            ,ENT_QUOTES);
			$zuteilungDeadline       = htmlentities($_POST['zuteilungDeadline']      ,ENT_QUOTES);
			$ablehnungDeadline       = htmlentities($_POST['ablehnungDeadline']      ,ENT_QUOTES);
			$zweiteZuteilungDeadline = htmlentities($_POST['zweiteZuteilungDeadline'],ENT_QUOTES);
			$themenZuteilungDeadline = htmlentities($_POST['themenZuteilungDeadline'],ENT_QUOTES);
			
			$error = false;

			if (empty ($zeitraumName)      || empty ($anmeldeAnfang) ||
				empty ($anmeldeEnde)       || empty ($ablehnungDeadline) ||
				empty ($zuteilungDeadline) || empty ($zweiteZuteilungDeadline) || empty($themenZuteilungDeadline)){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Alle Daten müssen angegeben werden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($anmeldeAnfang >= $anmeldeEnde || $anmeldeAnfang >= $zuteilungDeadline || $anmeldeAnfang >= $ablehnungDeadline || 
			   $anmeldeAnfang >= $zweiteZuteilungDeadline || $anmeldeAnfang >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Der Anmelde Anfang darf nicht später oder gleich der anderen Fristen sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($anmeldeEnde >= $zuteilungDeadline || $anmeldeEnde >= $ablehnungDeadline || 
			   $anmeldeEnde >= $zweiteZuteilungDeadline || $anmeldeEnde >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Der Anmelde Ende darf nicht später oder gleich der 1.Zuteilungsfrist, Ablehnungsfrist oder der 2.Zuteilungsfrist sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($zuteilungDeadline >= $ablehnungDeadline || $zuteilungDeadline >= $zweiteZuteilungDeadline ||
			   $zuteilungDeadline >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Die 1.Zuteilung darf nicht später oder gleich der Ablehnungsfrist oder der 2.Zuteilungsfrist sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($ablehnungDeadline >= $zweiteZuteilungDeadline || $ablehnungDeadline >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Die Ablehnungsfrist darf nicht später oder gleich der 2.Zuteilungsfrist oder der Frist zur Themenverteilung sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($zweiteZuteilungDeadline >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Die 2.Zuteilungsfrist darf nicht später oder gleich der Frist zur Themenverteilung oder der Frist zur Themenverteilung sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			$stmt          = $pdo->prepare("SELECT * 
						      				FROM Bewerbungszeitraum
											WHERE Name = :zeitraumName ");
			$result        = $stmt->execute(array(':zeitraumName' => $zeitraumName));
			$bwZeitraum    = $stmt->fetch();
		
			if ($bwZeitraum != false){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Dieser Bewerbungszeitraum ist bereits vorhanden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}

			if ($error == false){
				$bewerbungszeitraumFestlegen = $pdo->prepare("INSERT INTO Bewerbungszeitraum (Name, Anmelde_Anfang, Anmelde_Ende, Zuteilung_Deadline, Ablehnung_Deadline, Zweite_Zuteilung_Deadline, Themen_Zuteilung_Deadline)
											                  VALUES (:zeitraumName, :anmeldeAnfang, :anmeldeEnde, :zuteilungDeadline, :ablehnungDeadline, :zweiteZuteilungDeadline, :themenZuteilungDeadline)");
				$zeitraum  = $bewerbungszeitraumFestlegen->execute(array(':zeitraumName' => $zeitraumName, ':anmeldeAnfang' => $anmeldeAnfang, ':anmeldeEnde' => $anmeldeEnde, ':zuteilungDeadline' => $zuteilungDeadline,
																		 ':ablehnungDeadline' => $ablehnungDeadline, ':zweiteZuteilungDeadline' => $zweiteZuteilungDeadline, ':themenZuteilungDeadline' => $themenZuteilungDeadline));
			
				if($zeitraum){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Bewerbungszeitraum wurde erfolgreich festgelegt.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:bewerbungszeitraeume.php');
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Festlegen des Bewerbungszeitraums fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header("Location:bewerbungszeitraumFestlegen.php");
				exit;
			}
		}
		
		//Bewerbungszeitraum Bearbeiten
		if (isset ($_POST['bewerbungszeitraumBearbeiten'])){
			$bewerbungszeitraumID    = htmlentities($_POST['bewerbungszeitraumID'],ENT_QUOTES);
			$zeitraumName            = htmlentities($_POST['zeitraumName']           ,ENT_QUOTES);
			$anmeldeAnfang           = htmlentities($_POST['anmeldeAnfang']          ,ENT_QUOTES);
			$anmeldeEnde             = htmlentities($_POST['anmeldeEnde']            ,ENT_QUOTES);
			$zuteilungDeadline       = htmlentities($_POST['zuteilungDeadline']      ,ENT_QUOTES);
			$ablehnungDeadline       = htmlentities($_POST['ablehnungDeadline']      ,ENT_QUOTES);
			$zweiteZuteilungDeadline = htmlentities($_POST['zweiteZuteilungDeadline'],ENT_QUOTES);
			$themenZuteilungDeadline = htmlentities($_POST['themenZuteilungDeadline'],ENT_QUOTES);
			
			$error         = false;
			
			if (empty ($zeitraumName)      || empty ($anmeldeAnfang) ||
				empty ($anmeldeEnde)       || empty ($ablehnungDeadline) ||
				empty ($zuteilungDeadline) || empty ($zweiteZuteilungDeadline) || empty($themenZuteilungDeadline)){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Alle Daten müssen angegeben werden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($anmeldeAnfang >= $anmeldeEnde || $anmeldeAnfang >= $zuteilungDeadline || $anmeldeAnfang >= $ablehnungDeadline || 
			   $anmeldeAnfang >= $zweiteZuteilungDeadline || $anmeldeAnfang >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Der Anmelde Anfang darf nicht später oder gleich der anderen Fristen sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($anmeldeEnde >= $zuteilungDeadline || $anmeldeEnde >= $ablehnungDeadline || 
			   $anmeldeEnde >= $zweiteZuteilungDeadline || $anmeldeEnde >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Der Anmelde Ende darf nicht später oder gleich der 1.Zuteilungsfrist, Ablehnungsfrist oder der 2.Zuteilungsfrist sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($zuteilungDeadline >= $ablehnungDeadline || $zuteilungDeadline >= $zweiteZuteilungDeadline ||
			   $zuteilungDeadline >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Die 1.Zuteilung darf nicht später oder gleich der Ablehnungsfrist oder der 2.Zuteilungsfrist sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($ablehnungDeadline >= $zweiteZuteilungDeadline || $ablehnungDeadline >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Die Ablehnungsfrist darf nicht später oder gleich der 2.Zuteilungsfrist oder der Frist zur Themenverteilung sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if($zweiteZuteilungDeadline >= $themenZuteilungDeadline){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Die 2.Zuteilungsfrist darf nicht später oder gleich der Frist zur Themenverteilung oder der Frist zur Themenverteilung sein.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
	
			if ($error == false){
				$bewerbungszeitraumBearbeiten = $pdo->prepare("UPDATE Bewerbungszeitraum 
																SET Name = :zeitraumName, Anmelde_Anfang = :anmeldeAnfang, Anmelde_Ende = :anmeldeEnde, Zuteilung_Deadline = :zuteilungDeadline, 
																	Ablehnung_Deadline = :ablehnungDeadline, Zweite_Zuteilung_Deadline = :zweiteZuteilungDeadline, Themen_Zuteilung_Deadline = :themenZuteilungDeadline
																WHERE Bewerbungszeitraum_ID = :bewerbungszeitraumID
																LIMIT 1");
				$zeitraum  = $bewerbungszeitraumBearbeiten->execute(array(':zeitraumName' => $zeitraumName, ':anmeldeAnfang' => $anmeldeAnfang, ':anmeldeEnde' => $anmeldeEnde, ':zuteilungDeadline' => $zuteilungDeadline,
																		 ':ablehnungDeadline' => $ablehnungDeadline, ':zweiteZuteilungDeadline' => $zweiteZuteilungDeadline, ':themenZuteilungDeadline' => $themenZuteilungDeadline,
																		 ':bewerbungszeitraumID' => $bewerbungszeitraumID));
			
				if($zeitraum){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Bewerbungszeitraum wurde erfolgreich bearbeitet.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header("Location:bewerbungszeitraum.php?Bewerbungszeitraum_ID=".$bewerbungszeitraumID."&Name=".$zeitraumName." ");
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Bearbeitung des Bewerbungszeitraums fehlgeschlagen. Bitte erneut versuchen
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header("Location:bewerbungszeitraumBearbeiten.php?Bewerbungszeitraum_ID=".$bewerbungszeitraumID." ");
				exit;
			}
		}
		
		//Seminar Anlegen
		if (isset ($_POST['seminarAnlegen'])){
			$seminarID               = htmlentities($_POST['seminarID']       ,ENT_QUOTES);
			$semester                = htmlentities($_POST['semester']        ,ENT_QUOTES); // Seminar.Semester entspricht Bewerbungszeitraum.Name, da auf dasselbe Semester bezogen.
			$titel                   = htmlentities($_POST['titel']           ,ENT_QUOTES);
			$teilnehmeranzahl        = htmlentities($_POST['teilnehmeranzahl'],ENT_QUOTES);
			$abschluss               = htmlentities($_POST['abschluss']       ,ENT_QUOTES);
			$beschreibung  	         = htmlentities($_POST['beschreibung']    ,ENT_QUOTES);
			$bewerbungszeitraumID    = htmlentities($_POST['bewerbungszeitraumID'],ENT_QUOTES);
			$lehrstuhlID             = htmlentities($_POST['lehrstuhlID']     ,ENT_QUOTES);
			
			$error         = false;
			
			$stmt          = $pdo->prepare("SELECT * 
						      				FROM Seminar
						     				WHERE Seminar_ID = :seminarID
												AND Semester = :semester");
			$result        = $stmt->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
			$seminar       = $stmt->fetch();
		
			if ($seminar != false){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Das Seminar '.$titel.' mit der Prüfungsnummer '.$seminarID.' im '.$semester.' ist bereits vorhanden.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			if (empty ($seminarID) || empty ($titel) || empty($teilnehmeranzahl) || empty($abschluss) || empty($beschreibung)){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Bitte alle Felder ausfüllen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
			
			if ($error == false){
				
				//Beginne Transaktion, um Tabelle zu füllen.
				$pdo->beginTransaction();
				try{
					$seminarAnlegen = $pdo->prepare("INSERT INTO Seminar (Seminar_ID, Semester, Titel, Beschreibung, Teilnehmeranzahl, Abschluss, Bewerbungszeitraum_ID, Lehrstuhl_ID)
													VALUES (:seminarID, :semester, :titel, :beschreibung, :teilnehmeranzahl, :abschluss, :bewerbungszeitraumID, :lehrstuhlID)");
					$seminar  = $seminarAnlegen->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':titel' => $titel, ':beschreibung' => $beschreibung, ':teilnehmeranzahl' => $teilnehmeranzahl,
															   ':abschluss' => $abschluss, ':bewerbungszeitraumID' => $bewerbungszeitraumID, ':lehrstuhlID' => $lehrstuhlID));
			
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Seminar wurde erfolgreich angelegt.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:seminar.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Anlegen des Seminars fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header("Location:seminarAnlegen.php");
				exit;
			}
		}
		
		//Seminar Bearbeiten
		if (isset ($_POST['seminarBearbeiten'])){
			$semester             = htmlentities($_POST['semester']            ,ENT_QUOTES);
			$seminarID            = htmlentities($_POST['seminarID']           ,ENT_QUOTES);
			$titel                = htmlentities($_POST['titel']               ,ENT_QUOTES);
			$beschreibung  	      = htmlentities($_POST['beschreibung']        ,ENT_QUOTES);
			$teilnehmeranzahl     = htmlentities($_POST['teilnehmeranzahl']    ,ENT_QUOTES);
			$abschluss            = htmlentities($_POST['abschluss']           ,ENT_QUOTES);
			
			$error         = false;
		
			if (empty ($seminarID) || empty ($titel) || empty($teilnehmeranzahl) || empty($abschluss) || empty($beschreibung)){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Bitte alle Felder ausfüllen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>';
				$error = true;
			}
	
			if ($error == false){
				//Beginne Transaktion, um Tabelle upzudaten.
				$pdo->beginTransaction();
				try{
					$seminarBearbeiten = $pdo->prepare("UPDATE Seminar 
														SET Seminar_ID = :seminarID, Titel = :titel, Beschreibung = :beschreibung, Teilnehmeranzahl = :teilnehmeranzahl, Abschluss = :abschluss
														WHERE Seminar_ID = :seminarID
															AND Semester = :semester
														LIMIT 1");
					$seminar  = $seminarBearbeiten->execute(array(':seminarID' => $seminarID, ':titel' => $titel, ':beschreibung' => $beschreibung, ':teilnehmeranzahl' => $teilnehmeranzahl,
																  ':abschluss' => $abschluss, ':semester' => $semester));
			
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Seminar wurde erfolgreich bearbeitet.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:seminar.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 Bearbeitung des Seminars fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header("Location:seminarBearbeiten.php?Seminar_ID=".$seminarID."&Semester=".$semester." ");
				exit;
			}
		}
		
		//Seminarplatz Erweitern
		if (isset ($_POST['anzahlErweitern'])){
			$semester             = htmlentities($_POST['semester']            ,ENT_QUOTES);
			$seminarID            = htmlentities($_POST['seminarID']           ,ENT_QUOTES);
			$teilnehmeranzahl     = htmlentities($_POST['teilnehmeranzahl']    ,ENT_QUOTES);
			
			$error         = false;
		
			if (empty ($seminarID) || empty($teilnehmeranzahl) || empty($semester)){
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
						 	Daten wurden nicht richtig übermittelt. Erweiterung des Seminarplatzes fehlgeschlagen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			}
			if ($error == false){
				//Beginne Transaktion, um Tabelle upzudaten.
				$pdo->beginTransaction();
				try{
					$anzahlErweitern = $pdo->prepare("UPDATE Seminar 
														SET  Teilnehmeranzahl = :teilnehmeranzahl
														WHERE Seminar_ID = :seminarID
															AND Semester = :semester
														LIMIT 1");
					$seminar  = $anzahlErweitern->execute(array(':teilnehmeranzahl' => $teilnehmeranzahl,':seminarID' => $seminarID,':semester' => $semester));
			
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Teilnehmeranzahl wurde erfolgreich geändert.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:seminar.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Änderung der Seminarteilnehmeranzahl fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}
			if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header('Location:anzahlErweitern.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
				exit;
			}
		}
		
		//Seminar Löschen
		if (isset ($_POST['seminarLoeschen'])){
			$seminarID    = htmlentities($_POST['seminarID'],ENT_QUOTES);
			$semester     = htmlentities($_POST['semester'] ,ENT_QUOTES);
			
				$seminarLoeschen = $pdo->prepare("DELETE FROM Seminar 
												  WHERE Seminar_ID = :seminarID
													AND Semester = :semester ");
				$loeschen        = $seminarLoeschen->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
			
				if($loeschen){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Löschen des Seminars erfolgreich.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								  </div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:alleSeminare.php');
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								 Löschen des Seminars fehlgeschlagen. Bitte erneut versuchen.
								 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$_SESSION['Error']  = $errorMsg;
					header('Location:alleSeminare.php');
					exit;
				}
		}
		
		//Alle Seminare, die älter als 5 Jahre sind löschen
		if (isset ($_POST['alleSeminareLoeschen'])){
			$zeitraumID    = htmlentities($_POST['zeitraumID'],ENT_QUOTES);
			$altZeitraumName;
			$pdo->beginTransaction();
			try{
				$altBewerbungszeitraum   = $pdo->prepare("SELECT *
														  FROM Bewerbungszeitraum
														  JOIN Seminar ON Seminar.Bewerbungszeitraum_ID = Bewerbungszeitraum.Bewerbungszeitraum_ID
														  WHERE Zweite_Zuteilung_Deadline < CURRENT_DATE() - INTERVAL 5 YEAR
															AND Bewerbungszeitraum.Bewerbungszeitraum_ID = :zeitraumID");
				$result                  = $altBewerbungszeitraum->execute(array(':zeitraumID' => $zeitraumID));
				$altesBewerbungszeitraum = $altBewerbungszeitraum->fetch();
				
				$altZeitraumName = $altesBewerbungszeitraum['Name'];
				
				$altBewerbungszeitraum   = $pdo->prepare("SELECT *
														  FROM Bewerbungszeitraum
														  JOIN Seminar ON Seminar.Bewerbungszeitraum_ID = Bewerbungszeitraum.Bewerbungszeitraum_ID
														  WHERE Zweite_Zuteilung_Deadline < CURRENT_DATE() - INTERVAL 5 YEAR
															AND Bewerbungszeitraum.Bewerbungszeitraum_ID = :zeitraumID");
				$result                  = $altBewerbungszeitraum->execute(array(':zeitraumID' => $zeitraumID));
				
				foreach($altBewerbungszeitraum as $row){
					$altSeminarID = $row['Seminar_ID'];
					$altSemester  = $row['Semester'];
				
					$bewerbungszuteilungLoeschen = $pdo->prepare("DELETE FROM Bewerbungszuteilung 
																  WHERE Seminar_ID = :altSeminarID
																	AND Semester = :altSemester");
					$bewerbungLoeschen      = $bewerbungszuteilungLoeschen->execute(array(':altSeminarID' => $altSeminarID, ':altSemester' => $altSemester));
					
					$seminarthemaLoeschen = $pdo->prepare("DELETE FROM Seminarthema 
														   WHERE Seminar_ID = :altSeminarID
																AND Semester = :altSemester");
					$themaLoeschen        = $seminarthemaLoeschen->execute(array(':altSeminarID' => $altSeminarID, ':altSemester' => $altSemester));
				
					$seminarLoeschen = $pdo->prepare("DELETE FROM Seminar 
													  WHERE Seminar_ID = :altSeminarID
														AND Semester = :altSemester");
					$seminarLoeschen = $seminarLoeschen->execute(array(':altSeminarID' => $altSeminarID, ':altSemester' => $altSemester));
				}
				
				$bewerbungszeitraumLoeschen = $pdo->prepare("DELETE FROM Bewerbungszeitraum 
															 WHERE Bewerbungszeitraum_ID = :zeitraumID ");
				$zeitraumLoeschen    	    = $bewerbungszeitraumLoeschen->execute(array(':zeitraumID' => $zeitraumID));
					
				
				
				
				$pdo->commit();
				$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
								Löschen des Bewerbungszeitraums '.$altZeitraumName.' mit den dazugehörigen Seminaren und Bewerbungen erfolgreich.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							  </div>';
				$_SESSION['Error']  = $successMsg;
				header('Location:alleSeminare.php');
				exit;
					
			}catch(Exception $e){
				echo $e->getMessage();
				$pdo->rollBack();
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
							Löschen des Bewerbungszeitraums '.$altZeitraumName.' mit den dazugehörigen Seminaren und Bewerbungen fehlgeschlagen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$_SESSION['Error']  = $errorMsg;
				header("Location:alleSeminare.php");
				exit;
			}
		}
		
		//Bewerbung zum Seminar
		if (isset ($_POST['bewerben'])){
			$seminarID    = htmlentities($_POST['seminarID'],ENT_QUOTES);
			$semester     = htmlentities($_POST['semester'] ,ENT_QUOTES);
			$studentID    = htmlentities($_POST['studentID'],ENT_QUOTES);
			$prioritaet   = 0;
			
				$bewerbungszuteilung = $pdo->prepare("INSERT INTO Bewerbungszuteilung (Seminar_ID, Semester, Student_ID, Prioritaet, Bewerbung_Datum)
														VALUES (:seminarID, :semester, :studentID, :prioritaet, NOW())");
				$bewerbung           = $bewerbungszuteilung->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID, ':prioritaet' => $prioritaet));
			
				if($bewerbung){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Bewerbung zum Seminar erfolgreich abgesendet.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:bewerbungen.php');
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Absenden der Bewerbung fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$_SESSION['Error']  = $errorMsg;
					header('Location:seminar.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
				}
		}
		
		//Bewerbung Zurücknehmen zum Seminar
		if (isset ($_POST['bewerbungZurueck'])){
			$seminarID    = htmlentities($_POST['seminarID'],ENT_QUOTES);
			$semester     = htmlentities($_POST['semester'] ,ENT_QUOTES);
			$studentID    = htmlentities($_POST['studentID'],ENT_QUOTES);
			
				$bewerbungZ = $pdo->prepare("DELETE FROM Bewerbungszuteilung 
											 WHERE Seminar_ID = :seminarID
												AND Semester = :semester
												AND Student_ID = :studentID");
				$bewerbung  = $bewerbungZ->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
			
				if($bewerbung){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Widerrufen der Bewerbung zum Seminar erfolgreich.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error']  = $successMsg;
					header('Location:seminar.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Widerrufen der Bewerbung fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$_SESSION['Error']  = $errorMsg;
					header('Location:bewerbungen.php');
					exit;
				}
		}
		
		//Prioritäten Zuteilen
		if (isset ($_POST['prioritaetBearbeiten'])){
			$seminareID   = $_POST['seminarID'];
			$semester     = htmlentities($_POST['semester'] ,ENT_QUOTES);
			$studentID    = htmlentities($_POST['studentID'],ENT_QUOTES);
			$prios        = $_POST['prio'];
			
			$error = false;
			
		  if(!empty ($seminareID) && !empty($prios)){
			 $N = count($seminareID);
			 $p = 0;          //Zählt die Prioritäten, die zwischen 1 und 5 liegen.
			 $break = 0;      //Sobald eine Priorität unter 0 oder über 5 oder doppelt vergeben wird, wird diese Zahl erhöht und keine Priorität kann vergeben werden.
			 $a = array();    //Überprüft, ob eine Priorität doppelt vergeben wurde.
			 for($j=0; $j<$N; $j++){
				 if($prios[$j] < "0" && $prios[$j] > "5"){
					 $break++;
				 }
				 if($prios[$j] <= "5" && $prios[$j] >= "1"){
					 $p++;
					 $a[$j] = $prios[$j];
					 
					 for($k=0; $k < $j; $k++){    //Kontrolle, ob eine Priorität nur einmal vergeben wurde.
						if($a[$k] == $prios[$j]){
							$break++;
						}
					}
				 }else{
					 $a[$j] = $prios[$j];
				 }
			 }
		 
			if($break == 0){
				if($p < 6){
					
					$pdo->beginTransaction();
					try{
						for($i=0; $i < $N; $i++){
							$seminarID  = $seminareID[$i];
							$prioritaet = $prios[$i];
					
								$prioZuteilen = $pdo->prepare("UPDATE Bewerbungszuteilung
																SET Prioritaet = :prioritaet
																WHERE Seminar_ID = :seminarID
																	AND Semester = :semester
																	AND Student_ID = :studentID ");
								$prioZut         = $prioZuteilen->execute(array(':prioritaet' => $prioritaet, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
						}
						
						$pdo->commit();
						$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
										Prioritäten vergeben erfolgreich.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										</div>';
						$_SESSION['Error']  = $successMsg;
						header('Location:bewerbungen.php');
						exit;
					
					}catch(Exception $e){
						echo $e->getMessage();
						$pdo->rollBack();
						$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
									Prioritäten vergeben fehlgeschlagen. Bitte erneut versuchen.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
						$error = true;
					}
				}else{
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Sie dürfen nur 5 Prioritäten vergeben. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}else{
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
						 	Bitte geben Sie jede Priorität nur einmal an, wobei diese zwischen 0 und 5 sein sollen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
					</div>';
				$error = true;
			}
		  }else{
			 $errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
						  Keine Prioritäten vergeben. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						  </div>';
			$error = true;
		  }
		  if ($error == true){
				$_SESSION['Error']  = $errorMsg;
				header('Location:prioritaetBearbeiten.php');
				exit;
		  }
		}
		
		//Zuteilen von Bewerbern zum Seminar
		if (isset ($_POST['zuteilen'])){
			$seminarID         = $_GET['Seminar_ID'];
			$semester          = $_GET['Semester'];
			$student    	   = $_POST['student'];
			$anzahl    		   = $_POST['anzahl'];
			$teilnehmer 	   = $_POST['zugeteilteTeilnehmer'];
			$seminarleistungen = $_POST['seminarleistungen'];
			$studentNamen	   = $_POST['studentName'];
			$studentVornamen   = $_POST['studentVorname'];
			$emails			   = $_POST['email'];
			$titel             = $_POST['titel'];
			
			$error = false;
			
			if(!empty ($student)){
				$N = count($student);
				if($anzahl >= $N){
					$restZahl = $anzahl - $teilnehmer;
					
					if ($restZahl > 0 && $N <= $restZahl){
						$pdo->beginTransaction();
						try{
							//Der ganze Zeitraum nach Ablauf der Ablehnungsfrist bis ein neuer Bewerbungszeitraum angelegt wird.
							$ZnachAblehnungDeadl   = $pdo->prepare("SELECT *
																	FROM Bewerbungszeitraum
																	WHERE (NOW() > Ablehnung_Deadline)
																	ORDER BY Bewerbungszeitraum_ID DESC 
																	LIMIT 1");
							$result  		 	     = $ZnachAblehnungDeadl->execute(array());
							$ZnachAblehnungDeadline  = $ZnachAblehnungDeadl->fetch();
							
								for($i=0; $i < $N; $i++){
									$studentID 		= $student[$i];
									$studentName 	= $studentNamen[$i];
									$studentVorname = $studentVornamen[$i];
									$email			= $emails[$i];
									
									$zuteilung = 1;
									$zuteilen = $pdo->prepare("UPDATE Bewerbungszuteilung
																SET Zuteilung = :zuteilung, Zuteilung_Datum = NOW()
																WHERE Seminar_ID = :seminarID
																	AND Semester = :semester
																	AND Student_ID = :studentID
																LIMIT 1");
									$zut      = $zuteilen->execute(array(':zuteilung' => $zuteilung, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
								
									if(!empty($ZnachAblehnungDeadline)){
										$ablehnung = 0;
										$ablehn    = $pdo->prepare("UPDATE Bewerbungszuteilung
																	SET Ablehnung = :ablehnung, Ablehnung_Datum = NOW()
																	WHERE Seminar_ID = :seminarID
																		AND Semester = :semester
																		AND Student_ID = :studentID
																	LIMIT 1");
										$ablehnung = $ablehn->execute(array(':ablehnung' => $ablehnung, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));

										$seminarleistung = $seminarleistungen[$i] + 1;
										$seminarLeist    = $pdo->prepare("UPDATE Student
																			SET Seminarleistungen = :seminarleistung
																			WHERE Student_ID = :studentID
																			LIMIT 1");
										$leistung = $seminarLeist->execute(array(':seminarleistung' => $seminarleistung, ':studentID' => $studentID));
										
									}
								
									//Versand Mail ueber Zusage Seminar	
									include_once "phpmailer/PHPMailerAutoload.php";
				
									//Instance für PHPMailer
									$mail = new PHPMailer();
                
									//Host fuer PHPMailer
									$mail->Host = "smtp.gmail.com";
					
									//Aktiviert SMTP
									$mail->isSMTP();
                
									//Authentifizierung
									$mail->SMTPAuth = true;
                
									//Login Details fuer GMAIL
									$mail->Username = 'seminararbeitsvergabe.gruppe5@gmail.com';
									$mail->Password = 'z1cfc4f5';

									$mail->SMTPSecure = "tls";

									//Port
									$mail->Port = 587;

									//Betreff
									$mail->Subject = "Zusage zum Seminar $titel";
		
									$mail->isHTML(true);

									//Inhalt
									$mail->Body = "
									Sehr gehrte/r $studentVorname $studentName,<br><br>
									Sie wurden zum Seminar $titel zugelassen. Bitte &uuml;berpr&uuml;fen Sie den Status ihrer Bewerbungen. <br>
                    
									<a href='https://132.231.36.206/index.php'> Weiter zur Webseite</a>";

									//Absender
									$mail->setFrom('seminarvergabesystem.gruppe5@gmail.com');
               
									//Empfaenger
									$mail->addAddress($email);

									if ($mail->send()){
										$msg = "Sie haben den Seminarteilnemher erfolgreich hinzugefügt!";
										$successMsg2 = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
														Zusage dem Seminarteilnemher per Mail erfolgreich gesendet!
														<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
														</div>';
									}else{
										$msg = "Etwas ist schief gelaufen! Bitte probieren Sie es nochmal.";
										$errorMsg2 = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
														Etwas ist schief gelaufen! Zusage nicht per Mail an den Studenten gesendet! Bitte probieren Sie es nochmal.
														<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>';
										$error = true;
									}

								}
								$pdo->commit();
								$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
												Hinzufügen der Seminarteilnehmer erfolgreich.
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												</div>';
								$_SESSION['Error']  = $successMsg;
								if(!empty($successMsg2)){
									$_SESSION['Error2'] = $successMsg2;
								}else{
									$_SESSION['Error2'] = $errorMsg2;
								}
								header('Location:bewerberliste.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
								exit;
					
							}catch(Exception $e){
								echo $e->getMessage();
								$pdo->rollBack();
								$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
											Hinzufügen der Seminarteilnehmer fehlgeschlagen. Bitte erneut versuchen.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											</div>';
								$error = true;
							}
					}else{
						$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
									Zu viele Seminarteilnehmer ausgewählt. Bitte die maximale Teilnehmeranzahl beachten.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
						$error = true;
					}
				}else{
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Zu viele Seminarteilnehmer ausgewählt. Bitte die maximale Teilnehmeranzahl beachten.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}else{
				$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
						 	Keine Seminarteilnehmer zum Hinzufügen zum Seminar ausgewählt. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			}
			if ($error == true){
				$_SESSION['Error']   = $errorMsg;
				$_SESSION['Error2']  = $errorMsg2;
				header('Location:bewerberliste.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
				exit;
			}
		}
		
		//Zuteilen von Seminarthemen zu Studenten
		if (isset ($_POST['themaZuteilen'])){
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];
			$student       = $_POST['student'];
			$themas        = $_POST['thema'];
			
			$error = false;
			
			if(!empty ($student) && !empty($themas)){
				$N = count($student);
		 
				$pdo->beginTransaction();
				try{
					for($i=0; $i < $N; $i++){
						$studentID = $student[$i];
						$thema     = $themas[$i];
					
						$stmt          = $pdo->prepare("SELECT * 
														FROM Seminarthema
														WHERE Seminar_ID = :seminarID
															AND Semester = :semester
															AND Student_ID = :studentID ");
						$result        = $stmt->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
						$studentThema  = $stmt->fetch();
		
						if ($studentThema != false){
							$themaZuteilen = $pdo->prepare("UPDATE Seminarthema
															SET Thema = :thema
															WHERE Seminar_ID = :seminarID
																AND Semester = :semester
																AND Student_ID = :studentID
															LIMIT 1");
							$zuteilen      = $themaZuteilen->execute(array(':thema' => $thema, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
					
						}else{
							$themaZuteilung = $pdo->prepare("INSERT INTO Seminarthema(Thema, Seminar_ID, Semester, Student_ID)
															VALUES (:thema, :seminarID, :semester, :studentID)");
							$zuteilung      = $themaZuteilung->execute(array(':thema' => $thema, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
						}
					}
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Vergeben der Seminarthemen erfolgreich.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$_SESSION['Error'] = $successMsg;
					header('Location:seminarThemen.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
		
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Vergeben der Seminarthemen fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}else{
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Keine Seminarthemen vergeben. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			 }
			 
			 if($error == true){
				$_SESSION['Error']   = $errorMsg;
				header('Location:themenZuteilen.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
				exit;
			 }
		}
		
		//Seminarplatz zusagen
		if (isset ($_POST['zusagen'])){
			$seminarID       = htmlentities($_POST['seminarID'],ENT_QUOTES);
			$semester        = htmlentities($_POST['semester'] ,ENT_QUOTES);
			$studentID       = htmlentities($_POST['studentID'],ENT_QUOTES);
			$seminarleistung = htmlentities($_POST['seminarleistung'],ENT_QUOTES);
			$ablehnung = 0;
			$seminarleistung++;	
				
				$pdo->beginTransaction();
				try{
					$zusagen    = $pdo->prepare("UPDATE Bewerbungszuteilung
												SET Ablehnung = :ablehnung, Ablehnung_Datum = NOW()
												WHERE Seminar_ID = :seminarID
													AND Semester = :semester
													AND Student_ID = :studentID
												LIMIT 1");
					$zusage      = $zusagen->execute(array(':ablehnung' => $ablehnung, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
			
					$seminarLeist    = $pdo->prepare("UPDATE Student
													SET Seminarleistungen = :seminarleistung
													WHERE Student_ID = :studentID
													LIMIT 1");
					$leistung = $seminarLeist->execute(array(':seminarleistung' => $seminarleistung, ':studentID' => $studentID));
										
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Zusage zum Seminarplatz erfolgreich.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error'] = $successMsg;
					header('Location:seminarZuteilung.php');
					exit;
					
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Zusage zum Seminarplatz fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$_SESSION['Error'] = $errorMsg;
					header('Location:seminarZuteilung.php');
					exit;
				}
		}
		
		//Seminarplatz ablehnen
		if (isset ($_POST['absagen'])){
			$seminarID    = htmlentities($_POST['seminarID'],ENT_QUOTES);
			$semester     = htmlentities($_POST['semester'] ,ENT_QUOTES);
			$studentID    = htmlentities($_POST['studentID'],ENT_QUOTES);
			$ablehnung = 1;
				
				$absagen    = $pdo->prepare("UPDATE Bewerbungszuteilung
											SET Ablehnung = :ablehnung, Ablehnung_Datum = NOW()
											WHERE Seminar_ID = :seminarID
												AND Semester = :semester
												AND Student_ID = :studentID
											LIMIT 1");
				$absage      = $absagen->execute(array(':ablehnung' => $ablehnung, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
			
				if($absage){
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Ablehnen des Seminarplatzes erfolgreich.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</div>';
					$_SESSION['Error'] = $successMsg;
					header('Location:seminarZuteilung.php');
					exit;
				}else{
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								  Ablehnen des Seminarplatzes fehlgeschlagen. Bitte erneut versuchen.
								  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								  </button>
								</div>';
					$_SESSION['Error'] = $errorMsg;
					header('Location:seminarZuteilung.php');
					exit;
				}
		}
		
		//Absolviert Eintragen (Wurde das Seminar erfolgreich absolviert?)
		if (isset ($_POST['absolviertEintragen'])){
			$seminarID       = $_GET['Seminar_ID'];
			$semester        = $_GET['Semester'];
			$student         = $_POST['student'];
			$absolviertArray = $_POST['absolviert'];
			
			$error = false;
			
			if(!empty ($student) && !empty($absolviertArray)){
				$N = count($student);
		 
				$pdo->beginTransaction();
				try{
					for($i=0; $i < $N; $i++){
						$studentID = $student[$i];
						$absolviert     = $absolviertArray[$i];
						
						$absolviertEintragen = $pdo->prepare("UPDATE Bewerbungszuteilung
															SET Absolviert = :absolviert, Absolviert_Datum = NOW()
															WHERE Seminar_ID = :seminarID
																AND Semester = :semester
																AND Student_ID = :studentID
															LIMIT 1");
						$eintragen      = $absolviertEintragen->execute(array(':absolviert' => $absolviert, ':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
					
					}
					$pdo->commit();
					$successMsg = '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
									Eintragen, ob das Seminar absolviert wurde, erfolgreich.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$_SESSION['Error'] = $successMsg;
					header('Location:seminarTeilnehmer.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
					exit;
		
				}catch(Exception $e){
					echo $e->getMessage();
					$pdo->rollBack();
					$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
								Eintragen, ob das Seminar absolviert wurde, fehlgeschlagen. Bitte erneut versuchen.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					$error = true;
				}
			}else{
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Nicht angegeben, ob das Seminar absolviert wurde. Bitte erneut versuchen.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$error = true;
			 }
			 
			 if($error == true){
				$_SESSION['Error']   = $errorMsg;
				header('Location:seminarAbsolviert.php?Seminar_ID='.$seminarID.'&Semester='.$semester.' ');
				exit;
			 }
		}
		
		//CSV Download Bewerber
		if(isset($_POST["Export_Bewerber"])){
				$seminarID     = $_POST['seminarID'];       //Variable von der versteckten Form übernehmen
	 			$semester      = $_POST['semester'];        //Variable von der versteckten Form übernehmen
		 
			    header("Content-Type: text/csv; charset=utf-8");  
			    header("Content-Disposition: attachment; filename=Bewerber-$seminarID-$semester.csv");  
			    $output = fopen("php://output", "w");  
						
				$Cstmt       = $pdo->prepare("SELECT Student.Student_ID, Student.Name, Student.Vorname, Student.Email, Student.Studiengang, Student.Fachsemester, Bewerbungszuteilung.Prioritaet, Student.Seminarleistungen, Bewerbungszuteilung.Bewerbung_Datum
											  FROM Bewerbungszuteilung
											  JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											  JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											  WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester = :semester ");
				$result      = $Cstmt->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
				
				ob_end_clean();
				fputcsv($output, array('Matrikelnummer', 'Name', 'Vorname', 'E-Mail', 'Studiengang', 'Fachsemester', 'Prioritaet', 'Seminarleistungen', 'Bewerbung am')); 

				
				while ($row=$Cstmt->fetch(PDO::FETCH_ASSOC))  
			    {  
			           fputcsv($output, $row);  
			    }  
			    fclose($output);  

			 exit();
		}  

		//CSV Download Seminarteilnehmer
		if(isset($_POST["Export_Teilnehmer"])){
	  			$seminarID     = $_POST['seminarID'];       //Variable von der versteckten Form übernehmen
	  			$semester      = $_POST['semester'];        //Variable von der versteckten Form übernehmen
		 
			    header("Content-Type: text/csv; charset=utf-8");  
			    header("Content-Disposition: attachment; filename=Teilnehmer-$seminarID-$semester.csv"); 
			    $output = fopen("php://output", "w");  
			       
				$Sstmt       = $pdo->prepare("SELECT Student.Student_ID, Student.Name, Student.Vorname, Student.Email, Student.Studiengang, Student.Fachsemester, Bewerbungszuteilung.Prioritaet, Bewerbungszuteilung.Bewerbung_Datum, Seminarthema.Thema
											  FROM Bewerbungszuteilung
											  JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											  JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											  JOIN Seminarthema ON Seminarthema.Seminar_ID = Seminar.Seminar_ID
											  WHERE Seminarthema.Semester = Seminar.Semester
												AND Seminarthema.Student_ID = Student.Student_ID
												AND Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester = :semester
												AND Bewerbungszuteilung.Zuteilung = 1 ");
				$result      = $Sstmt->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
							
				ob_clean();
				ob_end_clean();
				fputcsv($output, array('Matrikelnummer', 'Name', 'Vorname', 'Email', 'Studiengang', 'Fachsemester','Prioritaet', 'Bewerbung am', 'Seminarthema')); 

				while ($row=$Sstmt->fetch(PDO::FETCH_ASSOC))  
			    {  
					fputcsv($output, $row);					   
			     }  
			      fclose($output);  
			 exit();
		}


	//Hisqis Download
	if(isset($_GET['Email'])){
		$email    = $_GET['Email'];       
		$name 	  = $_GET['Name'];

			$stmt     = $pdo->prepare("SELECT HISQIS_Auszug_Link										 
	  								   FROM Student
									   WHERE Email = :email");
			$result   = $stmt->execute(array(':email' => $email));
			$filePath = $stmt->fetch();

			if(!empty($filePath['HISQIS_Auszug_Link'])){

				$file = $filePath['HISQIS_Auszug_Link'];

				$fileName     = substr($file, 8);	

				echo "$fileName, $file, $fileP";
				ob_clean();
			    header("Content-Type: application/pdf"); 
				header("Content-Disposition: attachment; filename='Hiqis_Auszug_$name.pdf'"); 
				

				readfile($file);
				exit;
			}else{
				$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						 	Fehler beim Download. Es wurde noch kein HISQIS-Auszug hochgeladen!
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
				$_SESSION['Error']   = $errorMsg;
				header('Location:profil.php');
				exit;
			}
	}

			//Hisqis Upload Neu Profil.php
			if(isset($_POST['Upload'])){
			 	$email     = $_POST['Email'];
				$error == false;

			 	$stmt        = $pdo->prepare("SELECT HISQIS_Auszug_Link										 
											  FROM Student
											  WHERE Email = :email");
				$result      = $stmt->execute(array(':email' => $email));
				$oldFilePath = $stmt->fetch();

				$oldFile = $oldFilePath['HISQIS_Auszug_Link'];

				if (file_exists($oldFile)) {
					unlink($oldFile);
        		}
							 
				$hisqisAuszug = $_FILES['hisqisAuszug'];
				

				$fileName    = $_FILES['hisqisAuszug']['name'];
				$fileTmpName = $_FILES['hisqisAuszug']['tmp_name'];
				$fileSize    = $_FILES['hisqisAuszug']['size'];
				$fileError   = $_FILES['hisqisAuszug']['error'];
				$fileType    = $_FILES['hisqisAuszug']['type'];

				$fileExt       = explode('.', $fileName);
				$fileActualExt = strtolower(end($fileExt));

				$allowed = array('pdf');

				//Error Alert ergänzen!!!
				if(in_array($fileActualExt, $allowed)){
					if($fileError === 0) {
						if($fileSize < 4000000){
							$fileNameNew = uniqid('', true).".".$fileActualExt;
							$fileDestination = 'uploads/'.$fileNameNew;
							move_uploaded_file($fileTmpName, $fileDestination);

							$filePath ="uploads/".$fileNameNew;

							$stmt = $pdo->prepare("UPDATE Student 
													SET HISQIS_Auszug_Link = :filePath
													WHERE Email = :email
													LIMIT 1");
							$setzeNeuenHISQISLink  = $stmt->execute(array(':filePath' => $filePath, ':email' => $email));

						}else{
							$errorMsg ='<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
										Datei ist zu groß.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										</div>';
							$error = true;
						}
					}else{
						$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
										Problem mit dem Upload.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>';
						$error = true;
					}
				}else{
					$errorMsg = '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								 	Es sind nur PDF-Dateien erlaubt.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>';
					$error = true;
				}
				
				if($error == true){
					$_SESSION['Error']   = $errorMsg;
					header('Location:profil.php');
					exit;
				}
			}
	}

	?>
	</div>
  </body>
</html>