<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Seminar Anlegen für das <?php echo $bewerbungszeitraeume['Name']; ?> </h2>
	<?php
			
			if($rolle == 2 || $rolle ==4){
				if(empty ($bewerbungszeitraeume)){
					echo '
					<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
					<h5 class="alert-heading">Info:</h5>
						Kein Bewerbungszeitraum festgelegt. Es können keine Seminare angelegt werden.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>';
				}else{
					if(empty ($ZvorAnmeldeBeginn) && $testmodus['Testmodus'] == "0"){
	?>
			<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
				  <h5 class="alert-heading">Info:</h5>
				  <p> Die Bewerbungsphase hat angefangen. Es können keine neuen Seminare mehr angelegt werden. </p>
			   	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
			</div>
	<?php
				  }else{
					if($rolle == 2){
						$lehrstuhlID = $lehrstuhl['Lehrstuhl_ID'];
					}
					if($rolle == 4){
						//Wenn der Admin ein Seminar anlegt, wird dessen ID als Lehrstuhl_ID gespeichert.
						$lehrstuhlID = $admin['Admin_ID'];
					}
	?>
		<div class="col-md-10">
			<table class="table table-sm no-border">
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="seminarAnlegen"       value="anlegen">
				<input type="hidden" name="bewerbungszeitraumID" value=<?php echo $bewerbungszeitraeume['Bewerbungszeitraum_ID']; ?> >
				<input type="hidden" name="semester"             value=<?php echo $bewerbungszeitraeume['Name'] ?> >
				<input type="hidden" name="lehrstuhlID"          value=<?php echo $lehrstuhlID ?> >
			<tr>
				<th> Prüfungsnummer: </th>      
				<td> <input type="number" min="1" name="seminarID" class="form-control" placeholder="Prüfungsnummer" required> </td>
			</tr>
			<!-- Seminar.Semester entspricht Bewerbungszeitraum.Name, da auf dasselbe Semester bezogen. -->
			<tr>
				<td> <b>Titel:</b> </br>
					 <p class="text-muted">Titel des Seminars.</p> </td>
				<td> <input type="text" name="titel" class="form-control" placeholder="Titel" required> </td>
			</tr>
			<tr>
				<td> <b>Beschreibung:</b> </br>
					 <p class="text-muted">Kurzbeschreibung des Seminars.</p> </td>
				<td> <textarea type="text" name="beschreibung" class="form-control" placeholder="Beschreibung" required></textarea> </td>
			</tr>
			<tr>
				<th> Maximale Teilnehmeranzahl:</th>
				<td> <input type="number" min="1" name="teilnehmeranzahl" class="form-control" placeholder="Teilnehmeranzahl" required> </td>
			</tr>
			<tr>
				<td> <b>Abschluss:</b> </br>
					 <p class="text-muted">Der benötigte angestrebte Abschluss zum Absolvieren dieses Seminars.</p></td>      
				<td> <select name="abschluss" class="form-control" placeholder="Abschluss" required>
						<option disabled selected hidden> Abschluss </option>
						<option> Bachelor </option>
						<option> Master </option>
					 </select>
				</td>
			</tr>
			<tr>
				<th> <button type="submit" class="btn btn-info"> Seminar Anlegen</button>
				</form>
					 <a href="seminare.php?Bewerbungszeitraum_ID=<?php echo $bewerbungszeitraeume['Bewerbungszeitraum_ID']; ?>" class="btn btn-info"> Abbrechen </a></th>
			</tr>
			</table>
			</div>
			</br>
	<?php
				  }
				}
			}else{
				include 'keineBerechtigung.php';
			}
			include 'fusszeile.php';
		}
	?>
    </div>
  </body>
</html>
