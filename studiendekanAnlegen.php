<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	
			include 'navBar.php';
			include 'meldung.php';
			
			if($rolle == 4){
	?>
		 <h2> Studiendekan Anlegen </h2>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Bitte beachten, dass es zu jeder Zeit nur einen aktiven Studiendekan geben kann. 
						Beim Erstellen eines neuen Studiendekans wird der vorherige Studiendekan automatisch aus dem System gesperrt.</p>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			</div>
		
			<table>
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="studiendekanAnlegen" value="anlegen">
			<tr>
				<th> Studiendekan ID*: </th>      
				<td> <input type="number" name="studiendekanID" min="1" max="100000" maxlength="5" class="form-control" placeholder="Studiendekan ID" required> </td>
			</tr>
			<tr>
				<th> Name*: </th>      
				<td> <input type="text" name="studiendekanName" class="form-control" placeholder="Name" required> </td>
			</tr>
			<tr>
				<th> E-Mail*: </br></th>
				<td> <input type="text" style="width:70%" maxlength="20" name="email" class="form-control" placeholder="E-Mail" required> </td>
				<td class="form-control" style="margin-left:-60px; width:150px"> @uni-passau.de </td>
			</tr>
			<tr>
				<td><p class="text-muted" style="margin-bottom: -1px"> Bitte hier nur die universitäre Email Adresse angeben. &nbsp; </p></td>
			</tr>
			<tr>
				<th> </br> Alle Felder mit * müssen ausgefüllt werden. </th>
			</tr>
			<tr>
				<th> <button type="submit" class="btn btn-info">Studiendekan Anlegen</button>&nbsp;
				</form>
					 <a href="index.php" class="btn btn-info"> Abbrechen </a> </button> 
				</th>
			</tr>
			</table>
	<?php
			}else{
				include 'keineBerechtigung.php';
			}
			include 'fusszeile.php';
		}
	?>
    </div>
  </body>
</html>
