<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
		  if(!isset($_GET['Bewerbungszeitraum_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$bewerbungszeitraumID   = $_GET['Bewerbungszeitraum_ID'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if(empty ($bewerbungszeitraumBearbeiten) && $bewerbungszeitraeume['Bewerbungszeitraum_ID'] != $bewerbungszeitraumID && $testmodus['Testmodus'] == "0"){
				include 'keineBerechtigung.php';
			}else{
				if($rolle == 3 || $rolle ==4){
	?>
		 <h2> Bewerbungszeitraum Bearbeiten: <?php echo $bewerbungszeitraum['Name'] ?> </h2>
		 <h4> Bitte alle Daten angeben. </h4>
		
			<table class="table table-sm no-border">
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="bewerbungszeitraumBearbeiten" value="bearbeiten">
				<input type="hidden" name="bewerbungszeitraumID" value=<?php echo $bewerbungszeitraumID ?> >
			<tr>
				<td> <b>Bewerbungszeitraum für die Seminare im:</b> </br>
					 <p class="text-muted" >Bitte hier z.B. WS17/18 für Wintersemester 2017/2018 und </br>
					 SoSe18 für Sommersemester 2018 eingeben. </p> </td>      
				<td> <br><input type="text" name="zeitraumName" class="form-control" style="width: 229.42px" value="<?php echo $bewerbungszeitraum['Name'] ?>" required> </td>
			</tr>
			<tr>
				<td> <b>Anmelde Anfang:</b> </br>
					 <p class="text-muted" >Beginn der Bewerbungsphase für die Seminare.</p> </td>
				<td> <label>
						<?php $date = new DateTime($bewerbungszeitraum['Anmelde_Anfang']);
							echo $date->format('d.m.Y H:i'); ?>
						<input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="anmeldeAnfang" class="form-control" value="<?php echo $bewerbungszeitraum['Anmelde_Anfang'] ?>" required > </td>
					 </label>
			</tr>
			<tr>
				<td> <b>Anmelde Ende:</b> </br>
					 <p class="text-muted">Ende der Bewerbungsphase für die Seminare.</p> </td>
				<td> <label> 
						<?php $date = new DateTime($bewerbungszeitraum['Anmelde_Ende']);
							echo $date->format('d.m.Y H:i'); ?> 
						<input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="anmeldeEnde" class="form-control" value="<?php echo $bewerbungszeitraum['Anmelde_Ende'] ?>" required > </td>
					 </label>
			</tr>
			<tr>

				<td> <b>1. Zuteilung:</b> </br>
					 <p class="text-muted">Die 1. Runde zum Zuteilen der Studierenden </br>
					 zu den Seminaren nach Ablauf der Bewerbungsphase. </p></td>
				<td> <label>
						<?php $date = new DateTime($bewerbungszeitraum['Zuteilung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?>
						<input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="zuteilungDeadline" class="form-control" value="<?php echo $bewerbungszeitraum['Zuteilung_Deadline'] ?>" required > </td>
					 </label>
			</tr>
			<tr>
				<td> <b>Ablehnungsfrist:</b> </br>
					 <p class="text-muted">Die Frist für die Studierenden zum Ablehnen </br>
					 eines zugeteilten Seminarplatzes. </p></td>
				<td> <label>
						<?php $date = new DateTime($bewerbungszeitraum['Ablehnung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?>
						<input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="ablehnungDeadline" class="form-control" value="<?php echo $bewerbungszeitraum['Ablehnung_Deadline'] ?>" required > </td>
					 </label>
			</tr>
			<tr>
				<td> <b>2. Zuteilung:</b> </br>
					 <p class="text-muted">Die 2. Runde zum Zuteilen der Studierenden zu </br>
					 den Seminaren nach Ablauf der Ablehnungsfrist. </p></td>
				<td> <label>
						<?php $date = new DateTime($bewerbungszeitraum['Zweite_Zuteilung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?>
						<input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="zweiteZuteilungDeadline" class="form-control" value="<?php echo $bewerbungszeitraum['Zweite_Zuteilung_Deadline'] ?>" required > </td>
					 </label>
			</tr>
			<tr>
				<td> <b>Frist zur Themenverteilung:</b> </br>
					 <p class="text-muted">Die Frist für Lehrstühle zum Verteilen der Seminarthemen an ihre Seminarteilnehmer. </p></td>
				<td> <label>
						<?php $date = new DateTime($bewerbungszeitraum['Themen_Zuteilung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?>
						<input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="themenZuteilungDeadline" class="form-control" value="<?php echo $bewerbungszeitraum['Zweite_Zuteilung_Deadline'] ?>" required > </td>
					 </label>
			</tr>
			
			<tr>
				<th> <button type="submit" class="btn btn-info"> Bearbeitung Abschließen </button> &nbsp;
				</form> 
					 <a href="bewerbungszeitraeume.php" class="btn btn-info"> Abbrechen </a> </button> </th>
			</tr>
			</table>
	<?php
				}else{
					include 'keineBerechtigung.php';
				}
			}
			include 'fusszeile.php';
		  }
		}
	?>
    </div>
  </body>
</html>
