CREATE DATABASE IF NOT EXISTS seminarvergabesystem_db1;

use seminarvergabesystem_db1;

CREATE TABLE IF NOT EXISTS User(
Email                 VARCHAR(255) NOT NULL UNIQUE,
Passwort              VARCHAR(255) NOT NULL,
Rolle                 TINYINT(1)   NOT NULL,
Aktiv  	  			  BOOLEAN NOT NULL DEFAULT FALSE,
Token				  VARCHAR(10) NOT NULL DEFAULT '',
Profilbild_Link		  VARCHAR(255),	
PRIMARY KEY (Email)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS User_Historie(
User_Historie_ID                  INT NOT NULL AUTO_INCREMENT,
Email                             VARCHAR(255) NOT NULL,
Letzter_Login                     TIMESTAMP NOT NULL,
Login_Erfolgreich                 BOOLEAN NOT NULL DEFAULT FALSE,
Passwortaenderung                 TIMESTAMP,
Passwortaenderung_Erfolgreich     BOOLEAN NOT NULL DEFAULT FALSE,
Falsches_Passwort                 TINYINT(2) NOT NULL,
FOREIGN KEY (Email)               REFERENCES User(Email),
PRIMARY KEY (User_Historie_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Student(
Student_ID            INT(5) NOT NULL,
Name                  VARCHAR(255) NOT NULL,
Vorname               VARCHAR(255) NOT NULL,
Fachsemester          TINYINT(2)   NOT NULL,
Studiengang           VARCHAR(255) NOT NULL,
Abschluss             VARCHAR(15)  NOT NULL,
Email                 VARCHAR(255) NOT NULL,
HISQIS_Auszug_Link    VARCHAR(255),
Seminarleistungen     TINYINT(2) NOT NULL DEFAULT '0',
FOREIGN KEY (Email)   REFERENCES User(Email),
PRIMARY KEY (Student_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Lehrstuhl(
Lehrstuhl_ID          INT(5) NOT NULL,
Bezeichnung           VARCHAR(255) NOT NULL,
Email                 VARCHAR(255) NOT NULL,
FOREIGN KEY (Email)   REFERENCES User(Email),
PRIMARY KEY (Lehrstuhl_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Studiendekan(
Studiendekan_ID       INT(5) NOT NULL,
Name                  VARCHAR(255) NOT NULL,
Email                 VARCHAR(255) NOT NULL,
ImAmt                 BOOLEAN NOT NULL,
FOREIGN KEY (Email)   REFERENCES User(Email),
PRIMARY KEY (Studiendekan_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Admin(
Admin_ID              INT(5) NOT NULL AUTO_INCREMENT,
Name                  VARCHAR(255) NOT NULL,
Email                 VARCHAR(255) NOT NULL,
Testmodus             BOOLEAN      NOT NULL DEFAULT FALSE,
FOREIGN KEY (Email)   REFERENCES User(Email),
PRIMARY KEY (Admin_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Bewerbungszeitraum(
Bewerbungszeitraum_ID       INT NOT NULL AUTO_INCREMENT,
Name                        VARCHAR(255) NOT NULL,
Anmelde_Anfang              DATETIME NOT NULL,
Anmelde_Ende                DATETIME NOT NULL,
Zuteilung_Deadline          DATETIME NOT NULL,
Ablehnung_Deadline          DATETIME NOT NULL,
Zweite_Zuteilung_Deadline   DATETIME NOT NULL,
Themen_Zuteilung_Deadline   DATETIME NOT NULL,
PRIMARY KEY (Bewerbungszeitraum_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Seminar(
Seminar_ID             INT(6) NOT NULL,
Semester               VARCHAR(255) NOT NULL,
Titel                  VARCHAR(255) NOT NULL,
Beschreibung           VARCHAR(255) NOT NULL,
Teilnehmeranzahl       TINYINT(2) NOT NULL,
Abschluss              VARCHAR(15) NOT NULL,
Bewerbungszeitraum_ID  INT NOT NULL,
Lehrstuhl_ID           INT NOT NULL,
FOREIGN KEY (Bewerbungszeitraum_ID) REFERENCES Bewerbungszeitraum(Bewerbungszeitraum_ID),
FOREIGN KEY (Lehrstuhl_ID) REFERENCES Lehrstuhl(Lehrstuhl_ID),
PRIMARY KEY (Seminar_ID)
/* PRIMARY KEY (Semester) zwei Primärschlüssel in phpMyAdmin nicht möglich*/
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Seminarthema(
Thema_ID                 INT NOT NULL AUTO_INCREMENT,
Thema                    VARCHAR(255) NOT NULL,
Seminar_ID               INT NOT NULL,
Semester                 VARCHAR(255) NOT NULL,
Student_ID               INT NOT NULL,
FOREIGN KEY (Seminar_ID) REFERENCES Seminar(Seminar_ID),
/* FOREIGN KEY (Semester)   REFERENCES Seminar(Semester), Fremdschlüssel kann durch obiges Problem nicht angelegt werden */
FOREIGN KEY (Student_ID) REFERENCES Student(Student_ID),
PRIMARY KEY (Thema_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Bewerbungszuteilung(
Bewerbungszuteilung_ID   INT NOT NULL AUTO_INCREMENT,
Seminar_ID               INT NOT NULL,
Semester                 VARCHAR(255) NOT NULL,
Student_ID               INT NOT NULL,
Prioritaet               TINYINT(1) NOT NULL,
Bewerbung_Datum          TIMESTAMP NOT NULL,
Zuteilung                BOOLEAN NOT NULL DEFAULT FALSE,
Zuteilung_Datum          TIMESTAMP,
Ablehnung                BOOLEAN NOT NULL DEFAULT TRUE,
Ablehnung_Datum          TIMESTAMP,
Absolviert               BOOLEAN NOT NULL DEFAULT FALSE,
Absolviert_Datum         TIMESTAMP,
FOREIGN KEY (Seminar_ID) REFERENCES Seminar(Seminar_ID),
FOREIGN KEY (Student_ID) REFERENCES Student(Student_ID),
/* FOREIGN KEY (Semester)   REFERENCES Seminar(Semester), Fremdschlüssel kann durch obiges Problem nicht angelegt werden */
PRIMARY KEY (Bewerbungszuteilung_ID)
)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
