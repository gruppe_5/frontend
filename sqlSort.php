<?php

if (isset ($_POST['sortieren'])){	
	//Sortiert nach Seminar_ID - Daten des Studenten, der sich zum Seminar beworben hat.
	if($sortiere == 1){
		$bewerberDatenSort = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester = :semester 
											ORDER BY Student.Student_ID ASC");
		$result  	        = $bewerberDatenSort->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
	}
	//Sortiert nach Student.Name - Daten des Studenten, der sich zum Seminar beworben hat.
	if($sortiere == 2){
	$bewerberDatenSort = $pdo->prepare("SELECT *
										FROM Bewerbungszuteilung
										JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
										JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
										WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
											AND Bewerbungszuteilung.Semester = :semester 
										ORDER BY Student.Name ");
	$result  	        = $bewerberDatenSort->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
	}
	//Sortiert nach Fachsemester - Daten des Studenten, der sich zum Seminar beworben hat.
	if($sortiere == 3){
		$bewerberDatenSort = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester = :semester 
											ORDER BY Student.Fachsemester DESC ");
		$result  	        = $bewerberDatenSort->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
	}
	//Sortiert nach Priorität - Daten des Studenten, der sich zum Seminar beworben hat.
	if($sortiere == 4){
		$bewerberDatenSort = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester = :semester 
											ORDER BY Prioritaet = '1' DESC, Prioritaet = '2' DESC,
													 Prioritaet = '3' DESC, Prioritaet = '4' DESC,
													 Prioritaet = '5' DESC, Prioritaet = '0' DESC");
		$result  	        = $bewerberDatenSort->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
	}
	//Sortiert nach Seminarleistungen - Daten des Studenten, der sich zum Seminar beworben hat.
	if($sortiere == 5){
		$bewerberDatenSort = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester = :semester 
											ORDER BY Student.Seminarleistungen ");
		$result  	        = $bewerberDatenSort->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		}
}
?>