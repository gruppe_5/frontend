<?php

//SQL-Befehle für auslastungLehrstuehle.php (Anzeigen aller Lehrstühle mit zugehörigen Seminaren und dazugehörigen Seminarteilnehmern.)

	//Alle Lehrstühle 
	$lehrst       = $pdo->prepare("SELECT *
								   FROM Lehrstuhl");
	$result       = $lehrst->execute(array());
	$lehrstuehle  = $lehrst->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Lehrstühle 
	$lehrst       = $pdo->prepare("SELECT *
								   FROM Lehrstuhl");
	$result       = $lehrst->execute(array());
	
if(isset($_GET['Bewerbungszeitraum_ID'])){
	$bewerbungszeitraumID = $_GET['Bewerbungszeitraum_ID'];
	
	//Den Namen des Bewerbungszeitraums für die beiden Sichten auslastungLehrstuehle.php und uebrigGeblieben.php abfragen.
	$bewerbungszeitraumDaten = $pdo->prepare("SELECT Name
											  FROM Bewerbungszeitraum
											  WHERE Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result                 = $bewerbungszeitraumDaten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$bewerbungszeitraum     = $bewerbungszeitraumDaten->fetch();

  if(isset($_SESSION['Lehrstuhl_ID'])){
	$lehrstuhlID = $_SESSION['Lehrstuhl_ID'];
	
	//Alle Seminare, die ein Lehrstuhl anbietet.
	$seminare       = $pdo->prepare("SELECT *
									FROM Seminar
									WHERE Lehrstuhl_ID = :lehrstuhlID
										AND Bewerbungszeitraum_ID = :bewerbungszeitraumID
									ORDER BY Abschluss");
	$result2     = $seminare->execute(array(':lehrstuhlID' => $lehrstuhlID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$seminar     = $seminare->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Seminare, die ein Lehrstuhl anbietet.
	$seminare       = $pdo->prepare("SELECT *
									 FROM Seminar
									 WHERE Lehrstuhl_ID = :lehrstuhlID
										AND Bewerbungszeitraum_ID = :bewerbungszeitraumID
									 ORDER BY Abschluss");
	$result2     = $seminare->execute(array(':lehrstuhlID' => $lehrstuhlID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
  
	unset($_SESSION['Lehrstuhl_ID']);
  }
}

if(isset($_SESSION['SeminarID'])){
	 $seminarID = $_SESSION['SeminarID'];
	 $semester  = $_SESSION['Semester'];
	 
		//In dem Seminar eingetragene Seminarteilnehmer.
		$teilnehmerDaten       = $pdo->prepare("SELECT *
												FROM Bewerbungszuteilung
												JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
												JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
												WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
													AND Bewerbungszuteilung.Semester = :semester
													AND Bewerbungszuteilung.Zuteilung = '1' 
													AND Bewerbungszuteilung.Ablehnung = '0' ");
		$result      = $teilnehmerDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$teilnehmer  = $teilnehmerDaten->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//In dem Seminar eingetragene Seminarteilnehmer.
		$teilnehmerDaten       = $pdo->prepare("SELECT *
												FROM Bewerbungszuteilung
												JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
												JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
												WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
													AND Bewerbungszuteilung.Semester = :semester
													AND Bewerbungszuteilung.Zuteilung = '1' 
													AND Bewerbungszuteilung.Ablehnung = '0' ");
		$result      = $teilnehmerDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));	

		// Auf diesen SQL wird sowohl in auslastungLehrstuehle.php als auch in uebrigGeblieben.php zugegriffen.
		//Alle Seminarteilnehmer des jeweiligen Seminares zählen.
		$seminarteilnehmerZaehlen = $pdo->prepare("SELECT COUNT(Student_ID)
													FROM Bewerbungszuteilung
													WHERE Seminar_ID = :seminarID
														AND Semester = :semester
														AND Zuteilung = '1' 
														AND Ablehnung = '0' ");
		$result      			  = $seminarteilnehmerZaehlen->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$seminarteilnehmer		  = $seminarteilnehmerZaehlen->fetch();
}

// SQL-Befehle für uebrigGeblieben.php (Anzeigen aller Studenten, die zu keinem ihrer Bewerbungen eine Zusage erhalten haben.) 
/*
if(isset($_GET['Bewerbungszeitraum_ID'])){
	$bewerbungszeitraumID = $_GET['Bewerbungszeitraum_ID'];

	//Alle Studenten, die zu ihren Bewerbungen in dem Bewerbungszeitraum keine Zusage erhalten haben.
	$studenten    = $pdo->prepare("SELECT *
									FROM Student
									JOIN Bewerbungszuteilung ON Bewerbungszuteilung.Student_ID = Student.Student_ID
									JOIN Seminar ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
									WHERE Bewerbungszuteilung.Semester = Seminar.Semester
										AND Zuteilung = '0'
										AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID
									GROUP BY Student.Student_ID");
	$result       = $studenten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));	
	$student      = $studenten->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Studenten, die zu ihren Bewerbungen keine Zusage bekommen erhalten haben.
	$studenten    = $pdo->prepare("SELECT *
									FROM Student
									JOIN Bewerbungszuteilung ON Bewerbungszuteilung.Student_ID = Student.Student_ID
									JOIN Seminar ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
									WHERE Bewerbungszuteilung.Semester = Seminar.Semester
										AND Zuteilung = '0'
										AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID
									GROUP BY Student.Student_ID");
	$result       = $studenten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));	

  if(isset($_SESSION['Student_ID'])){
	$studentID = $_SESSION['Student_ID'];
	
	//Überprüfen, ob der jeweilige Student in dem Bewerbungszeitraum zu einem seiner Bewerbungen zugeteilt wurde.
	$zuteilung    = $pdo->prepare("SELECT *
									FROM Bewerbungszuteilung
									JOIN Seminar ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
									WHERE Bewerbungszuteilung.Semester = Seminar.Semester
										AND Zuteilung = '1'
										AND Student_ID = :studentID
										AND Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result       = $zuteilung->execute(array(':studentID' => $studentID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$zugeteilt    = $zuteilung->fetch();
	
	//Alle Seminare, zu welchen sich der jeweilige Student in diesem Bewerbungszeitraum beworben hat.
	$seminarBewerbung      = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
											WHERE Bewerbungszuteilung.Semester = Seminar.Semester
												AND Zuteilung = '0'
												AND Student_ID = :studentID
												AND Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result      		 = $seminarBewerbung->execute(array(':studentID' => $studentID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$seminareBewerbungen = $seminarBewerbung->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Seminare, zu welchen sich der jeweilige Student beworben hat.
	$seminarBewerbung      = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
											WHERE Bewerbungszuteilung.Semester = Seminar.Semester
												AND Zuteilung = '0'
												AND Student_ID = :studentID
												AND Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result      		 = $seminarBewerbung->execute(array(':studentID' => $studentID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
  
	unset($_SESSION['Student_ID']);
  }
}*/

?>