Un<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if($rolle == 2 || $rolle == 4){
	?>
				<h2> Alle Angelegten Seminare </h2>
	<?php
				if(empty ($angelegt)){
	?>
				<div class="alert alert-warning alert-auto alert-dismissible fade show" role="alert">
					<h5 class="alert-heading">Info:</h5>
						Als Lehrstuhl haben Sie keine Seminare angelegt.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
	<?php
				}else{
	?>
				<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
					<h5 class="alert-heading">Info:</h5>
						Hier sehen Sie alle Seminare, die Sie als Lehrstuhl angelegt haben.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
						
			<div class="table-responsive">	
			<table class="table table table-hover table-bordered">
			<thead>
			<tr>
				<th scope="col"> Semester 						 </th>				
				<th scope="col"> Prüfungsnummer					 </th>				
				<th scope="col"> Seminartitel  					 </th>				
				<th scope="col"> Maximale </br> Teilnehmeranzahl </th>
				<th scope="col"> Bewerberliste     				 </th>
				<th scope="col"> Seminarteilnehmerliste    		 </th>
			</tr>
			</thead>		
	<?php		
					foreach ($lehrstuhlSeminar as $row){ 
	?>
			<tbody>
			<tr>
				<th> <?php echo $row['Semester']; ?> </th>
				<td> <?php echo $row['Seminar_ID']; ?> </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><?php echo $row['Titel']; ?> </font>
					</a> 
				</td>   
				<td> <?php echo $row['Teilnehmeranzahl']; ?> </td> 
				<td> <a href="bewerberliste.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Bewerberübersicht"> Bewerber Anzeigen </font>
					</a> 
				</td>
				<td> <a href="seminarTeilnehmer.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Teilnehmerübersicht"> Seminarteilnehmer Anzeigen </font>
					</a> 
				</td>
			</tr>
			</tbody>
	<?php
					}
	?>
			</table>
		</div>
	<?php 
					if(empty ($ZvorAnmeldeBeginn) && $testmodus['Testmodus'] == "0"){
	?>
			</br>
			<p><a class="btn btn-info" href="seminarAnlegen.php" role="button"> Seminar Anlegen </a></p>
	<?php
					}
				}
				include 'fusszeile.php';
			}else{
				include 'keineBerechtigung.php';
			}
		}
	?>
    </div>
  </body>
</html>
