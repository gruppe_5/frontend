<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
		 <h2> Mein Profil </h2>
		<div class="container-fluid">
      <div class="row">
        <div class="col">
              <h3> 
	<?php 
				if(!empty ($admin)){
					echo $admin['Name'];
				}else{
					if(!empty ($student)){
						echo $student['Vorname'].'&nbsp;';
						echo $student['Name'];
					}else
					if(!empty ($lehrstuhl)){
						echo $lehrstuhl['Bezeichnung'];
					}else
					if(!empty ($studiendekan)){
						echo $studiendekan['Name'];
					}
				}
	?> 
			   </h3>
            </div>
			</div>
            
              <div class="row">
                <div class="col-md-4">
                    <img alt="User Pic" src="userBild.jpg" class="img-thumbnail img-responsive">
    <?php
				if(!empty ($admin)){
	?>
        </div>
				<div class=" col-md-4">
            <div class="card bg-transparent" style="border-color: #eee;"> 
                  <table class="table no-border"> 
                      <tr>
                        <td> <b>Admin ID:</b></td> 
                        <td> <?php echo $admin['Admin_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Name:</b></td>
                        <td> <?php echo $admin['Name']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Email:</b></td>
                        <td> <?php echo $admin['Email']; ?> </td>
                      </tr>
					  <tr>
	<?php
						if($admin['Testmodus'] == "0"){
	?>
                        <td> <form action="befehlProzesse.php" method="POST" >
								<input type="hidden" name="testAktivieren" value="aktivieren">
								<input type="hidden" name="adminEmail" value=<?php echo $email; ?> >
								<button type="submit" class="btn btn-outline-danger btn-sm"> 
									Testmodus Aktivieren
								</button>
							</form>	 </td>
	<?php
						}else{
	?>
                        <td> <form action="befehlProzesse.php" method="POST" >
								<input type="hidden" name="testDeaktivieren" value="deaktivieren">
								<input type="hidden" name="adminEmail" value=<?php echo $email; ?> >
								<button type="submit" class="btn btn-outline-danger btn-sm"> 
									Testmodus Deaktivieren
								</button>
							</form>	 </td>
	<?php
						}
	?>
                      </tr>
                  </table>
                </div>
				  </div>
	<?php
				}else{
					if(!empty ($student)){
  ?>                  <hr>
                    <form class="form-horizontal" action="befehlProzesse.php" method="post" name="upload_pdf" enctype="multipart/form-data">
                    <div class="input-group">
                      <input type="hidden" name="Email" value=<?php echo $email ?> >
                      <input type="file"   name="hisqisAuszug" class="form-control" placeholder="HISQIS-Auszug">
                      <span class="input-group-btn">
                        <input type="submit" name="Upload" title="Laden Sie hier ihren neuen HISQIS-Auszug hoch" class="btn btn-info form-control" float=""value="Upload"/>
                      </span>
                    </div>                  
                  </form>
                  <a href="befehlProzesse.php?Email=<?php echo $email ?>&Name=<?php echo $student['Name']; ?>" class="btn btn-info" role="button">Download letzter HISQIS-Auszug</a>

              </div>
             <div class="col-md-4"> 
                    <div class="card bg-transparent" style="border-color: #eee;">
				  <table class="table no-border">
                      <tr>
                        <td> <b>Matrikelnummer:</b></td>
                        <td> <?php echo $student['Student_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Name:</b> </td>
                        <td> <?php echo $student['Name']; ?> </td>
                      </tr>
					  <tr>
                        <td> <b>Vorname:</b></td>
                        <td> <?php echo $student['Vorname']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Fachsemester:</b></td>
                        <td> <?php echo $student['Fachsemester']; ?> </td>
                      </tr>
					  <tr>
                        <td> <b>Studiengang:</b> </td>
                        <td> <?php echo $student['Studiengang']; ?> </td>
                      </tr>
					  <tr>
                        <td> <b>Abschluss:</b>  </td>
                        <td>  <?php echo $student['Abschluss']; ?>  </td>
                      </tr>
					  <tr>
                        <td> <b>Email:</b> </td>
                        <td> <?php echo $student['Email']; ?>  </td>
                      </tr>
					  <tr>
                        <td> <b>Seminarleistungen:</b> </td>
                        <td> <?php echo $student['Seminarleistungen']; ?>  </td>
                      </tr>
                  </table>
                </div>
				  </div>
	<?php
						if(!empty($zugeteilteSeminare)){
	?>
	
		<div class="col-md-8"> 
		  <div class="card bg-transparent" style="border-color: #eee;">
		  <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">Studienhistorie Anzeigen</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
			<table class="table no-border">
			  <div class="table-responsive">	
			<table class="table table table-hover table-bordered">
			<thead>
			<tr>
				<th scope="col"> Semester 						 </th>				
				<th scope="col"> Prüfungsnummer					 </th>				
				<th scope="col"> Seminartitel  					 </th>
				<th scope="col"> Lehrstuhl     					 </th>	
				<th scope="col"> Status	    					 </th>	
				<th scope="col"> Bestanden     					 </th>				
			</tr>
			</thead>
	<?php
							foreach ($studentZugeteilteSeminare as $row){ 
	?>
			<tbody>
			<tr>
				<th> <?php echo $row['Semester']; ?> </th>
				<td> <?php echo $row['Seminar_ID']; ?> </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><?php echo $row['Titel']; ?> </font></a> </td> 
				<td> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" data-toggle="tooltip" title="Lehrstuhl anzeigen"><font color="black"> 
						<?php echo $row['Bezeichnung']; ?> </font></a> </td>
	<?php
				if($row['Ablehnung'] == "1" && $row['Ablehnung_Datum'] != NULL){
					echo '<td> Seminarplatz Abgelehnt</td>';
				}else
				if($row['Ablehnung'] == "1" && $row['Ablehnung_Datum'] == NULL){
					echo '<td> Seminarplatz Nicht Angenommen. </td>';
				}else{
					echo '<td> Seminarplatz Zugesagt </td>';
				}
				
				if($row['Absolviert'] == "0" && $row['Absolviert_Datum'] == NULL){
					echo '<td><i>  </i></td>';
				}else
				if($row['Absolviert'] == "0" && $row['Absolviert_Datum'] != NULL){
					echo '<td><i> Nein </i></td>';
				}else
				if($row['Absolviert'] == "1"){
					echo '<td><i> Ja </i></td>';
				}else{
					echo '<td> </td>';
				}
	?>
			</tr>
	<?php
							}
	?>
			</tbody>
			</table>
			</div>
			</table>
			</div>
    </div>
  </div>
</div>
		  </div>
		</div>
	<?php
						}
					}else
					if(!empty ($lehrstuhl)){
	?>               
                  </div>
                  <div class="col-md-8"> 
                    <div class="card bg-transparent" style="border-color: #eee;">
				  <table class="table no-border">
                      <tr>
                        <td> <b>Lehrstuhl ID:</b> </td>
                        <td> <?php echo $lehrstuhl['Lehrstuhl_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Bezeichnung:</b></td>
                        <td> <?php echo $lehrstuhl['Bezeichnung']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Email:</b> </td>
                        <td> <?php echo $lehrstuhl['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
        </div>
	<?php
						if(!empty($angelegt)){
	?>
	<div class="col-md-8"> 
		  <div class="card bg-transparent" style="border-color: #eee;">
		  <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">Seminare Anzeigen</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
			<table class="table no-border">
			  <div class="table-responsive">	
			<table class="table table table-hover table-bordered">
			<thead>
			<tr>
				<th scope="col"> Semester 						 </th>				
				<th scope="col"> Prüfungsnummer					 </th>				
				<th scope="col"> Seminartitel  					 </th>				
			</tr>
			</thead>
	<?php
							foreach ($lehrstuhlSeminar as $row){ 
	?>
			<tbody>
			<tr>
				<th> <?php echo $row['Semester']; ?> </th>
				<td> <?php echo $row['Seminar_ID']; ?> </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><?php echo $row['Titel']; ?> </font>
					</a> 
				</td>   
			</tr>
	<?php
							}
	?>
			</tbody>
			</table>
			</div>
			</table>
			</div>
    </div>
  </div>
</div>
		  </div>
		</div>
	<?php
						}
					}else
					if(!empty ($studiendekan)){
	?>             
                  </div>
                  <div class="col-md-8">
                    <div class="card bg-transparent" style="border-color: #eee;"> 
				  <table class="table no-border">
                      <tr>
                        <td> <b>Studiendekan ID:</b></td>
                        <td> <?php echo $studiendekan['Studiendekan_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Name:</b></td>
                        <td> <?php echo $studiendekan['Name']; ?> </td>
                      </tr>
                      <tr>
                        <td> <b>Email:</b> </td>
                        <td> <?php echo $studiendekan['Email']; ?> </td>
                      </tr>
                  </table>
                </div>
              </div>
				  </div>
	<?php
					}
				}
	?>
              </div>
	   </br>
		<table>
	<?php
				if($rolle != 4){
	?>
			<tr> 
				<td><p class="text-muted" align="right"> Bei Änderungen der Profildaten bitte an den Admin wenden. </p></td>
			</tr>
	<?php
				}
	?>
			<tr>
				<td> <a href="passwortAenderung.php" class="btn btn-info" role="button"> Passwort ändern.</a> </td>
			</tr>
		</table>
	<?php
			include 'fusszeile.php';
		}
	?>
    </div>
  </body>
</html>