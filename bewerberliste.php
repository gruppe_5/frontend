<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
		  if(!isset($_GET['Seminar_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Seminarbewerber: <a href="seminar.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><font color="black"> <?php echo $seminar['Titel']; ?> </font></a> </br>
				 Lehrstuhl: <a href="profil2.php?Email=<?php echo $seminar['Email'] ?>" data-toggle="tooltip" title="Weiter zum Lehrstuhl"><font color="black"> <?php echo $seminar['Bezeichnung'] ?> </font></a></h2>	
	<?php
				if(empty ($alleBewerber)){
	?>
			<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Keine Bewerber zu diesem Seminar.
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
				}else{
					if(!empty ($zwZuteilungZeitraum)){
						echo '<h2></br><b> Warteliste ';
						if(!empty($ZnachZweiteZuteilungEnde)){
							echo ': übrig gebliebene Seminarbewerber';
						}
						echo ' </b></h2>';
					}
	?>
			<h4> Es sind bereits <a href="seminarTeilnehmer.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>"> <font color="red">
								 <?php echo $eingetragen[0]; ?></font> </a> Studenten als Teilnehmer in diesem Seminar eingetragen. </h4>
			<h5> Maximale Teilnehmeranzahl: <?php echo $seminar['Teilnehmeranzahl']; ?> Studenten </h5>
	<?php 
					if($rolle != 1){
	?>
		<div class="table-responsive">
			<table class="table">
			
			<th> <h6> Sortieren Nach: </h6> </th>
				<form method="post">
					<input type="hidden" name="sortieren" value="sort">
					<input type="hidden" name="Seminar_ID" value=<?php echo $bewerber['Seminar_ID'] ?> >
					<input type="hidden" name="Semester" value=<?php echo $bewerber['Semester'] ?> >
				<td><label> <input type="radio" name="sortiere" value="1"> Matr.Nr.          </label> </td>
				<td><label> <input type="radio" name="sortiere" value="2"> Studentname       </label> </td>
				<td><label> <input type="radio" name="sortiere" value="3"> Fachsemester       </label> </td>
				<td><label> <input type="radio" name="sortiere" value="4"> Priorität         </label> </td>
				<td><label> <input type="radio" name="sortiere" value="5"> Seminarleistungen </label> </td>
				<td><label> <button type="submit" class="btn btn-outline-info"> Sortieren </button> 		  </label> </td>
				  </form> 
				<td>
					<div>
				   		<div>
						<form class="form-horizontal" align="right" action="befehlProzesse.php" method="post" name="export_csv" enctype="multipart/form-data">
                  			<div class="form-group">
                               	<input type="submit" name="Export_Bewerber" class="btn btn-success" value="Download CSV"/>
								<input type="hidden" name="seminarID" value=<?php echo $seminarID ?> >
								<input type="hidden" name="semester" value=<?php echo $semester ?> >
							</div>
                   		</div>                    
            			</form>
 					</div>
				</td>
			</table>
		 </div>	
	<?php
					}
	?>
		<div class="table-responsive">	
			<table class="table table table-striped table-bordered">
			<thead>	
			<tr>
	<?php
					if((!empty($zuteilungZeitraum)   && $email == $seminar['Email']) ||
					   (!empty($zuteilungZeitraum)   && $rolle == 4) 				 || 
					   (!empty($zwZuteilungZeitraum) && $email == $seminar['Email']) ||
					   (!empty($zwZuteilungZeitraum) && $rolle == 4)				 || 
					   (!empty($ZnachZweiteZuteilungEnde) && $rolle == 3)			 ||
					   (!empty($ZnachZweiteZuteilungEnde) && $rolle == 4) ){
						
	?>
				<th scope="col"> Auswählen        	</th>
	<?php
					}else{
	?>
				<th scope="col">        	</th>
	<?php		
					}
	?>
				<th scope="col"> Anzahl            </th>
				<th scope="col"> Matrikelnummer    </th>
				<th scope="col"> Vorname           </th>
				<th scope="col"> Name              </th>
				<th scope="col"> Email             </th>
				<th scope="col"> Studiengang	   </th>
				<th scope="col"> Fachsemester      </th>
				<th scope="col"> Priorität         </th>
				<th scope="col"> Seminarleistungen </th>
				<th scope="col"> Bewerbung am      </th>	
			</tr>
			</thead>
			<tbody>		

	<?php
					$error = false; //Wenn kein Sortierelement ausgewählt ist, wird $error auf true gesetzt und die unsortierte Ausgabe der Bewerber folgt.
					if (isset ($_POST['sortieren']) && !empty($_POST['sortiere'])){
						$sortiere  = $_POST['sortiere'];
						include 'sqlSort.php';
						
						switch ($sortiere){
							case "1":
								echo  '<div class="alert alert-success alert-dismissible fade show top0" role="alert">
										Sortiert nach Matrikelnummer.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									   </div>';
								break;
							case "2":
								echo  '<div class="alert alert-success alert-dismissible fade show top0" role="alert">
										Sortiert nach Studentname.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									   </div>';
								break;
							case "3":
								echo  '<div class="alert alert-success alert-dismissible fade show top0" role="alert">
										Sortiert nach Fachsemester.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									   </div>';
								break;
							case "4":
								echo  '<div class="alert alert-success alert-dismissible fade show top0" role="alert">
										Sortiert nach Priorität.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									   </div>';
								break;
							case "5":
								echo  '<div class="alert alert-success alert-dismissible fade show top0" role="alert">
										Sortiert nach Seminarleistungen.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									   </div>';
								break;
						default:
							echo '';
						}				
						
						$i = 1; //Zählt die Anzahl der Bewerber durch.
						$z = 0; //Zählt die Zahl der zugeteilten Studenten.
						foreach ($bewerberDatenSort as $row){ 
	?>
			<tr>
					<form action="befehlProzesse.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" method="POST">
					<input type="hidden" name="zuteilen"		 	value="zuteile">
					<input type="hidden" name="anzahl" 			 	value="<?php echo $row['Teilnehmeranzahl']; ?>" >
					<input type="hidden" name="seminarleistungen[]" value="<?php echo $row['Seminarleistungen']; ?>" >
					<input type="hidden" name="studentName[]" 	  	value="<?php echo $row['Name']; ?>" >
					<input type="hidden" name="studentVorname[]" 	value="<?php echo $row['Vorname']; ?>" >
					<input type="hidden" name="email[]" 	 		value="<?php echo $row['Email']; ?>" >
	<?php
						if($row['Zuteilung'] == "0"){
							if((!empty($zuteilungZeitraum)   && $email == $seminar['Email']) ||
								(!empty($zuteilungZeitraum)   && $rolle == 4) 				 || 
								(!empty($zwZuteilungZeitraum) && $email == $seminar['Email']) ||
								(!empty($zwZuteilungZeitraum) && $rolle == 4)				 || 
								(!empty($ZnachZweiteZuteilungEnde) && $rolle == 3)			 ||
								(!empty($ZnachZweiteZuteilungEnde) && $rolle == 4) ){
	?>
				<td> <input type="checkbox" name="student[]" value=<?php echo $row['Student_ID']; ?> ></td>
	<?php
							}else{
								echo '<td> </td>';
							}
						}else{
							if($row['Ablehnung'] == 1 && $row['Ablehnung_Datum'] != NULL){
	?>
				<td> Seminarplatz </br> Abgelehnt </td>
	<?php
							}else
							if(!empty ($ZnachAblehnungEnde)){
								if($row['Ablehnung'] == 1 && $row['Ablehnung_Datum'] == NULL){
	?>
				<td> Seminarplatz </br> Nicht </br> Angenommen </td>
	<?php
								}else{
									echo '<td> Teilnehmer </td>';
								}
							}else{
								$z++;
	?>
				<td> Zugeteilt </td>
	<?php	
							}
						}
	?>
				<th scope="row"> <?php echo $i; ?>              </th>
				<td> <?php echo $row['Student_ID']; ?>          </td>
				<td> <?php echo $row['Vorname']; ?>             </td> 
				<td> <?php echo $row['Name']; ?>                </td> 
				<td> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" data-toggle="tooltip" title="Weiter zum Profil"> 
						<font color="black"><?php echo $row['Email']; ?></font></a></td>   
				<td> <?php echo $row['Studiengang']; ?>   		</td>  
				<td> <?php echo $row['Fachsemester']; ?>   		</td>  
				<td> <?php echo $row['Prioritaet']; ?>          </td>
				<td> <?php echo $row['Seminarleistungen']; ?>   </td>
				<td> <?php $date = new DateTime($row['Bewerbung_Datum']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
			</tbody>
	<?php
					$i++;
						}
				}else
				if(isset ($_POST['sortieren']) && empty($_POST['sortiere'])){
					echo '<div class="alert alert-warning alert-dismissible fade show top0" role="alert">
							 Kein Sortierkriterium ausgewählt. Bitte ein Kriterium auswählen.
							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
					$error = true;
				}
				//Wenn kein Sortierelement ausgewählt wurde, werden die Bewerber unsortiert ausgegeben.
				if(!isset ($_POST['sortieren']) || $error == true){
					
					$i = 1; //Zählt die Anzahl der Bewerber durch.
					$z = 0; //Zählt die Zahl der zugeteilten Studenten.
					foreach ($alleBewerberDaten as $row){ 
	?>
			<tr>
					<form action="befehlProzesse.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" method="POST">
					<input type="hidden" name="zuteilen"		 	value="zuteile">
					<input type="hidden" name="anzahl" 			 	value="<?php echo $row['Teilnehmeranzahl']; ?>" >
					<input type="hidden" name="seminarleistungen[]" value="<?php echo $row['Seminarleistungen']; ?>" >
					<input type="hidden" name="studentName[]" 	  	value="<?php echo $row['Name']; ?>" >
					<input type="hidden" name="studentVorname[]" 	value="<?php echo $row['Vorname']; ?>" >
					<input type="hidden" name="email[]" 	 		value="<?php echo $row['Email']; ?>" >
					<input type="hidden" name="titel" 	 			value="<?php echo $seminar['Titel']; ?>" >
	<?php
						if($row['Zuteilung'] == "0"){
							if((!empty($zuteilungZeitraum)   && $email == $seminar['Email']) ||
								(!empty($zuteilungZeitraum)   && $rolle == 4) 				 || 
								(!empty($zwZuteilungZeitraum) && $email == $seminar['Email']) ||
								(!empty($zwZuteilungZeitraum) && $rolle == 4)				 || 
								(!empty($ZnachZweiteZuteilungEnde) && $rolle == 3)			 ||
								(!empty($ZnachZweiteZuteilungEnde) && $rolle == 4) ){
	?>
				<td> <input type="checkbox" name="student[]" value=<?php echo $row['Student_ID']; ?> ></td>
	<?php
							}else{
								echo '<td> </td>';
							}
						}else{
							if($row['Ablehnung'] == 1 && $row['Ablehnung_Datum'] != NULL){
	?>
				<td> Seminarplatz </br> Abgelehnt </td>
	<?php
							}else
							if(!empty ($ZnachAblehnungEnde)){
								if($row['Ablehnung'] == 1 && $row['Ablehnung_Datum'] == NULL){
	?>
				<td> Seminarplatz </br> Nicht </br> Angenommen </td>
	<?php
								}else{
									echo '<td> Teilnehmer </td>';
								}
							}else{
								$z++;
	?>
				<td> Zugeteilt </td>
	<?php	
							}
						}
	?>
				<th scope="row"> <?php echo $i; ?>              </th>
				<td> <?php echo $row['Student_ID']; ?>          </td>
				<td> <?php echo $row['Vorname']; ?>             </td> 
				<td> <?php echo $row['Name']; ?>                </td> 
				<td> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" data-toggle="tooltip" title="Weiter zum Profil"> 
						<font color="black"><?php echo $row['Email']; ?></font></a></td>   
				<td> <?php echo $row['Studiengang']; ?>   		</td>  
				<td> <?php echo $row['Fachsemester']; ?>   		</td>  
				<td> <?php echo $row['Prioritaet']; ?>          </td>
				<td> <?php echo $row['Seminarleistungen']; ?>   </td>
				<td> <?php $date = new DateTime($row['Bewerbung_Datum']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
			</tbody>
	<?php
					$i++;
					}
				}
	?>	
			</table>
		</div>
	<?php
					if((!empty($zuteilungZeitraum)   && $email == $seminar['Email']) ||
					   (!empty($zuteilungZeitraum)   && $rolle == 4) 				 || 
					   (!empty($zwZuteilungZeitraum) && $email == $seminar['Email']) ||
					   (!empty($zwZuteilungZeitraum) && $rolle == 4)				 || 
					   (!empty($ZnachZweiteZuteilungEnde) && $rolle == 3)			 ||
					   (!empty($ZnachZweiteZuteilungEnde) && $rolle == 4) ){
	?>
			</br> 
			<h5> Zugeteilte Studenten: <?php echo $z; ?> </br>
				 Es können noch <?php echo $seminar['Teilnehmeranzahl']-$z; ?> Studenten hinzugefügt werden. </h5>
			<input type="hidden" name="zugeteilteTeilnehmer" value="<?php echo $z ?>" > 
			<button type="submit" class="btn btn-outline-info"> Seminarteilnehmer Hinzufügen</button>
	<?php
					}
	?>
				</form>
	<?php	
				}
			include 'fusszeile.php';
		  }
		}
	?>
    </div>
  </body>
</html>
