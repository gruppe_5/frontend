<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container">
	 
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'meldung.php';
	?>	
	<div class="text-center">
		<img alt="icon" src="bilder/icon.png" class="img-responsive" style="width:17%";>
	</div>
      
	  <form action="befehlProzesse.php" method="POST" class="form-signin">
	  <input type="hidden"      name="passwortZuruecksetzen" value="zuruecksetzen">
        <h2 class="form-signin-heading"> Passwort Zurücksetzen </h2>
		<label for="id"     	  class="sr-only"> ID oder Matrikelnummer </label>
        <input type="text"        name="id"       class="form-control" placeholder="ID oder Matrikelnummer" required>
		<label for="email"        class="sr-only"> E-Mail Adresse </label>
        <input type="email"       name="email"    class="form-control" placeholder="E-Mail Adresse" required autofocus>	
	   <button class="btn btn-lg btn-primary btn-block" type="submit"> Passwort Zurücksetzen</button>
	   </form>
	   
	   <form class="form-signin">
	   <a href="index.php" class="btn btn-lg btn-primary btn-block"> Abbrechen </a> </button>
	   </form>
	<?php
		}else{
			include 'keineBerechtigung.php';
		}
		include 'fusszeile.php';
	?>
    </div>
  </body>
</html>
