3<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
		  if(!isset ($_GET['Email'])){
			include 'keineBerechtigung.php';
		  }else{
			$email2        = $_GET['Email'];
			
			if($email == $email2){
				//include 'profil.php';
				header("Location:profil.php");
				exit;
			}else{
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
	?>
		 <h2> Profil </h2>
		
      	<div class="container-fluid">
      <div class="row">
        <div class="col">
              <h3>
	<?php 
				if(!empty ($admin)){
					echo $admin['Name'];
				}else{
					if(!empty ($student)){
						echo $student['Vorname'].'&nbsp;';
						echo $student['Name'];
					}else
					if(!empty ($lehrstuhl)){
						echo $lehrstuhl['Bezeichnung'];
					}else
					if(!empty ($studiendekan)){
						echo $studiendekan['Name'];
					}
				}
	?> 
			   </h3>
            </div>
        </div>
			
            <div class="row">
                <div class="col-md-4">
                    <img alt="User Pic" src="userBild.jpg" class="img-thumbnail img-responsive">    
    <?php
				if(!empty ($admin)){
	?>
				</div>
				<div class=" col-md-4">
            		<div class="card bg-transparent" style="border-color: #eee;"> 
                  <table class="table no-border">
                      <tr>
                        <td> Admin ID: &nbsp; </td> 
                        <td> <?php echo $admin['Admin_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> Name: &nbsp; </td>
                        <td> <?php echo $admin['Name']; ?> </td>
                      </tr>
                      <tr>
                        <td> Email: &nbsp; </td>
                        <td> <?php echo $admin['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
				  </div>
	<?php
				}else{
					if(!empty ($student)){
	?>
                                 <hr>
	<?php
					if($rolle != 1){
	?>
              		<a href="befehlProzesse.php?Email=<?php echo $email ?>&Name=<?php echo $student['Name']; ?>" class="btn btn-info" role="button">Download letzten Hisqis-Auszug</a>
	<?php
					}
	?>
                  </div>
                  <div class="col-md-4"> 
                    <div class="card bg-transparent" style="border-color: #eee;">
				  <table class="table no-border">
                      <tr>
                        <td> Matrikelnummer: &nbsp; </td>
                        <td> <?php echo $student['Student_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> Name: &nbsp; </td>
                        <td> <?php echo $student['Name']; ?> </td>
                      </tr>
					  <tr>
                        <td> Vorname: &nbsp; </td>
                        <td> <?php echo $student['Vorname']; ?> </td>
                      </tr>
                      <tr>
                        <td> Fachsemester: &nbsp; </td>
                        <td> <?php echo $student['Fachsemester']; ?> </td>
                      </tr>
					  <tr>
                        <td> Studiengang: &nbsp; </td>
                        <td> <?php echo $student['Studiengang']; ?> </td>
                      </tr>
					  <tr>
                        <td> Abschluss: &nbsp; </td>
                        <td>  <?php echo $student['Abschluss']; ?>  </td>
                      </tr>
					  <tr>
                        <td> Email: &nbsp; </td>
                        <td> <?php echo $student['Email']; ?>  </td>
                      </tr>
					  <tr>
                        <td> Seminarleistungen: &nbsp; </td>
                        <td> <?php echo $student['Seminarleistungen']; ?>  </td>
                      </tr>
                  </table>
   
				  </div>
				</div>
	<?php
						if(!empty($zugeteilteSeminare)){
	?>
	<div class="col-md-8"> 
		  <div class="card bg-transparent" style="border-color: #eee;">
		  <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">Studienhistorie Anzeigen</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
			<table class="table table table-hover table-bordered">
			<thead>
			<tr>
				<th scope="col"> Semester 						 </th>				
				<th scope="col"> Prüfungsnummer					 </th>				
				<th scope="col"> Seminartitel  					 </th>
				<th scope="col"> Lehrstuhl     					 </th>	
				<th scope="col"> Status	    					 </th>	
				<th scope="col"> Bestanden     					 </th>				
			</tr>
			</thead>
	<?php
							foreach ($studentZugeteilteSeminare as $row){ 
	?>
			<tbody>
			<tr>
				<th> <?php echo $row['Semester']; ?> </th>
				<td> <?php echo $row['Seminar_ID']; ?> </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><?php echo $row['Titel']; ?> </font></a> </td> 
				<td> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" data-toggle="tooltip" title="Lehrstuhl anzeigen"><font color="black"> 
						<?php echo $row['Bezeichnung']; ?> </font></a> </td>
	<?php
				if($row['Ablehnung'] == 1 && $row['Ablehnung_Datum'] != NULL){
					echo '<td> Seminarplatz Abgelehnt</td>';
				}else
				if($row['Ablehnung'] == 1 && $row['Ablehnung_Datum'] == NULL){
					echo '<td> Seminarplatz Nicht Angenommen. </td>';
				}else{
					echo '<td> Seminarplatz Zugesagt </td>';
				}
				
				if($row['Absolviert'] == "0" && $row['Absolviert_Datum'] == NULL){
					echo '<td><i>  </i></td>';
				}else
				if($row['Absolviert'] == "0" && $row['Absolviert_Datum'] != NULL){
					echo '<td><i> Nein </i></td>';
				}else
				if($row['Absolviert'] == "1"){
					echo '<td><i> Ja </i></td>';
				}else{
					echo '<td> </td>';
				}
	?>
			</tr>
	<?php
							}
	?>
			</tbody>
			</table>
			</div>
			</table>
			</div>
    </div>
  </div>
</div>
		  </div>
		</div>
	<?php
						}
					}else
					if(!empty ($lehrstuhl)){
	?>
                </div>
                   <div class="col-md-4"> 
                    <div class="card bg-transparent" style="border-color: #eee;">
				  <table class="table no-border">
                      <tr>
                        <td> Lehrstuhl ID: &nbsp; </td>
                        <td> <?php echo $lehrstuhl['Lehrstuhl_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> Bezeichnung: &nbsp; </td>
                        <td> <?php echo $lehrstuhl['Bezeichnung']; ?> </td>
                      </tr>
                      <tr>
                        <td> Email: &nbsp; </td>
                        <td> <?php echo $lehrstuhl['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
				</div>
	<?php
						if(!empty($angelegt)){
	?>
	<div class="col-md-8"> 
		  <div class="card bg-transparent" style="border-color: #eee;">
		  <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">Seminare Anzeigen</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
			<table class="table no-border">
			  <div class="table-responsive">	
			<table class="table table table-hover table-bordered">
			<thead>
			<tr>
				<th scope="col"> Semester 						 </th>				
				<th scope="col"> Prüfungsnummer					 </th>				
				<th scope="col"> Seminartitel  					 </th>				
			</tr>
			</thead>
	<?php
							foreach ($lehrstuhlSeminar as $row){ 
	?>
			<tbody>
			<tr>
				<th> <?php echo $row['Semester']; ?> </th>
				<td> <?php echo $row['Seminar_ID']; ?> </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>"> 
						<font color="black" data-toggle="tooltip" title="Weiter zur Seminarübersicht"><?php echo $row['Titel']; ?> </font>
					</a> 
				</td>   
			</tr>
	<?php
							}
	?>
			</tbody>
			</table>
			</div>
			</table>
			</div>
    </div>
  </div>
</div>
		  </div>
		</div>
	<?php
						}
					}else
					if(!empty ($studiendekan)){
	?>
				</div>
                  <div class="col-md-4">
                    <div class="card bg-transparent" style="border-color: #eee;"> 				  
                    <table class="table no-border">
                      <tr>
                        <td> Studiendekan ID: &nbsp; </td>
                        <td> <?php echo $studiendekan['Studiendekan_ID']; ?> </td>
                      </tr>
                      <tr>
                        <td> Name: &nbsp; </td>
                        <td> <?php echo $studiendekan['Name']; ?> </td>
                      </tr>
                      <tr>
                        <td> Email: &nbsp; </td>
                        <td> <?php echo $studiendekan['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
				</div>
	<?php
					}
				}
	?>
                </div>
              </div>
	<?php
			include 'fusszeile.php';
			}
		  }
		}
	?>
    </div>
  </body>
</html>