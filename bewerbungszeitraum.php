<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	
		  if(!isset($_GET['Bewerbungszeitraum_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$bewerbungszeitraumID   = $_GET['Bewerbungszeitraum_ID'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if(empty ($bewerbungszeitraum)){
				echo 'Kein Inhalt zu diesem Zeitraum.';
			}else{
	?>
			<h2> Bewerbungszeitraum: <?php echo $bewerbungszeitraum['Name']; ?> </h2>			
	<?php
				if($rolle == 3 || $rolle == 4){
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Die genauen Bewerbungsfristen sind hier aufgelistet.
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
				}
	?>
			<table class="table table-sm no-border">
			<tr>
				<th> Anmelde Anfang:</th>
				<td> <?php $date = new DateTime($bewerbungszeitraum['Anmelde_Anfang']);
							echo $date->format('d.m.Y H:i'); ?> </td>     
			</tr>
			<tr>
				<td><p class="text-muted"> Beginn der Bewerbungsphase für die Seminare.</p></td>
			</tr>
			<tr>
				<th> Anmelde Ende:</th>
				<td> <?php $date = new DateTime($bewerbungszeitraum['Anmelde_Ende']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
			<tr>
				<td><p class="text-muted" >Ende der Bewerbungsphase für die Seminare.</p> </td>
			</tr>
			<tr>
				<th> 1. Zuteilung: </th>
				<td> <?php $date = new DateTime($bewerbungszeitraum['Zuteilung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
			<tr>
				<td><p class="text-muted" >Die 1. Runde zum Zuteilen der Studierenden zu den Seminaren nach Ablauf der Bewerbungsphase. </p></td>
			</tr>
				<th> Ablehnungsfrist: </th>
				<td> <?php $date = new DateTime($bewerbungszeitraum['Ablehnung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
			<tr>
				<td><p class="text-muted">Die Frist für die Studierenden zum Ablehnen eines zugeteilten Seminarplatzes. </p></td>
			</tr>
			<tr>
				<th> 2. Zuteilung:</th>
				<td><?php $date = new DateTime($bewerbungszeitraum['Zweite_Zuteilung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?> </td>   
			</tr>
			<tr>
				<td><p class="text-muted" >Die 2. Runde zum Zuteilen der Studierenden zu den Seminaren nach Ablauf der Ablehnungsfrist. </p></td>
			</tr>
			<tr>
				<th> Frist zur Themenverteilung: </th>
				<td><?php $date = new DateTime($bewerbungszeitraum['Themen_Zuteilung_Deadline']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
			<tr>
				<td><p class="text-muted">Die Frist für Lehrstühle zum Verteilen der Seminarthemen an ihre Seminarteilnehmer.</p></td>
			</tr>
			</table>
	<?php
			}
			if((!empty ($bewerbungszeitraumBearbeiten) && $bewerbungszeitraeume['Bewerbungszeitraum_ID'] == $bewerbungszeitraumID) || ($testmodus['Testmodus'] == "1")){
				if($rolle == 3 || $rolle == 4){
	?>
			<p><a class="btn btn-info" href="bewerbungszeitraumBearbeiten.php?Bewerbungszeitraum_ID=<?php echo $bewerbungszeitraum['Bewerbungszeitraum_ID'] ?>" role="button"><i class="material-icons"  
				style="font-size:15px">create</i> Bearbeiten</a></p>
	<?php
				}
			}
			include 'fusszeile.php';
		  }
		}
	?>
    </div>
  </body>
</html>
