
<!doctype html>
<html lang="en">
  <head>
    <title>Fixed Navbar </title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap-4.0.0-beta.2/bootstrap-4.0.0-beta.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../bootstrap-4.0.0-beta.2/bootstrap-4.0.0-beta.2/docs/4.0/examples/signin/signin.css" rel="stylesheet">
  	
  </head>

  <body>
	<div class="container">
  
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	?>
  
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #28497c;">
    	<a class="navbar-brand" href="index.php">Seminarvergabesystem</a>
    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
 		</button>

     	<div class="collapse navbar-collapse" id="navbarCollapse">
        	<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          		<li class="nav-item active">
            		<a class="nav-link" href="index.php"> Startseite <span class="sr-only">(current)</span></a>
          		</li>

		  		<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="seminare.php" name="navbarSeminarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Seminare</a>
				
				<div class="dropdown-menu" aria-labelledby="navbarSeminarDropdown">
					<?php
						if($rolle == 1 || $rolle == 4){
					?>
				<a class="dropdown-item" href="bewerbungen.php">Seminarbewerbung</a>
				<?php
					}
					if($rolle == 2 || $rolle == 4){
				?>
				<a class="dropdown-item" href="seminarAnlegen.php">Seminar Anlegen</a>
				<?php
					}
					if($rolle == 3 || $rolle == 4){
				?>
				<a class="dropdown-item" href="bewerbungszeitraumFestlegen.php">Bewerbungszeitraum Festlegen</a>
				<?php
					}
				?>
				</div>

		  		</li>
				<?php
					if($rolle == 4){
				?>

		  		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" name="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Anlegen: </a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="studiendekanAnlegen.php"> Studiendekan Anlegen </a>
				<a class="dropdown-item" href="lehrstuhlAnlegen.php"> Lehrstuhl Anlegen </a>
			</div>
		  </li>
			<?php	
				}
			?>
        </ul>

		<ul class="navbar-nav navbar-right col-md-4">
    		<li class="nav-item">
    			<span class="navbar-text navbar-right">Sie sind angemeldet als: <a href="profil.php" class="navbar-link" data-toggle="tooltip" data-placement="top" title="Weiter zum Profil">
					<?php if($rolle == 1){ echo ' Student'; }
						  if($rolle == 2){ echo ' Lehrstuhl'; }
						  if($rolle == 3){ echo ' Studiendekan'; }
						  if($rolle == 4){ echo ' Admin'; }
					?> 
				</a></span>
   			</li>
   		</ul>
        <form action="befehlProzesse.php" method="POST" class="form-inline mt-2 mt-md-0">
			<input type="hidden" name="logout" value="logout">
			<button class="btn btn-outline-light my-2 my-sm-0" type="submit"> Abmelden </button>
        </form>
      </div>
    </nav>

    <main role="main" class="container">
      <div class="page-header">

	  </div>
    </main>



	<?php
		}
	?>
	</div>
  </body>
</html>
