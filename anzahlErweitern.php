<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
		  if(!isset($_GET['Seminar_ID']) && !isset($_GET['Semester'])){
			  include 'keineBerechtigung.php';
		  }else{
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];
		
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
		<h2> Seminarteilnehmeranzahl Erweitern: <?php echo $seminar['Titel']; ?>  im <?php echo $seminar['Semester'] ?></h2>
	<?php
			if($rolle == 3 || $rolle ==4){
				if(empty ($seminar)){
					echo 'Kein Inhalt zu diesem Seminar.';
				}else{
	?>
		<div class="col-md-8">
			<table class="table no-border">
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="anzahlErweitern"   value="erweitern">
				<input type="hidden" name="seminarID"         value=<?php echo $seminarID ?> >
				<input type="hidden" name="semester"          value=<?php echo $semester ?> >
			<tr>
				<th style="witdh:100px"> Prüfungsnummer: </br></th>      
				<td> <?php echo $seminarID ?> </td>
			</tr>
			<tr>
				<th> Titel:</th>
				<td> <?php echo $seminar['Titel'] ?> </td>
			</tr>
			<tr>
				<th> Beschreibung:</th>
				<td> <?php echo $seminar['Beschreibung'] ?> </td>
			</tr>
			<tr>
				<th> Maximale Teilnehmeranzahl:</th>
				<td> <input style="width:23%" type="number" min="1" name="teilnehmeranzahl" class="form-control" value="<?php echo $seminar['Teilnehmeranzahl'] ?>" required> </td>
			</tr>
			<tr>
				<th> Abschluss:</th>      
				<td> <?php echo $seminar['Abschluss']; ?> </td>
			</tr>
			</table>
		
		
				 <button type="submit" class="btn btn-info"> Teilnehmeranzahl ändern </button>
				</form>
					 <a href="seminar.php?Seminar_ID=<?php echo $seminarID ?>&Semester=<?php echo $semester ?>" class="btn btn-info"> Abbrechen </a></th>
			</div>

	<?php
					include 'fusszeile.php';
				}
			}else{
				include 'keineBerechtigung.php';
			}
		  }
		}
	?>
    </div>
  </body>
</html>
