<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Bewerbungszeiträume </h2>
	<?php
			if(empty ($bewerbungszeitraeume)){
	?>
			<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Keine Bewerbungszeiträume vorhanden.
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
			}else{
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Hier können Sie die genauen Fristen der einzelnen Bewerbungszeiträume einsehen.
	<?php
				if(!empty ($altesBewerbungszeitraum)){
					if($rolle == 3 || $rolle == 4){
	?>
					</br></br><i class="material-icons"  style="font-size:15px">delete</i>= 
					Löscht den Bewerbungszeitraum aller dazugehörigen Seminare von Lehrstühlen und Bewerbungen von Studenten in dem Semester.
	<?php
					}
				}
	?>
						</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			</div>

			<p class="text-muted">Semester:</p>
			
			<ul>
				<table>
	<?php
				foreach ($bewZeitraeume as $row){ 
					$zeitraumID = $row['Bewerbungszeitraum_ID'];
					$_SESSION['Zeitraum_ID'] = $zeitraumID;
					include 'sql.php'; //Nochmal einbinden, da $zeitraumID neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
	?>
				<tr>
					<td> <li> <a href="bewerbungszeitraum.php?Bewerbungszeitraum_ID=<?php echo $row['Bewerbungszeitraum_ID'] ?>" title="Hier geht es weiter zum Bewerbungszeitraum"> 
								<font color="black"><?php echo $row['Name']; ?></font> </a></li>
	<?php	
					if(!empty ($altesBewerbungszeitraum)){
						if($rolle == 3 || $rolle == 4){
	?>
					<td> <form action="befehlProzesse.php" method="POST" >
						<input type="hidden" name="alleSeminareLoeschen" value="loeschen">
						<input type="hidden" name="zeitraumID" value=<?php echo $zeitraumID; ?> >
						 &nbsp; &nbsp; <button type="submit"><i class="material-icons"  style="font-size:15px">delete</i>
						</button>
					</form>	</td>
	<?php
						}
					}	
	?>
				</tr>
	<?php
				}
	?>
				</table>
			</ul>
	<?php
			}
			if(!empty($bewerbungszeitraumFestlegen)){
				if($rolle == 3 || $rolle == 4){
	?>
			</br>
				<p><a class="btn btn-info" href="bewerbungszeitraumFestlegen.php" role="button"> Bewerbungszeitraum Festlegen.</a></p>
	<?php
				}
			}
			include 'fusszeile.php';
	}
	?>
    </div>
  </body>
</html>
