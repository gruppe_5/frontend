<?php

// SQL-Befehle für uebrigGeblieben.php (Anzeigen aller Studenten, die zu keinem ihrer Bewerbungen eine Zusage erhalten haben.) 

		


if(isset($_GET['Bewerbungszeitraum_ID'])){
	$bewerbungszeitraumID = $_GET['Bewerbungszeitraum_ID'];
	
	//Den Namen des Bewerbungszeitraums für die beiden Sichten auslastungLehrstuehle.php und uebrigGeblieben.php abfragen.
	$bewerbungszeitraumDaten = $pdo->prepare("SELECT Name
											  FROM Bewerbungszeitraum
											  WHERE Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result                 = $bewerbungszeitraumDaten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$bewerbungszeitraum     = $bewerbungszeitraumDaten->fetch();

	//Alle Studenten, die zu ihren Bewerbungen in dem Bewerbungszeitraum keine Zusage erhalten haben.
	$studenten    = $pdo->prepare("SELECT *
									FROM Student
									JOIN Bewerbungszuteilung ON Bewerbungszuteilung.Student_ID = Student.Student_ID
									JOIN Seminar ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
									WHERE Bewerbungszuteilung.Semester = Seminar.Semester
										AND Zuteilung = '0'
										AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID
								");
	$result       = $studenten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));	
	$student      = $studenten->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Studenten, die zu ihren Bewerbungen keine Zusage erhalten haben.
	$studenten    = $pdo->prepare("SELECT *
									FROM Student
									JOIN Bewerbungszuteilung ON Bewerbungszuteilung.Student_ID = Student.Student_ID
									JOIN Seminar ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
									WHERE Bewerbungszuteilung.Semester = Seminar.Semester
										AND Zuteilung = '0'
										AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID
									");
	$result       = $studenten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));	


	if(isset($_SESSION['SeminarID'])){
	 $seminarID = $_SESSION['SeminarID'];
	 $semester  = $_SESSION['Semester'];


		//Alle Seminarteilnehmer des jeweiligen Seminares zählen.
		$seminarteilnehmerZaehlen = $pdo->prepare("SELECT COUNT(Student_ID)
													FROM Bewerbungszuteilung
													WHERE Seminar_ID = :seminarID
														AND Semester = :semester
														AND Zuteilung = '1' 
														AND Ablehnung = '0' ");
		$result      			  = $seminarteilnehmerZaehlen->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$seminarteilnehmer		  = $seminarteilnehmerZaehlen->fetch();
	}


  if(isset($_SESSION['Student_ID'])){
	$studentID = $_SESSION['Student_ID'];
	
	//Überprüfen, ob der jeweilige Student in dem Bewerbungszeitraum zu einem seiner Bewerbungen zugeteilt wurde.
	$zuteilung    = $pdo->prepare("SELECT *
									FROM Bewerbungszuteilung
									JOIN Seminar ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
									WHERE Bewerbungszuteilung.Semester = Seminar.Semester
										AND Zuteilung = '1'
										AND Bewerbungszuteilung.Student_ID = :studentID
										AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result       = $zuteilung->execute(array(':studentID' => $studentID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$zugeteilt    = $zuteilung->fetch();
	
	//Alle Seminare, zu welchen sich der jeweilige Student in diesem Bewerbungszeitraum beworben hat.
	$seminarBewerbung      = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
											WHERE Bewerbungszuteilung.Semester = Seminar.Semester
												AND Zuteilung = '0'
												AND Bewerbungszuteilung.Student_ID = :studentID
												AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result      		 = $seminarBewerbung->execute(array(':studentID' => $studentID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
	$seminareBewerbungen = $seminarBewerbung->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Seminare, zu welchen sich der jeweilige Student beworben hat.
	$seminarBewerbung      = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
											WHERE Bewerbungszuteilung.Semester = Seminar.Semester
												AND Zuteilung = '0'
												AND Bewerbungszuteilung.Student_ID = :studentID
												AND Seminar.Bewerbungszeitraum_ID = :bewerbungszeitraumID");
	$result      		 = $seminarBewerbung->execute(array(':studentID' => $studentID, ':bewerbungszeitraumID' => $bewerbungszeitraumID));
  
	unset($_SESSION['Student_ID']);
  }
}

?>