<?php
	$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
				Keine Berechtigung zum Aufrufen dieser Seite!
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>';
	$_SESSION['Error'] = $errorMsg;
	header("Location:index.php");
	exit;
?>