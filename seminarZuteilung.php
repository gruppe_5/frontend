<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if($rolle == 1 || $rolle == 4){
	?>
			<h2> Meine Seminare </h2>
	<?php
				
				if(empty ($zugeteilteSeminare)){
	?>
			<div class="alert alert-warning alert-auto alert-dismissible fade show" role="alert">
					<h5 class="alert-heading">Info:</h5>
					Keine Seminare vorhanden.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
	<?php
				}else{
	?>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
		<thead>
			<tr>
				<th scope="col"> Semester 		</th>			
				<th scope="col"> Prüfungsnummer </th>			
				<th scope="col"> Seminartitel   </th>				
				<th scope="col"> Lehrstuhl      </th>
				<th scope="col"> Priorität      </th>
				<th scope="col"> Zugeteilt am   </th>
				<th scope="col"> Status         </th>
				<th scope="col"> am             </th>
				<th scope="col">                </th>
				<th scope="col"> Bestanden      </th>
				<th scope="col"> am             </th>
			</tr>
		</thead>				
	<?php		
					foreach ($studentZugeteilteSeminare as $row){ 
	?>
		<tbody>
			<tr>
				<td> <?php echo $row['Semester']; ?>  </td>
				<td> <?php echo $row['Seminar_ID']; ?>  </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" title="Weiter zur Seminarübersicht"> 
						<font color="black"><?php echo $row['Titel']; ?></font>
					</a> 
				</td>   
				<td> <?php echo $row['Bezeichnung']; ?> </td>
				<td> <?php echo $row['Prioritaet']; ?>  </td>
				<td> <?php $date = new DateTime($row['Zuteilung_Datum']);
							echo $date->format('d.m.Y H:i'); ?>  </td>
	<?php
						if(($row['Ablehnung_Datum'] == NULL && !empty($ablehnZeitraum)) ||
						   ($row['Ablehnung_Datum'] == "0000-00-00 00:00:00" && !empty($ablehnZeitraum))){
	?>
				<td>
					<form action="befehlProzesse.php" method="POST">
					<input type="hidden" name="zusagen"         value="zusage">
					<input type="hidden" name="seminarID"       value=<?php echo $row['Seminar_ID'] ?> >
					<input type="hidden" name="semester"        value=<?php echo $row['Semester'] ?> >
					<input type="hidden" name="studentID"       value=<?php echo $row['Student_ID'] ?> >
					<input type="hidden" name="seminarleistung" value=<?php echo $row['Seminarleistungen'] ?> >
					<button type="submit" class="btn btn-outline-info btn-sm"> Seminarplatz Zusagen  </button>
					</form>
				</td>
				<td>
					<form action="befehlProzesse.php" method="POST">
					<input type="hidden" name="absagen"  value="absage">
					<input type="hidden" name="seminarID" value=<?php echo $row['Seminar_ID'] ?> >
					<input type="hidden" name="semester"  value=<?php echo $row['Semester'] ?> >
					<input type="hidden" name="studentID" value=<?php echo $row['Student_ID'] ?> >
					<button type="submit" class="btn btn-outline-info btn-sm"> Seminarplatz Ablehnen </button>
					</form>
				</td>
				<td> </td>
	<?php				
						}else{
							if($row['Ablehnung'] == 1){
								echo '<td> Seminarplatz Abgelehnt</td>';
								echo '<td> '; 
									if($row['Ablehnung_Datum'] != NULL){
										$date = new DateTime($row['Ablehnung_Datum']);
										echo $date->format('d.m.Y H:i');
									}
								echo '&nbsp; &nbsp; </td>';
								echo '<td> </td>';
							}else{
								echo '<td> Seminarplatz Zugesagt </td>';
								echo '<td> '; 
									$date = new DateTime($row['Ablehnung_Datum']);
									echo $date->format('d.m.Y H:i');
								echo '&nbsp; &nbsp; </td>';
	?>
								<td> <a href="seminarTeilnehmer.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button" class="btn btn-outline-info btn-sm">Seminarteilnehmer Anzeigen</a></td>
	<?php
							}
						}
					
						if($row['Absolviert_Datum'] == NULL || $row['Absolviert_Datum'] == "0000-00-00 00:00:00"){
							echo '<td>  </td> ';
							echo '<td>  </td> ';
						}else{
							if($row['Absolviert'] == 1){
								echo '<td> Seminar Bestanden am </td>';
								echo '<td> '; 
									$date = new DateTime($row['Absolviert_Datum']);
									echo $date->format('d.m.Y H:i');
								echo '</td>';
							}else{
								echo '<td> Nicht Bestanden </td>';
								echo '<td> '; 
									$date = new DateTime($row['Absolviert_Datum']);
									echo $date->format('d.m.Y H:i');
								echo ' </td>';
							}
						}
	?>
			</tr>
	<?php
					}
	?>
		</tbody>
			</table>
		</div>
	<?php
				}
				include 'fusszeile.php';
			}else{
				include 'keineBerechtigung.php';
			}
		}
	?>
    </div>
  </body>
</html>
