<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
		require 'sql3.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
		  if(!isset($_GET['Bewerbungszeitraum_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$bewerbungszeitraumID   = $_GET['Bewerbungszeitraum_ID'];
	
			include 'navBar.php';
			include 'meldung.php';
			include 'sql3.php';
			
			if($rolle == 1){
				include 'keineBerechtigung.php';
			}else{
	?>
			<h3> Zu keinem Seminar zugeteilte Studenten im: <?php echo $bewerbungszeitraum['Name']; ?></h3>
	<?php
				if(empty ($student)){
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Alle Studenten, die sich zu einem Seminar beworben haben, haben einen Seminarplatz erhalten.
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
				}else{
	?>
				<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
					<h5 class="alert-heading">Info:</h5>
						<p>Hier werden Studenten aufgelistet, die zu keinem ihrer Bewerbungen eine Zusage erhalten haben.</p> 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
				</div>
	<?php
					$z = 0; //Anzahl der Studenten, die keine Zusage erhalten haben.
					foreach ($studenten as $row){
						$studentID = $row['Student_ID'];
						$_SESSION['Student_ID'] = $studentID;
						include 'sql3.php';  //Nochmal einbinden, da $lehrstuhlID neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
						
						if($studentID != 1){
							if(empty ($zugeteilt)){
	?>		
			<h4>Student/in:  		<a href="profil2.php?Email=<?php echo $row['Email'] ?>" style="text-decoration:none; color:black;"> 
										<?php echo $row['Vorname'].' '.$row['Name']; ?> </a> </h4>
				Studiengang: 		<i> <?php echo $row['Studiengang']; ?> </i> </br>
				Abschluss: 			<i> <?php echo $row['Abschluss']; ?> </i> </br>
				Fachsemester:   	<i> <?php echo $row['Fachsemester']; ?> </i> </br> 
				Seminarleistungen:  <i> <?php echo $row['Seminarleistungen']; ?> </i> </br>
				E-Mail: 			<i> <?php echo $row['Email']; ?> </i>
	<?php
								if(!empty ($seminareBewerbungen)){
	?>
		<div class="table-responsive">	
			<table class="table table table-striped table-bordered">
			<thead>
			<tr>
				<th scope="col"> 				    			 </th>
				<th scope="col"> Prüfungsnummer     			 </th>
				<th scope="col"> Seminartitel      				 </th>
				<th scope="col"> Lehrstuhl      				 </th>
				<th scope="col"> Maximale </br> Teilnehmeranzahl </th>
				<th scope="col"> Seminarteilnehmer    		     </th>
				<th scope="col"> Priorität          			 </th>
				<th scope="col"> Bewerbung am      				 </th>
			</tr>
			</thead>
			<tbody>	
	<?php
									foreach ($seminarBewerbung as $row2){
										$seminarID = $row2['Seminar_ID'];
										$_SESSION['Seminar_ID'] = $seminarID;
										$semester  = $row2['Semester'];
										$_SESSION['Semester'] = $semester;
										include 'sql3.php';  //Nochmal einbinden, da $lehrstuhlID neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
	?>		
			<tr>
				<td> <a class="btn btn-outline-secondary btn-sm" href="bewerberliste.php?Seminar_ID=<?php echo $row2['Seminar_ID'] ?>&Semester=<?php echo $row2['Semester'] ?>" role="button">Alle Bewerber</a></td>
				<th scope="row"> <?php echo $row2['Seminar_ID']; ?> </th>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row2['Seminar_ID'] ?>&Semester=<?php echo $row2['Semester'] ?>"> 
						<?php echo $row2['Titel']; ?> 		   </a> </td> 
				<td> <a href="profil2.php?Email=<?php echo $row2['Email'] ?>" style="text-decoration:none; color:black;"> 
						<?php echo $row2['Bezeichnung']; ?>    </a> </td>
				<td> 	<?php echo $row2['Teilnehmeranzahl']; ?>       </td> 
				<td> 	<?php echo $seminarteilnehmer[0]; ?> &nbsp;
					 <a class="btn btn-outline-secondary btn-sm" href="seminarTeilnehmer.php?Seminar_ID=<?php echo $row2['Seminar_ID'] ?>&Semester=<?php echo $row2['Semester'] ?>" role="button">Anzeigen</a></td>  
				<td> 	<?php echo $row2['Prioritaet']; ?>   			</td>  
				<td> 	<?php $date = new DateTime($row2['Bewerbung_Datum']);
							echo $date->format('d.m.Y H:i'); ?> </td>
			</tr>
		<?php
									}//Ende foreach $seminare
								
		?>
			</tbody>
			</table>
			</div>
			</br></br>
		<?php
								}//Ende if(!empty ($seminare))
								$z++;
							}//Ende if(empty($zugeteilt))
						}//Ende if($studentID != 1)		
					}//Ende foreach $studenten
				}//Ende if (!empty($student))
				if($z == 0){
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<p>Alle Studenten, die sich zu einem Seminar beworben haben, haben einen Seminarplatz erhalten. &nbsp; &nbsp;</p>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
				}
			}//Rollenkontrolle
			include 'fusszeile.php';
		  }
		}
		?>
    </div>
  </body>
</html>
