<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
		
		  if(!isset($_GET['Seminar_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];  
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if($rolle == 2 || $rolle == 4){
				if((!empty($ZvorAnmeldeBeginn)) || ($rolle == 4 && $testmodus['Testmodus'] == "1")){
					if(empty ($seminar)){
						echo 'Kein Inhalt zu diesem Seminar '.$seminar['Titel'].'. </br>';
					}else{
	?>
		 <h2> Seminar im <?php echo $seminar['Semester'] ?> Bearbeiten: <?php echo $seminar['Titel']; ?> </h2>
		<div class="col-md-10">
			<table class="table table-sm no-border">
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="seminarBearbeiten" value="bearbeiten">
				<input type="hidden" name="seminarID"         value=<?php echo $seminarID ?> >
				<input type="hidden" name="semester"          value=<?php echo $semester ?> >
			<tr>
				<th> Prüfungsnummer: </br></th>      
				<td> <?php echo $seminarID ?> </td>
			</tr>
			<tr>
				<td> <b>Titel:</b> </br> 
					 <p class="text-muted">Titel des Seminars.</p> </td>
				<td><input type="text" name="titel" class="form-control" value="<?php echo $seminar['Titel'] ?>" required> </td>
			</tr>
			<tr>
				<td> <b>Beschreibung:</b> </br>
					 <p class="text-muted">Kurzbeschreibung des Seminars.</p> </td>
				<td> <textarea type="text" name="beschreibung"  class="form-control"  required><?php echo $seminar['Beschreibung'] ?></textarea>  </td>
			</tr>
			<tr>
				<th> Maximale Teilnehmeranzahl:</th>
				<td> <input type="number" min="1" name="teilnehmeranzahl" class="form-control" value="<?php echo $seminar['Teilnehmeranzahl'] ?>" required> </td>
			</tr>
			<tr>
				<td> <b>Abschluss:</b> </br>
					 <p class="text-muted">Der benötigte angestrebte Abschluss zum Absolvieren dieses Seminars.</p> </td>      
				<td> <select name="abschluss" class="form-control" value="<?php echo $seminar['Abschluss'] ?>" required>
						<option selected="selected" hidden> <?php echo $seminar['Abschluss']; ?> </option>
						<option> Bachelor </option>
						<option> Master </option>
					 </select>
				</td>
			</tr>
			<tr>
				<th> <button type="submit" class="btn btn-info"> Bearbeitung abschließen</button>
				</form>
					 <a href="seminar.php?Seminar_ID=<?php echo $seminarID ?>&Semester=<?php echo $semester ?>" class="btn btn-info"> Abbrechen </a></th>
			</tr>
			</table>
			</div>
	<?php
					}
				}else{
					include 'keineBerechtigung.php';
				}
			}else{
				include 'keineBerechtigung.php';
			}
			include 'fusszeile.php';
		  }
		}
	?>
    </div>
  </body>
</html>
