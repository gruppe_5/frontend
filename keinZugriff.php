<?php
	$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
				Kein Zugriff auf diese Seite! Bitte zuerst anmelden!
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>';
	$_SESSION['Error'] = $errorMsg;
	header("Location:index.php");
	exit;
?>