<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
      <h2> Mein Profil | Passwortänderung</h2>
		<div class="container-fluid">
     
        <div class="col">
              <h3> 
	<?php 
				if(!empty ($admin)){
					echo $admin['Name'];
				}else{
					if(!empty ($student)){
						echo $student['Vorname'].'&nbsp;';
						echo $student['Name'];
					}else
					if(!empty ($lehrstuhl)){
						echo $lehrstuhl['Bezeichnung'];
					}else
					if(!empty ($studiendekan)){
						echo $studiendekan['Name'];
					}
				}
	?> 
			   </h3>
            
            <div class="row">
                <div class="col-md-4">
                    <img alt="User Pic" src="userBild.jpg" class="img-thumbnail img-responsive">    
                </div>
                
                <div class=" col-md-8">
           		 <div class="card bg-transparent" style="border-color: #eee;"> 
					
                  <form action="befehlProzesse.php" method="POST">
					<input type="hidden" name="passwortAenderung" value="aendern">
				 <table>
					<tr> 
						<td> Altes Passwort:</td>
						<td> <input type="password" minlength="8" name="altPasswort" title="Altes Passwort hier eingeben" placeholder="Altes Passwort" required> </td>
					</tr>
					<tr> <td> <i><b> Das Passwort muss </br> mindestens 8 </br> Zeichen enthalten. </b></i></td> </tr>
					<tr> 
						<td> Neues Passwort:</td>
						<td> <input type="password" minlength="8" name="neuPasswort" title="Neues Passwort hier eingeben" placeholder="Neues Passwort" required> </td>
					</tr>
					<tr> 
						<td> Neues Passwort </br> Wiederholen:</td>
						<td> <input type="password" minlength="8" name="neuPasswort2" title="Neues Passwort Wiederholen" placeholder="Neues Passwort Wiederholen" required> </td>
					</tr>
					<tr>
						<td> <button type="submit" class="btn btn-info"> Passwort Ändern </button> </td>
				  </form> 
						<td> <button type="submit" class="btn btn-info"> <a href="profil.php" style="text-decoration:none; color:white;"> Abbrechen </a> </button></td>
					</tr>
				 </table>
				  
                </div>
              </div>
          
        <br>
        <br>
		<br>

		<table class="table no-border">
			<tr> 
				<td><b> 
	<?php
		if($rolle != 4){
			echo 'Bei Änderungen der Profildaten bitte an den Admin wenden.';
		}else{
			//Diese Anzahl an Leerzeichen müssen hier als Alternative ausgegeben werden, ansonsten wird das Layout für den Admin verschoben.
			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
	?>		
				</b></td>
			</tr>
		</table>
	<?php
			include 'fusszeile.php';
		}
	?>
    </div>
  </body>
</html>