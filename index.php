<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

  <body>
    <div class="container">

	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
		
		if(!isset($_SESSION['Email'])){
			include 'meldung.php';
			include 'sql.php';
			
			if ($adminUser[0] == "0"){
				include 'adminRegistrierung.php'; //nur bei der ersten Inbetriebnahme des Systems möglich!
			}else{
	?>

	<div class="text-center">
		<img alt="icon" src="bilder/icon.png" class="img-responsive" style="width:17%";>
	</div>

	  <form action="befehlProzesse.php" method="POST" class="form-signin">
	  <input type="hidden"     name="login" value="login">
        <h2 class="form-signin-heading"> Anmelden </h2>
        <label for="email"     class="sr-only"> E-Mail Adresse </label>
        <input type="email"    name="email"    class="form-control" placeholder="E-Mail Adresse" required autofocus>
		<label for="passwort"  class="sr-only"> Passwort </label>
        <input type="password" name="passwort" class="form-control" placeholder="Passwort" required>
		<a href="passwortZuruecksetzen.php"> Passwort Vergessen? Hier klicken. </a> </br>
	<?php
				if(!empty ($anmeldeZeitraum)){
	?>
		Student und noch kein Account? </br>
		 <a href="registrierung.php"> Hier registrieren. </a> </br>
    <?php
				}else{
	?>
		Die Anmeldephase für Seminare ist vorüber oder hat noch nicht angefangen. </br>
		Es haben nur bereits registrierte Nutzer Zugang zum System.
	<?php
				}
	?>
	   <button class="btn btn-lg btn-primary btn-block" type="submit"> Anmelden </button>
	   </form>
	<?php
			}
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];

			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
	<main role="main">

      <div class="container" style="margin-top:50px">
        <div class="row">
          <div class="col-md-4">
            <h2> Bewerbungszeiträume </h2>
            <p> Bewerbungszeiträume für Seminare ansehen. </p>
            <p><a class="btn btn-info" href="bewerbungszeitraeume.php" role="button"> Bewerbungszeiträume Ansehen. &raquo;</a></p>
          </div>
		  <div class="col-md-4">
            <h2> Seminare </h2>
            <p> Alle Seminare in den jeweiligen Semestern.</p>
            <p><a class="btn btn-info" href="alleSeminare.php" role="button"> Alle Seminare &raquo;</a></p>
          </div>
	<?php
			if($rolle == 1 || $rolle == 4){
	?>
          <div class="col-md-4">
            <h2> Meine Seminarbewerbungen </h2>
            <p> Hier befinden sich alle meine Bewerbungen zu Seminaren. Jeder Bewerbung kann ich eine Priorität vergeben.</p>
            <p><a class="btn btn-info" href="bewerbungen.php" role="button"> Ansehen &raquo;</a></p>
          </div>
		  <div class="col-md-4">
            <h2> Meine Seminare </h2>
            <p> Hier werden alle meine Seminare aufgelistet, zu welchen ich zugeteilt wurde. Anschließend kann ich dem Seminarplatz zu- bzw. absagen.</p>
            <p><a class="btn btn-info" href="seminarZuteilung.php" role="button"> Ansehen &raquo;</a></p>
          </div>
	<?php
			}
			if($rolle == 2 || $rolle == 4){
	?>
          <div class="col-md-4">
            <h2>Seminar Anlegen</h2>
            <p> Als Lehrstuhl können Sie neue Seminare in dem neuen Bewerbungszeitraum anlegen. </p>
            <p><a class="btn btn-info" href="seminarAnlegen.php" role="button"> Seminar Anlegen &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Alle Angelegten Seminare</h2>
            <p> Hier können Sie alle ihre angelegten Seminare einsehen </p>
            <p><a class="btn btn-info" href="lehrstuhlSeminare.php" role="button"> Angelegte Seminare Anzeigen &raquo;</a></p>
          </div>
	<?php
			}
			if($rolle == 3 || $rolle == 4){
	?>
          <div class="col-md-4">
            <h2> Bewerbungszeitraum Festlegen </h2>
            <p> Als Studiendekan können Sie die Bewerbungszeiträume für Seminare festlegen. Nach Festlegen des Zeitraumes können die Lehrstühle die jeweiligen Seminare anlegen. </p>
            <p><a class="btn btn-info" href="bewerbungszeitraumFestlegen.php" role="button"> Bewerbungszeitraum Festlegen. &raquo;</a></p>
          </div>
	<?php
			}
			if($rolle == 4){
	?>
          <div class="col-md-4">
            <h2> Lehrstuhl Anlegen </h2>
            <p>Als Admin können Sie Lehrstühle an der wirtschaftswissenschaftlichen Fakultät anlegen.</p>
            <p><a class="btn btn-info" href="lehrstuhlAnlegen.php" role="button"> Lehrstuhl Anlegen &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2> Studiendekan Anlegen </h2>
            <p> Als Admin können Sie den Studiendekan der wirtschaftswissenschaftlichen Fakultät anlegen. </p>
            <p><a class="btn btn-info" href="studiendekanAnlegen.php" role="button"> Studiendekan Anlegen &raquo;</a></p>
          </div>
		  <div class="col-md-4">
            <h2> Gesperrte Nutzer </h2>
            <p> Als Admin können Sie Nutzer, die sich durch merhmalige Falscheingabe des Passworts aus dem System gesperrt haben, einsehen und diese wieder entsperren. </p>
            <p><a class="btn btn-info" href="gesperrteUser.php" role="button"> Gesperrte Nutzer Anzeigen &raquo;</a></p>
          </div>
		  <div class="col-md-4">
            <h2> Alle Nutzer </h2>
            <p> Als Admin können Sie alle Nutzer einsehen, die Daten der Nutzer ändern und gegebenenfalls diese löschen. </p>
            <p><a class="btn btn-info" href="alleUser.php" role="button"> Alle Nutzer Anzeigen &raquo;</a></p>
          </div>
		  <div class="col-md-4">
            <h2> Nutzer-Historie </h2>
            <p> Als Admin können Sie alle Nutzer mit ihrer Historie einsehen. </p>
            <p><a class="btn btn-info" href="userHistorie.php" role="button"> Nutzer-Historie Anzeigen &raquo;</a></p>
          </div>
	<?php
			}
	?>
        </div>
      </div>
    </main>
	<?php
		}
			include 'fusszeile.php';
	?>
    </div>
  </body>
</html>
