<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
		  if (!isset ($_GET['Email'])){
			include 'keineBerechtigung.php';
		  }else{
			$email2        = $_GET['Email'];
			
	?>
			<h2> Nutzer des Systems</h2>
			
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Dies ist ein Nutzer des Systems. </br>
						Sie können als Admin die Daten dieses Nutzers ändern und/oder dieses gegebenenfalls löschen.</br>
						Falls der Nutzer nicht aktiviert sein sollte, gibt es hier nochmal extra die Möglichkeit den Nutzer zu löschen.</p>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			</div>
			
			<table class="table table-hover">
			<h3>
	<?php 
				if(!empty ($admin)){
					echo $admin['Name'];
				}else{
					if(!empty ($student)){
						echo $student['Vorname'].'&nbsp;';
						echo $student['Name'];
					}else
					if(!empty ($lehrstuhl)){
						echo $lehrstuhl['Bezeichnung'];
					}else
					if(!empty ($studiendekan)){
						echo $studiendekan['Name'];
					}
				}
	?> 
			   </h3>
        </div>		
            <div class="row">
                <div class="col-md-4">
				<form action="befehlProzesse.php?User_Email=<?php echo $email2; ?>" method="POST" >
					<input type="hidden" name="userBearbeiten" value="bearbeiten">
					<input type="hidden" name="userEmail" value="<?php echo $email2; ?>">
	<?php
				if(!empty ($admin)){
					$aktiv     = $admin['Aktiv'];
					$userRolle = $admin['Rolle'];
	?>
				</div>
				<div class=" col-md-4">
            		<div class="card bg-transparent" style="border-color: #eee;"> 
                  <table class="table no-border">
                      <tr>
                        <th> Admin ID: &nbsp; </th> 
                        <td> <?php echo $admin['Admin_ID']; ?> </td>
                      </tr>
                      <tr>
                        <th> Name: &nbsp; </th>
						<td>  <input type="text" style="width: 230px" name="adminName" class="form-control" value="<?php echo $admin['Name']; ?>" required> </td>
					  </tr>
                      <tr>
                        <th> Email: &nbsp; </th>
                        <td> <?php echo $admin['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
				  </div>
	<?php
				}else{
					if(!empty ($student)){
						$aktiv     = $student['Aktiv'];
						$userRolle = $student['Rolle'];
	?>
                                 <hr>
                  </div>
                  <div class="col-md-4"> 
                    <div class="card bg-transparent" style="border-color: #eee;">
				  <table class="table no-border">
                      <tr>
                        <th> Matrikelnummer: &nbsp; </th>
                        <td> <input style="width: 230px" type="number" name="studentID" min="10000" max="100000" maxlength="5" class="form-control" value="<?php echo $student['Student_ID']; ?>" required> </td>
                      <tr>
                        <th> Name: &nbsp; </th>
                        <td> <input style="width: 230px" type="text" name="studentName" class="form-control" value="<?php echo $student['Name']; ?>" required> </td>
					  </tr>
					  <tr>
                        <th> Vorname: &nbsp; </th>
                        <td> <input style="width: 230px" type="text" name="studentVorname" class="form-control" value="<?php echo $student['Vorname']; ?>" required> </td>
					  </tr>
                      <tr>
                        <th> Fachsemester: &nbsp; </th>
                        <td> <select  style="width: 230px" name="fachsemester" class="form-control" value="<?php echo $student['Fachsemester']; ?>" required>
								<option selected="selected" hidden> <?php echo $student['Fachsemester']; ?>  </option>
								<option> 1 </option>
								<option> 2 </option>
								<option> 3 </option>
								<option> 4 </option>
								<option> 5 </option>
								<option> 6 </option>
								<option> 7 </option>
								<option> 8 </option>
								<option> 9 </option>
								<option> 10 </option>
								<option> 11 </option>
								<option> 12 </option>
								<option> 13 </option>
								<option> 14 </option>
								<option> 15 </option>
							</select> </td>
					  </tr>
					  <tr>
                        <th> Studiengang: &nbsp; </th>
                        <td> <select style="width: 230px" name="studiengang" class="form-control" value="<?php echo $student['Studiengang']; ?>" required>
								<option selected="selected" hidden> <?php echo $student['Studiengang']; ?>  </option>
								<option> Business Administration and Economics (BWL/VWL) </option>
								<option> Wirtschaftsinformatik </option>
								<option> Business Administration (BWL) </option>
								<option> International Economics and Business </option>
								<option> Development Studies </option>
								<option> European Studies </option>
								<option> Kulturwirtschaft </option>
								<option> Staatswissenschaften </option>
								<option> Medien und Kommunikation </option>
								<option> Informatik </option>
							</select> </td>
                      </tr>
					  <tr>
                        <th> Abschluss: &nbsp; </th>
                        <td> <select  style="width: 230px" name="abschluss" class="form-control" value="<?php echo $student['Abschluss']; ?>" required>
								<option selected="selected" hidden> <?php echo $student['Abschluss']; ?> </option>
								<option> Bachelor </option>
								<option> Master </option>
							</select> </td>
                      </tr>
					  <tr>
                        <th> Email: &nbsp; </th>
                        <td> <?php echo $student['Email']; ?>  </td>
                      </tr>
					  <tr>
                        <th> Seminarleistungen: &nbsp; </th>
                        <td> <select  style="width: 230px" name="seminarleistungen" class="form-control" value="<?php echo $student['Seminarleistungen']; ?>" required>
								<option selected="selected" hidden> <?php echo $student['Seminarleistungen']; ?>  </option>
								<option> 0 </option>
								<option> 1 </option>
								<option> 2 </option>
								<option> 3 </option>
								<option> 4 </option>
								<option> 5 </option>
								<option> 6 </option>
								<option> 7 </option>
								<option> 8 </option>
								<option> 9 </option>
								<option> 10 </option>
							</select> </td>
					  </tr>
                  </table>
   
				  </div>
				</div>

	<?php
					}else
					if(!empty ($lehrstuhl)){
						$aktiv 	   = $lehrstuhl['Aktiv'];
						$userRolle = $lehrstuhl['Rolle'];
	?>
                </div>
                   <div class="col-md-4"> 
                    <div class="card bg-transparent" style="border-color: #eee;">
				  <table class="table no-border">
                      <tr>
                        <th> Lehrstuhl ID: &nbsp; </th>
                        <td> <?php echo $lehrstuhl['Lehrstuhl_ID']; ?> </td>
					  </tr>
                      <tr>
                        <th> Bezeichnung: &nbsp; </th>
						<td><input type="text" style="width: 230px" name="lehrstuhlBezeichnung" class="form-control" value="<?php echo $lehrstuhl['Bezeichnung']; ?>" required> </td>
					  </tr>
                      <tr>
                        <th> Email: &nbsp; </th>
                        <td> <?php echo $lehrstuhl['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
				</div>
	<?php
					}else
					if(!empty ($studiendekan)){
						$aktiv 	   = $studiendekan['Aktiv'];
						$userRolle = $studiendekan['Rolle'];
	?>
				</div>
                  <div class="col-md-4">
                    <div class="card bg-transparent" style="border-color: #eee;"> 				  
                    <table class="table no-border">
                      <tr>
                        <th> Studiendekan ID: &nbsp; </th>
                        <td> <?php echo $studiendekan['Studiendekan_ID']; ?> </td>
					  </tr>
                      <tr>
                        <th> Name: &nbsp; </th>
						<td><input type="text" style="width: 230px" name="studiendekanName" class="form-control" value="<?php echo $studiendekan['Name']; ?> " required> </td>
					 </tr>
                      <tr>
                        <th> Email: &nbsp; </th>
                        <td> <?php echo $studiendekan['Email']; ?> </td>
                      </tr>
                  </table>
				  </div>
				</div>
	<?php
					}
				}
	?>
			<div class="w-100"></div>
			<div class="col-md-4" style="padding-left: 90px">
			<table align="">
				<tr>
					<td> <button type="submit" class="btn btn-info btn-sm"> Bearbeitung abschließen </button>
			</form>
						<a href="alleUser.php" class="btn btn-info btn-sm"> Abbrechen </a>
	<?php
				if($aktiv == "0"){
	?>
						<form action="befehlProzesse.php" method="POST" >
							<input type="hidden" name="userLoeschen" value="loeschen">
							<input type="hidden" name="userEmail" value=<?php echo $email2; ?> >
							<input type="hidden" name="userRolle" value=<?php echo $userRolle; ?> >
							<br>
							<button type="submit" class="btn btn-danger btn-sm"><i class="material-icons"  style="font-size:15px">delete</i> Löschen </button>
						</form>
	<?php
				}
	?>
					</td> 
				</tr>
			</table>
			</table>
		</div>
		</div>
	<?php
			include 'fusszeile.php';
				}
		}
	?>
    </div>
  </body>
</html>
