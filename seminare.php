<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
		  
		  if(!isset($_GET['Bewerbungszeitraum_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$bewerbungszeitraumID   = $_GET['Bewerbungszeitraum_ID'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
		<h2> Seminare im <?php echo $bewerbungszeitraum['Name']; ?> </h2>
	<?php		
			if(empty ($seminare)){
	?>
			<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Keine Seminare vorhanden.
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
			}else{
	?>
			
		
		<div class="table-responsive">	
		<table class="table no-border">
		<thead class="thead">
			<tr>
				<th scope="col"> Prüfungsnummer 	</th>				
				<th scope="col"> Seminartitel 		</th>				
				<th scope="col"> Lehrstuhl 			</th>	
				<th scope="col"> Abschluss 			</th>	
				<th scope="col"> 					</th>	
				<th scope="col"> 					</th>	
				<th scope="col"> 					</th>	
				<th colspan="2"> </th>
			</tr>
		</thead>
		<tbody>			
	<?php		
					foreach ($seminareDaten as $row){ 
	?>
			<tr>
				<th scope="row"> <?php echo $row['Seminar_ID']; ?></th>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" data-toggle="tooltip" title="Hier weiter zur Bewerbung"><font color="black"> 
						<?php echo $row['Titel']; ?> </font>
					</a>
				</td>   
				<td> <a href="profil2.php?Email=<?php echo $row['Email'] ?>" data-toggle="tooltip" title="Lehrstuhl anzeigen"><font color="black"> 
						<?php echo $row['Bezeichnung']; ?> </font></a> </td>
				<td>    <?php echo $row['Abschluss']; ?>   </td>
				<td> <a class="btn btn-outline-info btn-sm" href="bewerberliste.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button">Alle Bewerber Anzeigen</a></td>
	<?php
						if(!empty($zZuteilungNachAnmeldeEnde) || $bewerbungszeitraeume['Bewerbungszeitraum_ID'] != $bewerbungszeitraumID){
	?>
				<td> <a class="btn btn-outline-info btn-sm" href="seminarTeilnehmer.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button">Seminarteilnehmer Anzeigen</a></td> 
	<?php
						}
						if(!empty ($ZnachAblehnungEnde) || $bewerbungszeitraeume['Bewerbungszeitraum_ID'] != $bewerbungszeitraumID){
	?>
				<td><a class="btn btn-outline-info btn-sm" href="seminarThemen.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" role="button"> Teilnehmer mit zugehörigen Seminarthemen</a> </td>
	<?php
						}
	?>		
			</tr>
	<?php
					}
	?>
		</tbody>
			</table>
		</div>
	<?php
			
	?>
			</br>
	<?php
				if(!empty($ZvorAnmeldeBeginn)){
					if($rolle == 2 || $rolle == 4){
						echo '<p><a class="btn btn-info custom" href="seminarAnlegen.php" role="button"> Seminar Anlegen.</a></p>';
					}
				}
				if(!empty($zZuteilungNachAnmeldeEnde) || $bewerbungszeitraeume['Bewerbungszeitraum_ID'] != $bewerbungszeitraumID){
					if($rolle != 1){
	?>
				<p><a class="btn btn-info" href="auslastungLehrstuehle.php?Bewerbungszeitraum_ID=<?php echo $row['Bewerbungszeitraum_ID'] ?>" role="button"> Auslastung aller Lehrstühle anzeigen </a></p>
				<p><a class="btn btn-info" href="uebrigGeblieben.php?Bewerbungszeitraum_ID=<?php echo $row['Bewerbungszeitraum_ID'] ?>" role="button"> Zu keinem Seminar zugeteilte Bewerber anzeigen </a></p>
	<?php	
					}
				}
				include 'fusszeile.php';
			}
		  }
		}
	?>
    </div>
  </body>
</html>
