<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if($rolle != 4){
				include 'keineBerechtigung.php';
			}else{
	?>
			<h2> Gesperrte Nutzer</h2>
	<?php
				
					if(empty ($gesperrt)){
						echo 'Keine gesperrten Nutzer vorhanden.';
					}else{
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Diese Nutzer haben sich durch mehrmalige Falscheingabe des Passworts aus dem System gesperrt. Damit diese wieder Zugang zum System haben, müssen sie entperrt werden.</p>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			</div>
			
		<div class="table-responsive">
			<table class="table table-hover">
			<thead>
			<tr>			
				<th scope="col"> Nutzerrolle 				</th>	
				<th scope="col"> ID / </br> Matrikelnummer  </th>	
				<th scope="col"> Name		 				</th>				
				<th scope="col"> E-Mail     			 	</th>		
				<th scope="col"> Letzter Login 				</th>
			</tr>
			</thead>			
	<?php		
						$i = 0; //Zähle geperrte Nutzer. Falls keine gesperrten Nutzer vorhanden, wird dies so angegeben.
						foreach ($gesperrteNutzer as $row){ 
							$userEmail = $row['Email'];
							$_SESSION['User_Email'] = $userEmail;
							include 'sql.php'; //Nochmal einbinden, da $lehrstuhlID neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
						
							if($row['User_Historie_ID'] >= $letzterLogin[0]){
	?>
			<tbody>
			<tr>
				<td>  <?php if ($row['Rolle'] == 1){ echo 'Student'; } 
							if ($row['Rolle'] == 2){ echo 'Lehrstuhl'; }
							if ($row['Rolle'] == 3){ echo 'Studiendekan'; } ?></td>
				<td>  <?php if ($row['Rolle'] == 1){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $student['Student_ID']; } 
							if ($row['Rolle'] == 2){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $lehrstuhl['Lehrstuhl_ID']; }
							if ($row['Rolle'] == 3){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $studiendekan['Studiendekan_ID']; } ?></td>
				<td>  <?php if ($row['Rolle'] == 1){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $student['Vorname'].'&nbsp;'.$student['Name']; } 
							if ($row['Rolle'] == 2){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $lehrstuhl['Bezeichnung']; }
							if ($row['Rolle'] == 3){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $studiendekan['Name']; } ?></td>
				<td> <?php echo $row['Email']; ?> </td>   
				<td> <?php $date = new DateTime($row['Letzter_Login']);
							echo $date->format('d.m.Y H:i'); ?></td>  
				<td> <form action="befehlProzesse.php" method="POST" class="form-signin form-margin">
						<input type="hidden" name="userEntsperren" value="entsperren">
						<input type="hidden" name="userEmail" value=<?php echo $row['Email']; ?> >
						<button type="submit" class="btn btn-outline-danger btn-sm"> Entsperren </button>
					</form>	
				</td>   
			</tr>
			</tbody>
	<?php
								$i++;
							}
						}
						if($i == 0){
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<p> Es haben sich keine Nutzer aus dem System gesperrt. &nbsp; &nbsp; &nbsp;</p>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			</div>
	<?php
						}
	?>
			</table>
		</div>
	<?php
					}
			include 'fusszeile.php';
				}
		}
	?>
    </div>
  </body>
</html>
