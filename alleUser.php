<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if($rolle != 4){
				include 'keineBerechtigung.php';
			}else{
	?>
			<h2> Alle Nutzer des Systems</h2>
			
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				<h5 class="alert-heading">Info:</h5>
					<p>Hier werden alle Nutzer des Seminarvergabesystems angezeigt. </br>
						Sie können als Admin die Daten der jeweiligen Nutzer ändern und/oder diese gegebenenfalls löschen.</p>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
			</div>
			
		<div class="table-responsive">
			<table class="table table-hover">
			<thead>
			<tr>			
				<th scope="col"> Account </br> Aktiv		</th>	
				<th scope="col"> Nutzerrolle 				</th>	
				<th scope="col"> ID / </br> Matrikelnummer  </th>	
				<th scope="col"> Name		 				</th>				
				<th scope="col"> E-Mail     	 			</th>		
				<th scope="col"> Letzter Login 				</th>
			</tr>
			</thead>		
			<tbody>	
	<?php		
						$rolle;
						foreach ($alleNutzer as $row){ 
							$userEmail              = $row['Email'];
							$_SESSION['User_Email'] = $userEmail;
							$userRolle              = $row['Rolle'];
							$_SESSION['User_Rolle'] = $userRolle;
							include 'sql.php'; //Nochmal einbinden, da $userEmail neu in der Session übergeben wird. Ansonsten wird der restliche Teil der Seite erst bei Neuladen angezeigt.
							
							if($row['User_Historie_ID'] >= $letzterLogin[0]){
								if($rolle != $row['Rolle']){
									if($row['Rolle'] == "1"){
										echo '<tr><td><b><i>Studenten</i></b></td></tr>';
									}
									if($row['Rolle'] == "2"){
										echo '<tr><td><b><i>Lehrstühle</i></b></td></tr>';
									}
									if($row['Rolle'] == "3"){
										echo '<tr><td><b><i>Studiendekane</i></b></td></tr>';
									}
									if($row['Rolle'] == "4"){
										echo '<tr><td><b><i>Admin</i></b></td></tr>';
									}
								}
								$rolle = $row['Rolle'];
	?>
			
			<tr>
				<td>  <?php if($row['Aktiv'] == "0"){
								echo '<i class="material-icons"  style="color:red">clear</i>';
							}else{
								echo '<i class="material-icons"  style="color:green">check</i>'; } ?> </td>
				<td>  <?php if ($row['Rolle'] == 1){ echo 'Student'; } 
							if ($row['Rolle'] == 2){ echo 'Lehrstuhl'; }
							if ($row['Rolle'] == 3){ echo 'Studiendekan'; } 
							if ($row['Rolle'] == 4){ echo 'Admin'; } ?></td>
				<td>  <?php if ($row['Rolle'] == 1){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $student['Student_ID']; } 
							if ($row['Rolle'] == 2){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $lehrstuhl['Lehrstuhl_ID']; }
							if ($row['Rolle'] == 3){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $studiendekan['Studiendekan_ID']; }
							if ($row['Rolle'] == 4){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $studiendekan['Studiendekan_ID']; } ?></td>
				<td>  <?php if ($row['Rolle'] == 1){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $student['Vorname'].'&nbsp;'.$student['Name']; } 
							if ($row['Rolle'] == 2){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $lehrstuhl['Bezeichnung']; }
							if ($row['Rolle'] == 3){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $studiendekan['Name']; } 
							if ($row['Rolle'] == 4){ 
								$_SESSION['Email2'] = $row['Email']  ;
								include 'sql.php';
								echo $studiendekan['Name']; } ?></td>
				<td> <?php echo $row['Email']; ?> </td>   
				<td> <?php $date = new DateTime($row['Letzter_Login']);
							echo $date->format('d.m.Y H:i'); ?></td>  
				<td> <a href="userBearbeiten.php?Email=<?php echo $row['Email'] ?>" class="btn btn-outline-dark btn-sm"><i class="material-icons"  style="font-size:15px">create</i></a></td>   
	<?php
								if(($row['Aktiv'] == "0") || (empty($alteDaten) && $row['Rolle'] == 1)){
	?>
				<td> <form action="befehlProzesse.php" method="POST" >
						<input type="hidden" name="userLoeschen" value="loeschen">
						<input type="hidden" name="userEmail" value=<?php echo $row['Email']; ?> >
						<input type="hidden" name="userRolle" value=<?php echo $row['Rolle']; ?> >
						<button type="submit" class="btn btn-outline-danger btn-sm"> 
							<i class="material-icons"  style="font-size:15px">delete</i>
						</button>
					</form>	</td>
	<?php
								}
	?>
			</tr>
			
	<?php
							}
						}
	?>
			</tbody>
		</table>
		</div>
	<?php
			include 'fusszeile.php';
				}
		}
	?>
    </div>
  </body>
</html>
