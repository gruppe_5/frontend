<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
 </head>

<?php
	require_once 'session.php';
	require 'dbVerbindung.php';

	/*function redirect() {
		header('Location:registrierung.php');
		exit();
	} */

	if (!isset($_GET['email']) || !isset($_GET['token'])) {
		//redirect();
		include 'keineBerechtigung.php';
	}else{
		$email = htmlentities($_GET['email'],ENT_QUOTES);
		$token = htmlentities($_GET['token'],ENT_QUOTES);

		$stmt     = $pdo->prepare("SELECT Email 
									FROM User 
									WHERE Email = '$email' 
										AND Token = '$token' 
										AND Aktiv=0");
	 	$stmt-> fetch();

		if (!empty($stmt)) {
			$stmt2 = $pdo->prepare("UPDATE User 
									SET Aktiv= 1, Token='' 
									WHERE Email='$email'");
			$stmt2->execute();
			
			$successMsg = '<div class="alert alert-success alert-dismissible fade show top30" role="alert">
							Dein Account ist nun verifiziert! Du kannst dich nun einloggen! 
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
			$_SESSION['Error']  = $successMsg;
			header('Location:index.php');
			exit;
			//echo 'Dein Account ist nun verifiziert! Du kannst dich nun einloggen! <a href="index.php"  >Bitte hier klicken</a>';
		}else{
			//redirect();
			$errorMsg = '<div class="alert alert-danger alert-dismissible fade show top50" role="alert">
						Verifizierung deines Accounts fehlgeschlagen!
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						</div>';
			$_SESSION['Error'] = $errorMsg;
			header('Location:index.php');
			exit;
		}
	}
	
	include 'fusszeile.php';
?>
</html>