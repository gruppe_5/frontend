<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>
	
	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'meldung.php';
			include 'sql.php';
			
			if(!empty ($anmeldeZeitraum)){
	?>
		 <h1>Seminarvergabesystem der wirtschaftswissenschaftlichen Fakultät der Universität Passau</h1>
		 <h2> Student Registrierung </h2>
		 
			   <div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				  <h5 class="alert-heading">Wichtig!</h5>
				  <p>Bei der Registrierung muss die universitäre E-Mail Adresse verwendet werden. </br>
			   			Anschließend wird eine E-Mail zur Verifizierung gesendet. </br>
			   			Erst nach Aktivierung des Accounts ist der Zugang zum System freigeschaltet. </p>
			   	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
				</div>
		 
			<table>
				<form action="befehlProzesse.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="studentRegistrierung" value="registrierung">
			<tr>
				<th> Matrikelnummer*: </th>      
				<td> <input type="number" name="studentID" min="10000" max="100000" maxlength="5" class="form-control" placeholder="Matrikelnummer" required> </td>
			</tr>
			<tr>
				<th> Name*: </th>      
				<td> <input type="text" name="studentName" class="form-control" placeholder="Name" required> </td>
			</tr>
			<tr>
				<th> Vorname*: </th>      
				<td> <input type="text" name="studentVorname" class="form-control" placeholder="Vorname" required> </td>
			</tr>
			<tr>
				<th> Fachsemester*: </th>      
				<td> <select name="fachsemester" class="form-control" placeholder="Fachsemester" required>
						<option disabled selected hidden> Fachsemester </option>
						<option> 1 </option>
						<option> 2 </option>
						<option> 3 </option>
						<option> 4 </option>
						<option> 5 </option>
						<option> 6 </option>
						<option> 7 </option>
						<option> 8 </option>
						<option> 9 </option>
						<option> 10 </option>
						<option> 11 </option>
						<option> 12 </option>
						<option> 13 </option>
						<option> 14 </option>
						<option> 15 </option>
					</select> </td>
			</tr>
			<tr>
				<th> Studiengang*: </th>      
				<td>  <select name="studiengang" class="form-control" placeholder="Studiengang" required>
						<option disabled selected hidden> Studiengang </option>
						<option> Business Administration and Economics (BWL/VWL) </option>
						<option> Wirtschaftsinformatik </option>
						<option> Business Administration (BWL) </option>
						<option> International Economics and Business </option>
						<option> Development Studies </option>
						<option> European Studies </option>
						<option> Kulturwirtschaft </option>
						<option> Staatswissenschaften </option>
						<option> Medien und Kommunikation </option>
						<option> Informatik </option>
					 </select> 
				 </td>
			</tr>
			<tr>
				<th> Abschluss*: </th>      
				<td> <select name="abschluss" class="form-control" placeholder="Abschluss" required>
						<option disabled selected hidden> Abschluss </option>
						<option> Bachelor </option>
						<option> Master </option>
					 </select>
				</td>
			</tr>
			<tr>
				<th> E-Mail*: </th>
				<td> <input type="text" style="width:25%" maxlength="10" name="email" class="form-control" placeholder="E-Mail" required></td>
				<td class="form-control" style="margin-left:-305px; width:170px"> @gw.uni-passau.de </td>
			</tr>
			<tr>
				<th> HISQIS-Auszug: </th>      
				<td> <input type="file" name="hisqisAuszug" class="form-control" placeholder="HISQIS-Auszug"> </td>
			</tr>
			<tr>
				<td><p class="text-muted"> Anzahl der bereits abgelegten Seminarleistungen: </p></td>
			</tr>
			<tr>
				<th> Seminarleistungen*: </th>
				<td> <select name="seminarleistungen" class="form-control" placeholder="Seminarleistungen" required>
						<option disabled selected hidden> Seminarleistungen </option>
						<option> 0 </option>
						<option> 1 </option>
						<option> 2 </option>
						<option> 3 </option>
						<option> 4 </option>
						<option> 5 </option>
						<option> 6 </option>
						<option> 7 </option>
						<option> 8 </option>
						<option> 9 </option>
						<option> 10 </option>
					</select> </td>
			</tr>
			<tr>
				<td> <p class="text-muted"> Das Passwort muss mindestens 8 Zeichen enthalten. </p></td>
			</tr>
			<tr>
				<th> Passwort*: </th>       
				<td> <input type="password" minlength="8" name="passwort" class="form-control" placeholder="Passwort" required> </td>
			</tr>
			<tr>
				<th> Passwort Wiederholen*: </th>
				<td> <input type="password" minlength="8" name="passwort2" class="form-control" placeholder="Passwort Wiederholen" required> </td>
			</tr>
			<tr>
				<th> </br> (Alle Felder mit * müssen ausgefüllt werden.) </th>
			</tr>
			<tr>
				<th> <button type="submit" class="btn btn-primary">Registrieren</button>
				</form> &nbsp;
					 <a href="index.php" class="btn btn-primary"> Abbrechen </a> </th>
			</tr>
			</table>
	<?php
				}else{
					include 'keinZugriff.php';
				}
			}else{
				include 'keineBerechtigung.php';
			}
			include 'fusszeile.php';
	?>
    </div>
  </body>
</html>
