<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50" >
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
		
		  if(!isset($_GET['Seminar_ID'])){
			  include 'keineBerechtigung.php';
		  }else{
			$seminarID     = $_GET['Seminar_ID'];
			$semester      = $_GET['Semester'];
			
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Seminar: <?php echo $seminar['Titel']; ?> </h2>
	<?php		
			if(empty ($seminar)){
				echo '
				<div class="alert alert-warning alert-auto alert-dismissible fade show" role="alert">
						<h5 class="alert-heading">Info:</h5>
					<p>Kein Inhalt zu diesem Seminar.
					</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
						</button>
			</div>';
			}else{
	?>
			
		<div class="col-md-8">
			<table class="table no-border">
			<tr>
				<th width=200> Prüfungsnummer:</th>
				<td> <?php echo $seminarID; ?> </td>     
			</tr>
			<tr>
				<th> Semester:</th>
				<td> <?php echo $semester; ?> </td>     
			</tr>
			<tr>
				<th> Beschreibung:</th>
				<td> <?php echo $seminar['Beschreibung']; ?> </td>     
			</tr>
			<tr>
				<th> Teilnehmeranzahl: </th>
				<td> <?php echo $seminar['Teilnehmeranzahl']; ?> </td>
			</tr>
			<tr>
				<th> Abschluss: </th>
				<td> <?php echo $seminar['Abschluss']; ?> </td>
			</tr>
			<tr>
				<th> Lehrstuhl:</th>
				<td> <a href="profil2.php?Email=<?php echo $seminar['Email'] ?>" title="Weiter zum Lehrstuhl"> 
						<font color="black"><?php echo $seminar['Bezeichnung']; ?></font> </a> </td>
			</tr>
			</table>
		
		<?php
			}
			if((!empty ($ZvorAnmeldeBeginn) && empty($alleBewerber)) || ($rolle == 4 && $testmodus['Testmodus'] == "1" && empty($alleBewerber))){ 
				if($email == $seminar['Email'] || $rolle == 4){
		?>
			<p>
			  <table>
				<td> <a class="btn btn-info" href="seminarBearbeiten.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>" role="button"><i class="material-icons"  style="font-size:15px">create</i> Bearbeiten </a> &nbsp;  </td>
				<td> <form action="befehlProzesse.php" method="POST">
						<input type="hidden" name="seminarLoeschen"  value="loeschen">
						<input type="hidden" name="seminarID" value=<?php echo $seminarID ?> >
						<input type="hidden" name="semester"  value=<?php echo $semester ?> >
						<button type="submit" class="btn btn-danger"><i class="material-icons"  style="font-size:15px">delete</i> Löschen </button>
					</form> </td>
			  </table>
			</p>
			</div>
		<?php
				}
			}
			if(!empty ($ZnachAblehnungEnde)){
		?>
			<p>
			  <table>
				<td><a class="btn btn-info" href="seminarThemen.php?Seminar_ID=<?php echo $seminarID ?>&Semester=<?php echo $semester ?>" role="button"> Teilnehmer mit zugehörigen Seminarthemen</a> </td>
			</table>
			</p>
		<?php
			}
			if(!empty ($zZuteilungNachAnmeldeEnde)){ 
				if($rolle == 3 || $rolle == 4){
		?>
			<p>
			  <table>
				<td> <a class="btn btn-info" href="anzahlErweitern.php?Seminar_ID=<?php echo $seminar['Seminar_ID'] ?>&Semester=<?php echo $seminar['Semester'] ?>" role="button"> Seminarteilnehmeranzahl erweitern </a> &nbsp;  </td>
			  </table>
			</p>
		<?php
				}
			}
			if($rolle == 1 || $rolle == 4){
				//Prüfe, ob eine Bewerbung vorliegt.
				$studentAbschluss = " ";
				$adminAbschluss   = " ";
				$adminAbschluss2  = " ";
				
				//Abfrage der Student_ID und des angestrebten Abschlusses
				if($rolle == 1){
					$studentID = $student['Student_ID'];
					$studentAbschluss = $student['Abschluss'];
				}
				//Abfrage der Admin_ID, falls der Admin sich für das Seminar zur Probe des Systems bewirbt.
				if($rolle == 4){
					$studentID = $admin['Admin_ID'];
					$adminAbschluss = 'Bachelor';
					$adminAbschluss2 = 'Master';
				}
				if(!empty($anmeldeZeitraum)){
				  //Kontrolle, ob der Abschluss des Studenten, dem benötigten Abschluss des Seminars entspricht.
					if( ($seminar['Abschluss'] == $studentAbschluss) || ($seminar['Abschluss'] == $adminAbschluss) || ($seminar['Abschluss'] == $adminAbschluss2)){
						if(empty ($bewerber)){
							//Wenn noch keine Bewerbung zu dem Seminar vorliegt, wird der Button "Für das Seminar bewerben" angezeigt.
	?>
			<form action="befehlProzesse.php" method="POST">
			<input type="hidden" name="bewerben"  value="bewerben">
			<input type="hidden" name="seminarID" value=<?php echo $seminarID ?> >
			<input type="hidden" name="semester"  value=<?php echo $semester ?> >
			<input type="hidden" name="studentID" value=<?php echo $studentID ?> >
			<p><button type="submit" class="btn btn-info"> Für das Seminar bewerben </button> </p>
			</form>
	<?php
						}else{
							//Falls schon eine Bewerbung vorliegt, wird der Button "Bewerbunng zurücknehmen" angezeigt.
	?>
			<form action="befehlProzesse.php" method="POST">
			<input type="hidden" name="bewerbungZurueck"  value="bewerbZu">
			<input type="hidden" name="seminarID" value=<?php echo $seminarID ?> >
			<input type="hidden" name="semester"  value=<?php echo $semester ?> >
			<input type="hidden" name="studentID" value=<?php echo $studentID ?> >
			<p><button type="submit" class="btn btn-info"> Bewerbung Widerrufen </button> </p>
			</form>
	<?php
						}
					}else{
						echo '<div class="alert alert-warning alert-dismissible fade show top50" role="alert">
								Sie können sich für dieses Seminar nicht bewerben, da Sie nicht den benötigten angestrebten Abschluss besitzen. 
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								</div>';
					}
				}else{
					if(!empty($bewerbungSeminar)){
						echo '<div class="alert alert-success alert-dismissible fade show top50" role="alert">
								Sie haben sich für dieses Seminar beworben.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							  </div>';
					}
				}
			}
			include 'fusszeile.php';
		  }
		}
	?>
    </div>
  </body>
</html>
