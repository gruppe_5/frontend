<?php
	//Kontrolle, wie viele Nutzer im System vorhanden sind. Wenn 0, dann schalte Registrierung für Admin frei. Wenn ungleich 0, dann Login Seite zu sehen.
	$userZaehlen = $pdo->prepare("SELECT COUNT(Email)
								  FROM User");
	$result 	 = $userZaehlen->execute(array());
	$adminUser   = $userZaehlen->fetch();
	
	//Alle Nutzer, die im System gespeichert sind
	$alleNutzer = $pdo->prepare("SELECT DISTINCT *
								 FROM User
								 JOIN User_Historie ON User.Email = User_Historie.Email
								 ORDER BY Rolle, User.Email, Letzter_Login DESC ");
	$result 	= $alleNutzer->execute(array());
	$nutzer	    = $alleNutzer->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Nutzer, die im System gespeichert sind
	$alleNutzer = $pdo->prepare("SELECT DISTINCT *
								 FROM User
								 JOIN User_Historie ON User.Email = User_Historie.Email
								 ORDER BY  Rolle, User.Email, Letzter_Login DESC ");
	$result 	= $alleNutzer->execute(array());
	
	
	//Alle Nutzer, die sich durch mehrmalige Falscheingabe des Passwortes aus dem System gesperrt haben.
	//Nur der Admin ist dazu berechtigt, dies abzufragen.
	$gesperrteNutzer = $pdo->prepare("SELECT DISTINCT *
										FROM User_Historie
										JOIN User ON User.Email = User_Historie.Email
										WHERE Falsches_Passwort = '10' 
										ORDER BY Letzter_Login DESC, Rolle ");
	$result     	 = $gesperrteNutzer->execute(array());
	$gesperrt   	 = $gesperrteNutzer->fetch();
	//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
	//Alle Nutzer, die sich durch mehrmalige Falscheingabe des Passwortes aus dem System gesperrt haben.
	//Nur der Admin ist dazu berechtigt, dies abzufragen.
	$gesperrteNutzer = $pdo->prepare("SELECT DISTINCT *
										FROM User_Historie
										JOIN User ON User.Email = User_Historie.Email
										WHERE Falsches_Passwort = '10' 
										ORDER BY Letzter_Login DESC, Rolle ");
	$result     	 = $gesperrteNutzer->execute(array());
	
	if(isset($_SESSION['User_Email'])){
		$userEmail = $_SESSION['User_Email'];
			
		//Abfrage des letzten Login-Datums des gesperrten Nutzers.
		$userLetzterLogin = $pdo->prepare("SELECT MAX(User_Historie_ID)
											FROM User_Historie
											WHERE Email = :userEmail");
		$result           = $userLetzterLogin->execute(array(':userEmail' => $userEmail));
		$letzterLogin     = $userLetzterLogin->fetch();
	  
		if(isset($_SESSION['User_Rolle'])){
			$userRolle = $_SESSION['User_Rolle'];
		  
			if($userRolle == "1"){
				//Prüfe, ob der Student Daten älter als 5 Jahre besitzt.
				$alteNutzerDaten = $pdo->prepare("SELECT Bewerbungszeitraum.Bewerbungszeitraum_ID
												  FROM Bewerbungszeitraum
												  JOIN Seminar ON Seminar.Bewerbungszeitraum_ID = Bewerbungszeitraum.Bewerbungszeitraum_ID
												  JOIN Bewerbungszuteilung ON Bewerbungszuteilung.Seminar_ID = Seminar.Seminar_ID
												  JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
												  WHERE Bewerbungszuteilung.Semester = Seminar.Semester
													AND Zweite_Zuteilung_Deadline > CURRENT_DATE() - INTERVAL 5 YEAR
													AND Student.Email = :userEmail");
				$result          = $alteNutzerDaten->execute(array(':userEmail' => $userEmail));
				$alteDaten		 = $alteNutzerDaten->fetch();
			
			unset($_SESSION['User_Rolle']);
			}
		}
		unset($_SESSION['User_Email']);
	}
	
	

		//Alle bisher angelegten Bewerbungszeiträume, absteigend sortiert.
		$bewZeitraeume        = $pdo->prepare("SELECT Name, Bewerbungszeitraum_ID
												FROM Bewerbungszeitraum
												ORDER BY Bewerbungszeitraum_ID DESC");
		$result               = $bewZeitraeume->execute(array());
		$bewerbungszeitraeume = $bewZeitraeume->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Alle bisher angelegten Bewerbungszeiträume, absteigend sortiert.
		$bewZeitraeume        = $pdo->prepare("SELECT Name, Bewerbungszeitraum_ID
												FROM Bewerbungszeitraum
												ORDER BY Bewerbungszeitraum_ID DESC");
		$result               = $bewZeitraeume->execute(array());
	
	

	
		//Vor Beginn des Anmeldezeitraums für Seminare
		$ZvorAnmeldeBeg    = $pdo->prepare("SELECT *
											FROM Bewerbungszeitraum
											WHERE (NOW() < Anmelde_Anfang)
											ORDER BY Bewerbungszeitraum_ID DESC
											LIMIT 1");
		$result     	   = $ZvorAnmeldeBeg->execute(array());
		$ZvorAnmeldeBeginn = $ZvorAnmeldeBeg->fetch();
	
		//Anmeldezeitraum, der Zeitraum, in dem die Studenten sich für Seminare bewerben können.
		$seminaranmeldungszeitraum = $pdo->prepare("SELECT *
													FROM Bewerbungszeitraum
													WHERE (NOW() BETWEEN Anmelde_Anfang AND Anmelde_Ende)
													ORDER BY Bewerbungszeitraum_ID DESC 
													LIMIT 1");
		$result    				   = $seminaranmeldungszeitraum->execute(array());
		$anmeldeZeitraum 		   = $seminaranmeldungszeitraum->fetch();
	
		//Zuteilungszeitraum, der Erste Zuteilungs-Zeitraum, in dem die Studenten von den Lehrstühlen zu Seminaren zugeteilt werden.
		$ersteZuteilungZeitraum = $pdo->prepare("SELECT *
												FROM Bewerbungszeitraum
												WHERE (NOW() BETWEEN Anmelde_Ende AND Zuteilung_Deadline)
												ORDER BY Bewerbungszeitraum_ID DESC 
												LIMIT 1");
		$result     			= $ersteZuteilungZeitraum->execute(array());
		$zuteilungZeitraum	    = $ersteZuteilungZeitraum->fetch();
		
		//Zuteilungszeitraum komplett, nach Ablauf der Anmeldefrist.
		$zuteilungZeitraumNachAnmeldeEnde  = $pdo->prepare("SELECT *
															FROM Bewerbungszeitraum
															WHERE (NOW() > Anmelde_Ende)
															ORDER BY Bewerbungszeitraum_ID DESC
															LIMIT 1");
		$result    							= $zuteilungZeitraumNachAnmeldeEnde->execute(array());
		$zZuteilungNachAnmeldeEnde  		= $zuteilungZeitraumNachAnmeldeEnde->fetch();
	
		//Zeitraum zwischen dem ersten Zuteilungs-Ende und der Ablehnungsfrist.
		$wartelisteZwischenZeitraum = $pdo->prepare("SELECT *
													FROM Bewerbungszeitraum
													WHERE (NOW() BETWEEN Zuteilung_Deadline AND Ablehnung_Deadline)
													ORDER BY Bewerbungszeitraum_ID DESC 
													LIMIT 1");
		$result     				= $wartelisteZwischenZeitraum->execute(array());
		$wartelisteZeitraum	    	= $wartelisteZwischenZeitraum->fetch();
	
		//Ablehnungszeitraum, der Zeitraum, in dem die Studenten einen zugeteilten Seminarplatz annehmen bzw. ablehnen können.
		$ablehnungszeitraum = $pdo->prepare("SELECT *
											FROM Bewerbungszeitraum
											WHERE (NOW() BETWEEN Anmelde_Ende AND Ablehnung_Deadline)
											ORDER BY Bewerbungszeitraum_ID DESC 
											LIMIT 1");
		$result       	    = $ablehnungszeitraum->execute(array());
		$ablehnZeitraum     = $ablehnungszeitraum->fetch();
	
		//Zweiter Zuteilungszeitraum, der Zweite Zuteilungs-Zeitraum, in dem die Studenten von den Lehrstühlen zu Seminaren zugeteilt werden.
		$zweiteZuteilungZeitraum = $pdo->prepare("SELECT *
												FROM Bewerbungszeitraum
												WHERE (NOW() BETWEEN Ablehnung_Deadline AND Zweite_Zuteilung_Deadline)
												ORDER BY Bewerbungszeitraum_ID DESC 
												LIMIT 1");
		$result      			 = $zweiteZuteilungZeitraum->execute(array());
		$zwZuteilungZeitraum     = $zweiteZuteilungZeitraum->fetch();
		
		//Ganzer Zeitraum nach Ablauf des Ablehnungsfrists.
		$ZnachAblehnungEnd   = $pdo->prepare("SELECT *
											FROM Bewerbungszeitraum
											WHERE (NOW() > Ablehnung_Deadline)
											ORDER BY Bewerbungszeitraum_ID DESC 
											LIMIT 1");
		$result   			 = $ZnachAblehnungEnd->execute(array());
		$ZnachAblehnungEnde  = $ZnachAblehnungEnd->fetch();
		
		//Themen Zuteilungszeitraum, der Zuteilungs-Zeitraum, in dem die Lehrstühle die Seminarthemen verteilen.
		$themenZuteilungZeitraum = $pdo->prepare("SELECT *
												FROM Bewerbungszeitraum
												WHERE (NOW() BETWEEN Ablehnung_Deadline AND Themen_Zuteilung_Deadline)
												ORDER BY Bewerbungszeitraum_ID DESC 
												LIMIT 1");
		$result      			 = $themenZuteilungZeitraum->execute(array());
		$thZuteilungZeitraum     = $themenZuteilungZeitraum->fetch();

		//Zeitraum nach Ablauf des Zweiten Zuteilungszeitraums bis ein neuer Bewerbungszeitraum angelegt wird.
		$ZnachZweiteZuteilungEnd   = $pdo->prepare("SELECT *
													FROM Bewerbungszeitraum
													WHERE (NOW() > Zweite_Zuteilung_Deadline)
													ORDER BY Bewerbungszeitraum_ID DESC 
													LIMIT 1");
		$result     			   = $ZnachZweiteZuteilungEnd->execute(array());
		$ZnachZweiteZuteilungEnde  = $ZnachZweiteZuteilungEnd->fetch();
	
	
		//Prüfen, ob die Zuteilungsfrist der Themen noch nicht vorbei ist.
		$altBewerbungszeitraumBearbeiten   = $pdo->prepare("SELECT *
															FROM Bewerbungszeitraum
															WHERE (NOW() < Themen_Zuteilung_Deadline)
															ORDER BY Bewerbungszeitraum_ID DESC
															LIMIT 1");
		$result     						= $altBewerbungszeitraumBearbeiten->execute(array());
		$bewerbungszeitraumBearbeiten		= $altBewerbungszeitraumBearbeiten->fetch();
	
		//Prüfen, ob die Zuteilungsfrist der Themen vorbei ist.
		$neuenBewerbungszeitraumFestlegen   = $pdo->prepare("SELECT *
															FROM Bewerbungszeitraum
															WHERE (NOW() > Themen_Zuteilung_Deadline)
															ORDER BY Bewerbungszeitraum_ID DESC
															LIMIT 1");
		$result     						= $neuenBewerbungszeitraumFestlegen->execute(array());
		$bewerbungszeitraumFestlegen		= $neuenBewerbungszeitraumFestlegen->fetch();
	
		//Prüfen, ob der Testmodus aktiviert ist.
		$testmodusAktiv = $pdo->prepare("SELECT Testmodus
										FROM Admin
										LIMIT 1");
		$result     	= $testmodusAktiv->execute(array());
		$testmodus		= $testmodusAktiv->fetch();
	
	
	if(isset($_SESSION['Zeitraum_ID'])){
		$zeitraumID = $_SESSION['Zeitraum_ID'];
		
		//Prüfe, ob der Bewerbungszeitraum älter als 5 Jahre ist.
		$altBewerbungszeitraum   = $pdo->prepare("SELECT Bewerbungszeitraum_ID
												  FROM Bewerbungszeitraum
												  WHERE Zweite_Zuteilung_Deadline < CURRENT_DATE() - INTERVAL 5 YEAR
													AND Bewerbungszeitraum_ID = :zeitraumID");
		$result                  = $altBewerbungszeitraum->execute(array(':zeitraumID' => $zeitraumID));
		$altesBewerbungszeitraum = $altBewerbungszeitraum->fetch();
	}
	
	
	
	
	if(isset ($_GET['Bewerbungszeitraum_ID'])){	
		$bewerbungszeitraumID   = $_GET['Bewerbungszeitraum_ID'];
		
		//Abfrage der Details des Bewerbungszeitraums.
		$bewZeitraum       = $pdo->prepare("SELECT *
											FROM Bewerbungszeitraum
											WHERE Bewerbungszeitraum_ID = :bewerbungszeitraumID");
		$result    			 = $bewZeitraum->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));
		$bewerbungszeitraum  = $bewZeitraum->fetch();
	
		//Seminare mit zugehörigen Daten im jeweiligen Bewerbungszeitraum.
		$seminareDaten = $pdo->prepare("SELECT *
										FROM Seminar
										JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
										WHERE Bewerbungszeitraum_ID = :bewerbungszeitraumID
									    ORDER BY Abschluss, Bezeichnung");
		$result    	  = $seminareDaten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));
		$seminare     = $seminareDaten->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Seminare mit zugehörigen Daten im aktuellen Bewerbungszeitraum.
		$seminareDaten = $pdo->prepare("SELECT *
										FROM Seminar
										JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
										WHERE Bewerbungszeitraum_ID = :bewerbungszeitraumID
									    ORDER BY Abschluss, Bezeichnung");
		$result    	  = $seminareDaten->execute(array(':bewerbungszeitraumID' => $bewerbungszeitraumID));
	}
	
	
	

if(isset($_SESSION['Email']) || isset ($_GET['Email']) || isset($_SESSION['Email2'])){	
		if(isset($_SESSION['Email'])){
			$email = $_SESSION['Email'];
		}
		if (isset ($_GET['Email'])){
			$email = $_GET['Email'];
		}else
		if(isset($_SESSION['Email2'])){
			$email = $_SESSION['Email2'];
		}
		
		//Studiendekan-Daten Abfragen.
		$studiendekanDaten = $pdo->prepare("SELECT *
											FROM Studiendekan
											JOIN User ON User.Email = Studiendekan.Email
											WHERE Studiendekan.Email = :email");
		$result     	   = $studiendekanDaten->execute(array(':email' => $email));
		$studiendekan	   = $studiendekanDaten->fetch();

		//Lehrstuhl-Daten Abfragen.
		$lehrstuhlDaten = $pdo->prepare("SELECT *
										FROM Lehrstuhl
										JOIN User ON User.Email = Lehrstuhl.Email
										WHERE Lehrstuhl.Email = :email");
		$result         = $lehrstuhlDaten->execute(array(':email' => $email));
		$lehrstuhl      = $lehrstuhlDaten->fetch();
		
		//Student-Daten Abfragen.
		$studentDaten = $pdo->prepare("SELECT *
										FROM Student
										JOIN User ON User.Email = Student.Email
										WHERE Student.Email = :email ");
		$result       = $studentDaten->execute(array(':email' => $email));
		$student      = $studentDaten->fetch();
		
		//Admin-Daten Abfragen.
		$adminDaten = $pdo->prepare("SELECT * 
									 FROM Admin
									 JOIN User ON User.Email = Admin.Email
									 WHERE Admin.Email = :email");
		$result     = $adminDaten->execute(array(':email' => $email));
		$admin      = $adminDaten->fetch();
		
		unset($_SESSION['Email2']);

		
		
		
		
		//Alle Seminaren, die ein Lehrstuhl angelegt hat.
		$lehrstuhlSeminar = $pdo->prepare("SELECT *
											FROM Seminar
											JOIN Lehrstuhl ON Seminar.Lehrstuhl_ID = Lehrstuhl.Lehrstuhl_ID
											WHERE Lehrstuhl.Email = :email
											ORDER BY Bewerbungszeitraum_ID DESC");
		$result    		  = $lehrstuhlSeminar->execute(array(':email' => $email));
		$angelegt  		  = $lehrstuhlSeminar->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Alle Bewerbungen zu Seminaren, die ein Student abgeschickt hat.
		$lehrstuhlSeminar = $pdo->prepare("SELECT *
											FROM Seminar
											JOIN Lehrstuhl ON Seminar.Lehrstuhl_ID = Lehrstuhl.Lehrstuhl_ID
											WHERE Lehrstuhl.Email = :email
											ORDER BY Bewerbungszeitraum_ID DESC");
		$result    		  = $lehrstuhlSeminar->execute(array(':email' => $email));
		

		//Alle Bewerbungen zu Seminaren, die ein Student abgeschickt hat.
		$studentBewerbungSeminar = $pdo->prepare("SELECT *
												FROM Bewerbungszuteilung
												JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
												JOIN Lehrstuhl ON Seminar.Lehrstuhl_ID = Lehrstuhl.Lehrstuhl_ID
												JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
												WHERE Bewerbungszuteilung.Semester = Seminar.Semester
													AND Student.Email = :email
												ORDER BY Bewerbung_Datum DESC");
		$result    				 = $studentBewerbungSeminar->execute(array(':email' => $email));
		$bewerbungSeminar  		 = $studentBewerbungSeminar->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Alle Bewerbungen zu Seminaren, die ein Student abgeschickt hat.
		$studentBewerbungSeminar = $pdo->prepare("SELECT *
												FROM Bewerbungszuteilung
												JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
												JOIN Lehrstuhl ON Seminar.Lehrstuhl_ID = Lehrstuhl.Lehrstuhl_ID
												JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
												WHERE Bewerbungszuteilung.Semester = Seminar.Semester
													AND Student.Email = :email
												ORDER BY Bewerbung_Datum DESC");
		$result    				 = $studentBewerbungSeminar->execute(array(':email' => $email));
	
		//Alle Seminare bzw. Bewerbungen eines Studenten, zu welchen sie vom Lehrstuhl zugeteilt wurden.
		$studentZugeteilteSeminare = $pdo->prepare("SELECT *
													FROM Bewerbungszuteilung
													JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
													JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
													JOIN Lehrstuhl ON Seminar.Lehrstuhl_ID = Lehrstuhl.Lehrstuhl_ID
													WHERE Bewerbungszuteilung.Semester = Seminar.Semester
														AND Student.Email = :email
														AND Bewerbungszuteilung.Zuteilung = '1' 
													ORDER BY Bewerbungszuteilung.Bewerbungszuteilung_ID DESC");
		$result  				   = $studentZugeteilteSeminare->execute(array(':email' => $email));
		$zugeteilteSeminare  	   = $studentZugeteilteSeminare->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Alle Seminare eines Studenten, zu welchen sie vom Lehrstuhl zugeteilt wurden.
		$studentZugeteilteSeminare = $pdo->prepare("SELECT *
													FROM Bewerbungszuteilung
													JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
													JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
													JOIN Lehrstuhl ON Seminar.Lehrstuhl_ID = Lehrstuhl.Lehrstuhl_ID
													WHERE Bewerbungszuteilung.Semester = Seminar.Semester
														AND Student.Email = :email
														AND Bewerbungszuteilung.Zuteilung = '1' 
													ORDER BY Bewerbungszuteilung.Bewerbungszuteilung_ID DESC");
		$result  				   = $studentZugeteilteSeminare->execute(array(':email' => $email));
}	
	
	
	
	if(isset($_GET['Seminar_ID'])){	
		$seminarID     = $_GET['Seminar_ID'];
		$semester      = $_GET['Semester'];
		
		//Seminar-Daten mit zugehörigem Lehrstuhl-Daten
		$semin   = $pdo->prepare("SELECT *
									FROM Seminar
									JOIN Lehrstuhl ON Lehrstuhl.Lehrstuhl_ID = Seminar.Lehrstuhl_ID
									WHERE Seminar_ID = :seminarID
										AND Semester   = :semester");
		$result  = $semin->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$seminar = $semin->fetch();
	
		
		//Daten der Studenten, die sich zum Seminar beworben hat.
		$alleBewerberDaten = $pdo->prepare("SELECT *
										FROM Bewerbungszuteilung
										JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
										JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
										WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
											AND Bewerbungszuteilung.Semester = :semester ");
		$result  	       = $alleBewerberDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$alleBewerber      = $alleBewerberDaten->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Daten des Studenten, der sich zum Seminar beworben hat.
		$alleBewerberDaten = $pdo->prepare("SELECT *
										FROM Bewerbungszuteilung
										JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
										JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
										WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
											AND Bewerbungszuteilung.Semester = :semester ");
		$result  	   = $alleBewerberDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));



		//Daten des Studenten, der sich zum Seminar beworben hat.
		$bewerberDaten = $pdo->prepare("SELECT *
										FROM Bewerbungszuteilung
										JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
										JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
										WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
											AND Bewerbungszuteilung.Semester = :semester 
											AND Student.Email = :email");
		$result  	   = $bewerberDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':email' => $email));
		$bewerber      = $bewerberDaten->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//Daten des Studenten, der sich zum Seminar beworben hat.
		$bewerberDaten = $pdo->prepare("SELECT *
										FROM Bewerbungszuteilung
										JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
										JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
										WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
											AND Bewerbungszuteilung.Semester = :semester 
											AND Student.Email = :email");
		$result  	   = $bewerberDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':email' => $email));


		//Zugeteilt zum Seminar
		$zugeteilteStudenten = $pdo->prepare("SELECT *
												FROM Bewerbungszuteilung
												WHERE Seminar_ID = :seminarID
													AND Semester = :semester
													AND Zuteilung = '1' ");
		$result     		 = $zugeteilteStudenten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$zugeteilt    		 = $zugeteilteStudenten->fetch();
				
	
		//In dem Seminar eingetragene Teilnehmer zählen.
		$eingetragenZaehlen = $pdo->prepare("SELECT COUNT(Student_ID)
											FROM Bewerbungszuteilung
											WHERE Seminar_ID = :seminarID
												AND Semester  = :semester
												AND Zuteilung = '1'
												AND Ablehnung = '0' ");
		$result		   		= $eingetragenZaehlen->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$eingetragen   		= $eingetragenZaehlen->fetch();
	
	
		//In dem Seminar eingetragene Teilnehmer mit ihren Daten.
		$teilnehmerDaten = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester   = :semester
												AND Bewerbungszuteilung.Zuteilung = '1' 
												AND Bewerbungszuteilung.Ablehnung = '0' ");
		$result			 = $teilnehmerDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));
		$teilnehmer		 = $teilnehmerDaten->fetch();
		//OHNE fetch(), da diese die erste Zeile der Abfrage abfängt und nicht anzeigt.
		//In dem Seminar eingetragene Teilnehmer mit ihren Daten.
		$teilnehmerDaten = $pdo->prepare("SELECT *
											FROM Bewerbungszuteilung
											JOIN Student ON Student.Student_ID = Bewerbungszuteilung.Student_ID
											JOIN Seminar ON Seminar.Seminar_ID = Bewerbungszuteilung.Seminar_ID
											WHERE Bewerbungszuteilung.Seminar_ID = :seminarID
												AND Bewerbungszuteilung.Semester   = :semester
												AND Bewerbungszuteilung.Zuteilung = '1' 
												AND Bewerbungszuteilung.Ablehnung = '0' ");
		$result			 = $teilnehmerDaten->execute(array(':seminarID' => $seminarID, ':semester' => $semester));

		
	
	  if(isset($_SESSION['Student_ID'])){
			$studentID = $_SESSION['Student_ID'];
			
			//Abfrage des Seminarthemas des jeweiligen Seminarteilnehmers.
			$themaStmt       = $pdo->prepare("SELECT *
											  FROM Seminarthema
											  WHERE Seminar_ID = :seminarID
												AND Semester = :semester
												AND Student_ID = :studentID ");
			$result      = $themaStmt->execute(array(':seminarID' => $seminarID, ':semester' => $semester, ':studentID' => $studentID));
			$thema       = $themaStmt->fetch();
			
			unset($_SESSION['Student_ID']);
	  }
	}
?>