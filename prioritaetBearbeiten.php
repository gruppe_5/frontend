<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
				
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
			
			if($rolle == 1 || $rolle == 4){
	?>
				<h2> Meine Bewerbungen zu Seminaren </h2>
	<?php
				if(empty ($bewerbungSeminar)){
					echo 'Keine Bewerbungen zu Seminaren vorhanden.';
				}else{
	?>
				<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
					<h5 class="alert-heading">Info:</h5>
						Bitte teilen Sie ihren Bewerbungen eine Priorität von 1 - 5 zu. 
						Jede Priorität darf nur einmal vergeben werden. </br>
						Dabei ist 1 die höchste und 5 die niedrigste Priorität. </br>
						(Alle weiteren Bewerbungen werden ohne Priorität mit 0 aufgelistet.) 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
				</div>
						
			<div class="table-responsive">	
			<table class="table table table-striped table-hover table-bordered">
			<thead>
			<tr>
				<th scope="col"> Semester 		</th>				
				<th scope="col"> Prüfungsnummer </th>				
				<th scope="col"> Seminartitel  	</th>				
				<th scope="col"> Lehrstuhl      </th>
				<th scope="col"> Priorität      </th>
				<th scope="col"> Bewerbung am   </th>
				<th scope="col"> Status         </th>
			</tr>
			</thead>				
	<?php		
					foreach ($studentBewerbungSeminar as $row){ 
	?>
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="prioritaetBearbeiten" value="bearbeiten">
				<input type="hidden" name="seminarID[]" value="<?php echo $row['Seminar_ID'] ?>" >
				<input type="hidden" name="semester" value="<?php echo $row['Semester'] ?>" >
				<input type="hidden" name="studentID" value="<?php echo $row['Student_ID'] ?>" >
			<tbody>
			<tr>
				<td> <?php echo $row['Semester']; ?> </td>
				<td> <?php echo $row['Seminar_ID']; ?> </td>
				<td> <a href="seminar.php?Seminar_ID=<?php echo $row['Seminar_ID'] ?>&Semester=<?php echo $row['Semester'] ?>" data-toggle="tooltip" title="Weiter zur Seminarübersicht"> 
						<font color="black"><?php echo $row['Titel']; ?></font>
					</a> </td>   
				<td> <?php echo $row['Bezeichnung']; ?> </td>
				<td> <select name="prio[]" class="form-control" value="<?php echo $row['Prioritaet']; ?>" required>
						<option selected="selected" hidden> <?php echo $row['Prioritaet']; ?> </option>
						<option> 0 </option>
						<option> 1 </option>
						<option> 2 </option>
						<option> 3 </option>
						<option> 4 </option>
						<option> 5 </option>
					 </select> &nbsp; &nbsp; </td>
				<td> <?php $date = new DateTime($row['Bewerbung_Datum']);
							echo $date->format('d.m.Y H:i'); ?> </td>
	<?php				if(empty ($row['Zuteilung_Datum'])){
							if(!empty ($anmeldeZeitraum)){
							echo '<td><i> Anmeldung zu Seminaren läuft </i></td>';
							}
							if(!empty ($zuteilungZeitraum)){
							echo '<td><i> In Bearbeitung </i></td>';
							}
							if(!empty ($wartelisteZeitraum)){
							echo '<td><i> Ablehnungsfrist läuft. </i></td>';
							}
							if(!empty ($zwZuteilungZeitraum)){
							echo '<td><i> Nachrückerverfahren </i></td>';
							}
							if(!empty ($ZnachZweiteZuteilungEnde)){
							echo '<td><i> Warteliste. </br> Der Studiendekan </br> verteilt nun Plätze.</i></td>';
							}
						}else{
							if($row['Zuteilung'] == 1){
								echo '<td><i> Zum Seminar Zugteilt </i></td>';
								echo '<td><i> <a href="seminarZuteilung.php" role="button"> Nehmen Sie den Platz an? </a></i></td>';
							}
						}
	?>
			</tr>
			</tbody>
	<?php
					}
	?>
			</table>
		</div>
	<?php
					if(!empty ($anmeldeZeitraum)){
	?>
			 <button type="submit" class="btn btn-info"> Prioritäten Vergeben</button>&nbsp;
				</form>
	<?php
					}
	?>
			 <a href="bewerbungen.php" class="btn btn-info" > Abbrechen </a> </button>			
	<?php
				}
				include 'fusszeile.php';
			}else{
				include 'keineBerechtigung.php';
			}
		}
	?>
    </div>
  </body>
</html>
