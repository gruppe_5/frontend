<!doctype html>
<html lang="en">
  <head>
    <title>Seminarvergabesystem</title>

	<?php include 'css.php'; ?>
  </head>

  <body>
    <div class="container top50">
	
	<?php
		require_once 'session.php';
		require 'dbVerbindung.php';
			
		if(!isset($_SESSION['Email'])){	
			include 'keinZugriff.php';
		}else{
			$email         = $_SESSION['Email'];
			$rolle         = $_SESSION['Rolle'];
	
			include 'navBar.php';
			include 'meldung.php';
			include 'sql.php';
	?>
			<h2> Bewerbungszeitraum Festlegen </h2>
	<?php
			if(!empty($bewerbungszeitraeume) && empty($bewerbungszeitraumFestlegen) && $testmodus['Testmodus'] == "0"){
	?>
			<div class="alert alert-danger alert-auto alert-dismissible fade show" role="alert">
				  <h5 class="alert-heading">Info:</h5>
				  <p> Sie können keinen neuen Bewerbungszeitraum festlegen, solange der vorherige Bewerbungszeitraum noch nicht abgelaufen ist. </p>
			   	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
			</div>
	<?php
			}else{
				if($rolle == 3 || $rolle ==4){
	?>
			<div class="alert alert-info alert-auto alert-dismissible fade show" role="alert">
				  <h5 class="alert-heading">Info:</h5>
				  <p>Nach Erstellen des Bewerbungszeitraums können die Lehrstühle die Seminare anlegen. </br>
					 Alle Daten müssen aufeinander folgen, d.h. die Ablehnungsfrist darf nicht vor der 1.Zuteilungsfrist oder nach der 2.Zuteilungsfrist liegen, sondern muss zwischen diesen zwei Daten liegen. </p>
			   	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				</button>
				</div>

			<table class="table table-sm no-border">
				<form action="befehlProzesse.php" method="POST">
				<input type="hidden" name="bewerbungszeitraumFestlegen" value="festlegen">
			<tr>
				<td> <b>Bewerbungszeitraum für die Seminare im:</b> </br>
					 <p class="text-muted">Bitte hier z.B. WS17/18 für Wintersemester 2017/2018 und </br>
					 SoSe18 für Sommersemester 2018 eingeben. </p></td>      
				<td> <br><input type="text" maxlength="7" name="zeitraumName" class="form-control" placeholder="Bewerbungszeitraum" required> </td>
			</tr>
			<tr>
				<td><b> Anmelde Anfang:</b> <br>
					 <p class="text-muted"> Beginn der Bewerbungsphase für die Seminare.</p> </td>
				<td> <br><input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="anmeldeAnfang" class="form-control" placeholder="Anmelde Anfang" required> </td>
			</tr>
			<tr>
				<td> <b>Anmelde Ende:</b> </br>
					 <p class="text-muted" >Ende der Bewerbungsphase für die Seminare.</p> </td>
				<td> <br><input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="anmeldeEnde" class="form-control" placeholder="Anmelde Ende" required> </td>
			</tr>
			<tr>
				<td> <b>1. Zuteilung:</b> </br>
					 <p class="text-muted" >Die 1. Runde zum Zuteilen der Studierenden zu </br>
					 den Seminaren nach Ablauf der Bewerbungsphase. </p></td>
				<td> <br><input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="zuteilungDeadline" class="form-control" placeholder="1. Zuteilung" required> </td>
			</tr>
			<tr>
				<td> <b>Ablehnungsfrist:</b> </br>
					 <p class="text-muted">Die Frist für die Studierenden zum Ablehnen </br> 
					 	eines zugeteilten Seminarplatzes. </p></td>
				<td> <br><input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="ablehnungDeadline" class="form-control" placeholder="Ablehnungsfrist" required> </td>
			</tr>
			<tr>
				<td> <b>2. Zuteilung:</b> </br>
					 <p class="text-muted">Die 2. Runde zum Zuteilen der Studierenden zu </br>
					 den Seminaren nach Ablauf der Ablehnungsfrist.</p></td>
				<td><br> <input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="zweiteZuteilungDeadline" class="form-control" placeholder="2. Zuteilung" required> </td>
			</tr>
			<tr>
				<td> <b>Frist zur Themenverteilung: </b></br>
					 <p class="text-muted">Die Frist für Lehrstühle zum Verteilen der Seminarthemen an ihre Seminarteilnehmer.</p></td>
				<td><br> <input type="datetime-local" min="2018-02-01T00:00" max="2100-01-01T00:00" name="themenZuteilungDeadline" class="form-control" placeholder="Frist zur Themenverteilung" required> </td>
			</tr>
			<tr>
				<th> <button type="submit" class="btn btn-info"> Bewerbungszeitraum Festlegen</button>
				</form> 
					 <a href="bewerbungszeitraeume.php"  class="btn btn-info"> Abbrechen </a></th>
			</tr>
			</table>
			
			</br>
	<?php
				}else{
					include 'keineBerechtigung.php';
				}
			}
			include 'fusszeile.php';
		}
	?>
    </div>
  </body>
</html>
